﻿
using Fart.Geometry;
using Fart.Maths;
using System;

namespace Fart.Shading
{
    public sealed class DiffuseMaterial: Material
    {
        public Color3 Diffuse { get; private set; }

        public DiffuseMaterial(Color3 diffuseColor)
        {
            Diffuse = diffuseColor;
        }

        public override bool IsDelta()
        {
            return false;
        }

        public override BSDFType GetBSDFType()
        {
            return BSDFType.Diffuse | BSDFType.Reflection;
        }

        public override ShadingState GetShading(Ray ray, InterState interState)
        {
            var state = new ShadingState(interState, this, null);

            return state;
        }

        public override Color3 GetReflectance(Vector3 wo)
        {
            return Diffuse;
        }

        public override Color3 GetReflectance()
        {
            return Diffuse;
        }

        public override Color3 f(ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            float dni = Vector3.Dot(shadingState.GeoNormal, wi);
            float dno = Vector3.Dot(shadingState.GeoNormal, wo);

            if (Math.Sign(dni) == Math.Sign(dno))
            {
                return Diffuse / Utils.PI;
            }

            return Color3.Black;
        }

        public override BSDFType SampleBSDF(Sample1 bsdfSample)
        {
            return BSDFType.Diffuse | BSDFType.Reflection;
        }

        public override Color3 Sample(Sample2 dirSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            wi = Utils.CosineSampleHemisphere(shadingState.Normal, dirSample);

            bsdfPdf = Utils.CosineHemispherePDF(Vector3.AbsDot(shadingState.Normal, wi));

            return Diffuse / Utils.PI;
        }

        public override Color3 Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            return Sample(dirSample, shadingState, wo, out wi, out bsdfPdf);
        }

        public override MaterialSampleInfo Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo)
        {
            Vector3 wi;
            float bsdfPdf;
            Color3 fr = Sample(dirSample, shadingState, wo, out wi, out bsdfPdf);

            return new MaterialSampleInfo {sampledType = BSDFType.Diffuse | BSDFType.Reflection, bsdfPdf = bsdfPdf, fr = fr, wi = wi};
        }

        public sealed override MaterialSampleInfo Sample(Sample2 dirSample, BSDFType bsdfType, ShadingState shadingState, Vector3 wo)
        {
            if ((bsdfType & BSDFType.Diffuse) != 0)
            {
                return this.Sample(dirSample, new Sample1 { s = 0.5f }, shadingState, wo);
            } 
            else
            {
                return new MaterialSampleInfo();
            }
        }

        public override float PDF(ShadingState shadingState, Vector3 wo, Vector3 wi)
        {
            float dni = Vector3.Dot(shadingState.GeoNormal, wi);
            float dno = Vector3.Dot(shadingState.GeoNormal, wo);
            
            if (Math.Sign(dni) == Math.Sign(dno))
            {
                return Vector3.AbsDot(shadingState.Normal, wi) / Utils.PI;
            }

            return 0;
        }

        public override float PDF(BSDFType bsdfType)
        {
            return (bsdfType & BSDFType.Diffuse) != 0 ? 1 : 0;
        }
    }
}
