﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;
using Fart.Geometry;
using Fart.Render;

namespace Fart.Shading
{
    public sealed class Phong: Material
    {
        float m_shiness;
        Color3 m_pd, m_ps;
        Color3 m_kd, m_ks;
        float m_dq, m_sq;

        public Phong(Color3 diffuse, Color3 specular, float shiness)
        {
            m_pd = diffuse;
            m_ps = specular;

            m_shiness = shiness;

            // Normalize the diffuse and specular reflection so the energy is conserved.
            float totalInverseAvg = 1 / (m_pd.Average + m_ps.Average);
            if (totalInverseAvg < 1)
            {
                m_pd *= totalInverseAvg;
                m_ps *= totalInverseAvg;
            }

            m_kd = m_pd * Utils.INV_PI;
            m_ks = m_ps * (m_shiness + 2) * Utils.INV_TWO_PI;

            float den = 1.0f / (2 * m_pd.Average + (m_shiness + 2) / (m_shiness + 1) * m_ps.Average);

            m_dq = 2 * m_pd.Average * den;
            m_sq = (m_shiness + 2) / (m_shiness + 1) * m_ps.Average * den;
        }

        public override bool IsDelta()
        {
            return false;
        }

        public override BSDFType GetBSDFType()
        {
            return BSDFType.Glossy | BSDFType.Diffuse | BSDFType.Reflection;
        }

        public override ShadingState GetShading(Ray ray, InterState interState)
        {
            var state = new ShadingState(interState, this, null);

            return state;
        }

        public override Color3 f(ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            float dni = Vector3.Dot(shadingState.GeoNormal, wi);
            float dno = Vector3.Dot(shadingState.GeoNormal, wo);

            if (Math.Sign(dni) == Math.Sign(dno))
            {
                // Calculate specular shading
                Vector3 r = Vector3.Reflect(shadingState.Normal, -wi);

                Color3 sp = m_ks * (float) Math.Pow(Math.Max(0, Vector3.Dot(r, wo)), m_shiness);

                return m_kd + sp;
            }

            return Color3.Black;
        }

        public override Color3 f(BSDFType bsdfType, ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            float dni = Vector3.Dot(shadingState.GeoNormal, wi);
            float dno = Vector3.Dot(shadingState.GeoNormal, wo);

            if (Math.Sign(dni) == Math.Sign(dno))
            {
	            if ((bsdfType & BSDFType.Diffuse) != 0)
	            {
	                return m_kd;
	            }
                else if ((bsdfType & BSDFType.Glossy) != 0)
	            {
	                // Calculate specular shading
                    Vector3 r = Vector3.Reflect(shadingState.Normal, -wi);
                    return m_ks * (float) Math.Pow(Math.Max(0, Vector3.Dot(r, wo)), m_shiness);
	            }
            }

            return Color3.Black;
        }

        public override BSDFType SampleBSDF(Sample1 bsdfSample)
        {
            if (bsdfSample.s < m_dq)
            {
                return BSDFType.Diffuse | BSDFType.Reflection;
            }
            else if (bsdfSample.s <= m_dq + m_sq)
            {
                return BSDFType.Glossy | BSDFType.Reflection;
            }

            return BSDFType.None;
        }

        public override Color3 Sample(Sample2 dirSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            throw new NotImplementedException();
        }

        public override Color3 Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            if (Vector3.Dot(shadingState.GeoNormal, wo) > 0)
            {
                Vector3 normal = shadingState.Normal;

	            if (bsdfSample.s < m_dq)
	            {
	                wi = Utils.CosineSampleHemisphere(normal, dirSample);

                    bsdfPdf = Utils.CosineHemispherePDF(Vector3.AbsDot(normal, wi)) * m_dq;
	
	                return m_kd;
	            }
	            else if (bsdfSample.s <= m_dq + m_sq)
	            {
	                Vector3 v = Vector3.Reflect(normal, -wo);

                    float cosN = (float) Math.Pow(dirSample.s0, 1 / (m_shiness + 1));
	                float thetaS = (float) Math.Acos(cosN);
	                float phiS = 2 * Utils.PI * dirSample.s1;
	
	                wi = Utils.SphericalToDir(v, thetaS, phiS);
                    if (Vector3.Dot(normal, wi) < 0)
                    {
                        wi = Utils.SphericalToDir(v, -thetaS, phiS);
                    }

                    bsdfPdf = (m_shiness + 1) * Utils.INV_TWO_PI * (float) Math.Pow(cosN, m_shiness);
                    bsdfPdf *= m_sq;

                    return m_ks * (float) Math.Pow(cosN, m_shiness);
	            }
            }

            wi = Vector3.Make(0, 0, 0);
            bsdfPdf = 0;

            return Color3.Black;
        }

        public override MaterialSampleInfo Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo)
        {
            BSDFType sampledBSDF = SampleBSDF(bsdfSample);

            return Sample(dirSample, sampledBSDF, shadingState, wo);
        }

        public override MaterialSampleInfo Sample(Sample2 dirSample, BSDFType bsdfType, ShadingState shadingState, Vector3 wo)
        {
            if (Vector3.Dot(shadingState.GeoNormal, wo) > 0)
            {
                Vector3 normal = shadingState.Normal;

                if ((bsdfType & BSDFType.Diffuse) != 0)
	            {
	               Vector3 wi = Utils.CosineSampleHemisphere(normal, dirSample);
	
	               float bsdfPdf = Utils.CosineHemispherePDF(Vector3.AbsDot(normal, wi)) * m_dq;
	
	               return new MaterialSampleInfo { sampledType = BSDFType.Diffuse | BSDFType.Reflection, bsdfPdf = bsdfPdf, fr = m_kd, wi = wi };
	            } 
	            else if ((bsdfType & BSDFType.Glossy) != 0)
	            {
	                Vector3 v = Vector3.Reflect(normal, -wo);
	
	                float cosN = (float) Math.Pow(dirSample.s0, 1 / (m_shiness + 1));
	                float thetaS = (float) Math.Acos(cosN);
	                float phiS = 2 * Utils.PI * dirSample.s1;
	
	                Vector3 wi = Utils.SphericalToDir(v, thetaS, phiS);
	                if (Vector3.Dot(normal, wi) < 0)
	                {
                        return new MaterialSampleInfo();
	                }
	
	                float pcos = (float) Math.Pow(cosN, m_shiness);
	
	                float bsdfPdf = (m_shiness + 1) * Utils.INV_TWO_PI * pcos;
	                bsdfPdf *= m_sq;
	
	                Color3 fr = m_ks * pcos;
	
	                return new MaterialSampleInfo { sampledType = BSDFType.Glossy | BSDFType.Reflection, bsdfPdf = bsdfPdf, fr = fr, wi = wi };
	            }
            }

            return new MaterialSampleInfo();
        }

        public override float PDF(ShadingState shadingState, Vector3 wo, Vector3 wi)
        {
            float dni = Vector3.Dot(shadingState.GeoNormal, wi);
            float dno = Vector3.Dot(shadingState.GeoNormal, wo);

            if (Math.Sign(dni) == Math.Sign(dno))
            {
                float diffusePDF = Utils.CosineHemispherePDF(Vector3.AbsDot(shadingState.Normal, wi)) * m_dq;

                Vector3 r = Vector3.Reflect(shadingState.Normal, -wi);
                float specularPDF = (m_shiness + 1) * Utils.INV_TWO_PI * (float) Math.Pow(Math.Max(0, Vector3.Dot(r, wo)), m_shiness) * m_sq;

                return diffusePDF + specularPDF;
            }

            return 0;
        }

        public override float PDF(BSDFType bsdfType)
        {
            float pdf = 0;

            if ((bsdfType & BSDFType.Diffuse) != 0)
            {
                pdf += m_dq;
            }

            if ((bsdfType & BSDFType.Glossy) != 0)
            {
                pdf += m_sq;
            }

            return pdf;
        }
    }
}
