﻿
using Fart.Geometry;
using Fart.Maths;
using Fart.Scene;

namespace Fart.Shading
{
    public class ShadingState
    {
        private InterState m_interState;
        private Material m_material;   // If the shading state is on a material surface
        private ILight m_light;         // If the shading state is on a light surface

        public ShadingState(InterState interState, Material material, ILight light)
        {
            m_interState = interState;
            m_material = material;
            m_light = light;
        }

        public Vector3 Position
        {
            get
            {
                return m_interState.Position;
            }
        }

        public Vector3 Normal
        {
            get
            {
                return m_interState.ShadingNormal;
            }
        }

        public Vector3 GeoNormal
        {
            get
            {
                return m_interState.GeoNormal;
            }
        }

        public InterState InterState
        {
            get
            {
                return m_interState;
            }
        }

        public Material Material
        {
            get
            {
                return m_material;
            }
        }

        public ILight Light
        {
            get
            {
                return m_light;
            }
        }
    }
}
