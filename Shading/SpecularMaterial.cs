﻿using System;
using System.Collections.Generic;

using Fart.Maths;
using Fart.Geometry;

namespace Fart.Shading
{
    public class SpecularMaterial: Material
    {
        bool m_mirror;

        float m_refractionIndex;

        public SpecularMaterial()
        {
            m_mirror = true;
        }

        public SpecularMaterial(float index)
        {
            m_mirror = false;

            m_refractionIndex = index;
        }

        public sealed override bool IsDelta()
        {
            return true;
        }

        public sealed override BSDFType GetBSDFType()
        {
            return BSDFType.Specular | (m_mirror? BSDFType.Reflection: BSDFType.Transmition);
        }

        public sealed override ShadingState GetShading(Ray ray, InterState interState)
        {
            var state = new ShadingState(interState, this, null);

            return state;
        }

        public sealed override Color3 GetReflectance(Vector3 wo)
        {
            return Color3.Black;
        }

        public sealed override Color3 GetReflectance()
        {
            return Color3.Black;
        }

        public sealed override Color3 f(ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            return Color3.Black;
        }

        public sealed override BSDFType SampleBSDF(Sample1 bsdfSample)
        {
            return this.GetBSDFType();
        }

        public sealed override Color3 Sample(Sample2 s, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            Vector3 normal = shadingState.Normal;
            float c1 = Vector3.Dot(normal, wo);
            bool entering = c1 > 0;

            if (m_mirror)
            {
                if (entering)
	            {
                    wi = Vector3.Reflect(normal, -wo);
                    bsdfPdf = 1;

                    return Color3.Make(0.9f, 0.9f, 0.9f);
	            }
	            else
	            {
                    wi = Vector3.Make(0, 0, 0);
                    bsdfPdf = 0;

                    return Color3.Black;
	            }
            } 
            else
            {
                float n = entering ? 1 / m_refractionIndex : m_refractionIndex;

                c1 = Math.Abs(c1);

                float s2 = n * n * (1 - c1 * c1);
                if (s2 <= 1)
                {
                    float c2 = Utils.Sqrtf(Math.Max(0, 1 - s2));

                    if (!entering)
                    {
                        normal = -normal;
                    }

                    wi = (n * -wo) + (n * c1 - c2) * normal;
                    bsdfPdf = 1;

                    return Color3.Make(0.9f, 0.9f, 0.9f);
                }

                wi = Vector3.Make(0, 0, 0);
                bsdfPdf = 0;

                return Color3.Black;
            }
        }

        public sealed override Color3 Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            return Sample(dirSample, shadingState, wo, out wi, out bsdfPdf);
        }

        public sealed override MaterialSampleInfo Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo)
        {
            Vector3 wi;
            float bsdfPdf;
            Color3 fr = Sample(dirSample, shadingState, wo, out wi, out bsdfPdf);

            return new MaterialSampleInfo { sampledType = this.GetBSDFType(), bsdfPdf = bsdfPdf, fr = fr, wi = wi };
        }

        public sealed override MaterialSampleInfo Sample(Sample2 dirSample, BSDFType bsdfType, ShadingState shadingState, Vector3 wo)
        {
            if ((bsdfType & BSDFType.Specular) != 0)
            {
                return this.Sample(dirSample, new Sample1(0.5f), shadingState, wo);
            } 
            else
            {
                return new MaterialSampleInfo();
            }
        }

        public sealed override float PDF(ShadingState shadingState, Vector3 wo, Vector3 wi)
        {
            return 0;
        }

        public override float PDF(BSDFType bsdfType)
        {
            return (bsdfType & BSDFType.Specular) != 0 ? 1 : 0;
        }
    }
}
