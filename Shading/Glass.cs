﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;
using Fart.Geometry;
using System.Diagnostics;

namespace Fart.Shading
{
    public class Glass: Material
    {
        float m_ior = 1;

        public Glass(float ior)
        {
            m_ior = ior;
        }

        public override bool IsDelta()
        {
            return true;
        }

        public override BSDFType GetBSDFType()
        {
            return BSDFType.Specular | BSDFType.Reflection | BSDFType.Transmition;
        }

        public override ShadingState GetShading(Ray ray, InterState interState)
        {
            return new ShadingState(interState, this, null);
        }

        public override Color3 f(ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            return Color3.Black;
        }

        public sealed override Color3 GetReflectance(Vector3 wo)
        {
            return Color3.Black;
        }

        public sealed override Color3 GetReflectance()
        {
            return Color3.Black;
        }

        public override BSDFType SampleBSDF(Sample1 bsdfSample)
        {
            throw new NotImplementedException();
        }

        public override Color3 Sample(Sample2 dirSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            throw new NotImplementedException();
        }

        public override Color3 Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf)
        {
            throw new NotImplementedException();
        }

        public sealed override MaterialSampleInfo Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo)
        {
            bool inside;
            float fresnel = Dielectric(shadingState.Normal, shadingState.GeoNormal, wo, m_ior, out inside);

            if (!inside && bsdfSample.s < fresnel)
            {
                return new MaterialSampleInfo
                {
                    wi = Vector3.Reflect(shadingState.Normal, -wo),
                    fr = Color3.Make(fresnel, fresnel, fresnel),
                    bsdfPdf = fresnel,
                    sampledType = BSDFType.Specular | BSDFType.Reflection
                };
            }
            else
            {
                float t = 1 - fresnel;

                Vector3 wi;
                if (Refract(shadingState.Normal, shadingState.GeoNormal, wo, m_ior, out wi))
                {
                    return new MaterialSampleInfo
                    {
                        wi = wi,
                        fr = Color3.Make(t, t, t),
                        bsdfPdf = t,
                        sampledType = BSDFType.Specular | BSDFType.Transmition
                    };
                }
            }

            return new MaterialSampleInfo();
        }

        public sealed override MaterialSampleInfo Sample(Sample2 dirSample, BSDFType bsdfType, ShadingState shadingState, Vector3 wo)
        {
            if ((bsdfType & BSDFType.Specular) != 0)
            {
                bool inside;
                float fresnel = Dielectric(shadingState.Normal, shadingState.GeoNormal, wo, m_ior, out inside);
                Debug.Assert(fresnel <= 1 && fresnel >= 0);

                bool doRefl = !inside || fresnel == 1;
                if (doRefl && (bsdfType & BSDFType.Reflection) != 0)
                {
                    return new MaterialSampleInfo
                    {
                        backFaced = inside,
                        wi = Vector3.Reflect(shadingState.Normal, -wo),
                        fr = Color3.Make(fresnel, fresnel, fresnel),
                        bsdfPdf = 1,
                        sampledType = BSDFType.Specular | BSDFType.Reflection
                    };
                }
                else if ((bsdfType & BSDFType.Transmition) != 0)
                {
                    float t = 1 - fresnel;

                    Vector3 wi;
                    if (Refract(shadingState.Normal, shadingState.GeoNormal, wo, m_ior, out wi))
                    {
                        return new MaterialSampleInfo
                        {
                            backFaced = inside,
                            wi = wi,
                            fr = Color3.Make(t, t, t),
                            bsdfPdf = 1,
                            sampledType = BSDFType.Specular | BSDFType.Transmition
                        };
                    }
                }
            }

            return new MaterialSampleInfo();
        }

        static bool Refract(Vector3 normal, Vector3 geoNormal, Vector3 wo, float ior, out Vector3 wi)
        {
            bool entering = Vector3.Dot(geoNormal, wo) > 0;

            float n = entering ? 1 / ior : ior;

            float c1 = Vector3.AbsDot(normal, wo);

            float s2 = n * n * (1 - c1 * c1);
            if (s2 <= 1)
            {
                float c2 = Utils.Sqrtf(1 - s2);

                if (!entering)
                {
                    normal = -normal;
                }

                wi = (-n * wo) + (n * c1 - c2) * normal;
                wi.GetNormalized();

                return true;
            }

            wi = Vector3.Make(0, 0, 0);

            return false; // TIR
        }

        static float Dielectric(Vector3 normal, Vector3 geoNormal, Vector3 wo, float ior, out bool inside)
        {
            float n, n1, n2;
            if (Vector3.Dot(geoNormal, wo) > 0)
            {
                n1 = 1; n2 = ior;

                inside = false;
            }
            else
            {
                n1 = ior; n2 = 1;

                inside = true;
            }
            n = n1 / n2;

            float cosI = Vector3.AbsDot(normal, wo);
            float sinT2 = n * n * (1 - cosI * cosI);
            if (sinT2 > 1)
            {
                return 1; // TIR
            }

            float cosT = Utils.Sqrtf(1 - sinT2);
            float rOrth = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
            float rPar = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);

            return 0.5f * (rOrth * rOrth + rPar * rPar);
        }

        public override float PDF(ShadingState shadingState, Vector3 wo, Vector3 wi)
        {
            throw new NotImplementedException();
        }

        public override float PDF(BSDFType bsdfType)
        {
            throw new NotImplementedException();
        }
    }
}
