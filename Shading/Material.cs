﻿using System;

using Fart.Maths;
using Fart.Geometry;
using Fart.Scene;
using Fart.Render;

namespace Fart.Shading
{
    [Flags]
    public enum BSDFType
    {
        None = 0x0,
        Reflection = 0x1,
        Diffuse = 0x2,
        Transmition = 0x4,
        Specular = 0x8,
        Glossy = 0x10
    }

    public struct MaterialSampleInfo 
    {
        public bool backFaced;
        public BSDFType sampledType;
        public float bsdfPdf;
        public Color3 fr;
        public Vector3 wi;
    }

    public abstract class Material
    {
        public abstract bool IsDelta();

        public abstract BSDFType GetBSDFType();

        public abstract ShadingState GetShading(Ray ray, InterState interState);

        public virtual Color3 GetReflectance(Vector3 wo)
        {
            Color3 rho = Color3.Black;

            ShadingState sState = new ShadingState(new InterState(Vector3.Make(0, 1, 0)), this, null);
            SampleState sampleState = new SampleState(2, 0, 0, 0);
            for (int i = 0; i < 16; i++)
            {
                var splittedState = sampleState.SplitTrajectory(i, 16);
                var matInfo = Sample(sampleState.PopSample2D(), sampleState.PopSample1D(), sState, wo);

                if (matInfo.bsdfPdf > 0)
                {
                    rho += matInfo.fr * Math.Abs(matInfo.wi.y) / matInfo.bsdfPdf;
                }
            }

            return rho / (Utils.PI * 16);
        }

        public virtual Color3 GetReflectance()
        {
            Color3 rho = Color3.Black;

            float pdfO = Utils.UniformHemispherePDF();

            ShadingState sState = new ShadingState(new InterState(Vector3.Make(0, 1, 0)), this, null);
            SampleState sampleState = new SampleState(2, 0, 0, 0);
            for (int i = 0; i < 16; i++)
            {
                var splittedState = sampleState.SplitTrajectory(i, 16);

                Vector3 wo = Utils.UniformSampleHemisphere(sampleState.PopSample2D());
                var matInfo = Sample(sampleState.PopSample2D(), sampleState.PopSample1D(), sState, wo);

                if (matInfo.bsdfPdf > 0)
                {
                    rho += matInfo.fr * Math.Abs(matInfo.wi.z * wo.z) / (matInfo.bsdfPdf * pdfO);
                }
            }

            return rho / (Utils.PI * 16);
        }


        public abstract Color3 f(ShadingState shadingState, Vector3 wi, Vector3 wo);

        public virtual Color3 f(BSDFType bsdfType, ShadingState shadingState, Vector3 wi, Vector3 wo)
        {
            return f(shadingState, wi, wo);
        }

        public abstract BSDFType SampleBSDF(Sample1 bsdfSample);

        public abstract Color3 Sample(Sample2 dirSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf);

        public abstract Color3 Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo, out Vector3 wi, out float bsdfPdf);

        public abstract MaterialSampleInfo Sample(Sample2 dirSample, Sample1 bsdfSample, ShadingState shadingState, Vector3 wo);

        public virtual MaterialSampleInfo Sample(Sample2 dirSample, BSDFType bsdfType, ShadingState shadingState, Vector3 wo)
        {
            return Sample(dirSample, new Sample1 { s = 0.5f }, shadingState, wo);
        }

        public abstract float PDF(ShadingState shadingState, Vector3 wo, Vector3 wi);

        public abstract float PDF(BSDFType bsdfType);
    }
}
