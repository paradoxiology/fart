﻿using System;
using System.Collections.Generic;

using Fart.Maths;
using FreeImageAPI;

namespace Fart.Shading
{
    public class Texture: IDisposable
    {
        private string m_filename;
        private uint m_image;
        private uint m_width, m_height;

        public enum FilterType: int
        {
            POINT,
            BILINEAR
        }

        public Texture(string filename)
        {
            uint image = FreeImage.Load(Format.Choose(filename), filename, 0);
            if (image == 0)
            {
                throw new Exception(String.Format("File \"{0}\" can not be loaded as an Image.", filename));
            }

            if (FreeImage.GetImageType(image) != FREE_IMAGE_TYPE.FIT_BITMAP)
            {
                throw new Exception(String.Format("File \"{0}\" can not be loaded as an Bitmap Image.", filename));
            }

            m_image = image;
            m_filename = filename;
            m_width = FreeImage.GetWidth(m_image);
            m_height = FreeImage.GetHeight(m_image);
        }

        public Color3 GetTexel(Vector2 uv, FilterType filterType)
        {
            if (filterType == FilterType.BILINEAR)
            {
                return GetTexelBilinear(uv);
            }
            else
            {
                return GetTexelPoint(uv);
            }
        }

        private Color3 GetTexelPoint(Vector2 uv)
        {
            RGBQUAD c = new RGBQUAD();

            uint x = (uint)Math.Floor(uv.x * m_width);
            uint y = (uint)Math.Floor((1.0f - uv.y) * m_height);

            FreeImage.GetPixelColor(m_image, x, y, c);

            return ConvertToColor3(c);
        }


        private Color3 GetTexelBilinear(Vector2 uv)
        {
            RGBQUAD c00 = new RGBQUAD();
            RGBQUAD c01 = new RGBQUAD();
            RGBQUAD c10 = new RGBQUAD();
            RGBQUAD c11 = new RGBQUAD();

            float pixelX = uv.x * m_width;
            float pixelY = (1.0f - uv.y) * m_height;

            uint x0 = (uint)Math.Floor(pixelX);
            uint y0 = (uint)Math.Floor(pixelY);
            uint x1 = (uint)Math.Ceiling(pixelX);
            uint y1 = (uint)Math.Ceiling(pixelY);

            float xLerp = pixelX - (float)Math.Truncate(pixelX);
            float yLerp = pixelY - (float)Math.Truncate(pixelY);

            FreeImage.GetPixelColor(m_image, x0, y0, c00);
            FreeImage.GetPixelColor(m_image, x0, y1, c01);
            FreeImage.GetPixelColor(m_image, x1, y0, c10);
            FreeImage.GetPixelColor(m_image, x1, y1, c11);

            Color3 color00 = ConvertToColor3(c00);
            Color3 color01 = ConvertToColor3(c01);
            Color3 color10 = ConvertToColor3(c10);
            Color3 color11 = ConvertToColor3(c11);
            Color3 finalColor = color00 * ((1.0f - xLerp) * (1.0f - yLerp)) +
                                color10 * (xLerp * (1.0f - yLerp)) +
                                color01 * ((1.0f - xLerp) * yLerp) +
                                color11 * (xLerp * yLerp);

            return finalColor;
        }


        public void Dispose()
        {
            FreeImage.Unload(m_image);
        }

        private static Color3 ConvertToColor3(RGBQUAD c)
        {
            return Color3.Make(c.rgbRed / 256.0f, c.rgbGreen / 256.0f, c.rgbBlue / 256.0f);
        }

        public override bool Equals(object obj)
        {
            Texture other = obj as Texture;
            if (other != null)
            {
                return m_filename == other.m_filename;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return m_filename.GetHashCode();
        }


        internal static class Format
        {
            private static Dictionary<string, FREE_IMAGE_FORMAT> s_formats = new Dictionary<string, FREE_IMAGE_FORMAT>();

            static Format()
            {
                s_formats.Add("bmp", FREE_IMAGE_FORMAT.FIF_BMP);
                s_formats.Add("dds", FREE_IMAGE_FORMAT.FIF_DDS);
                s_formats.Add("jpg", FREE_IMAGE_FORMAT.FIF_JPEG);
                s_formats.Add("png", FREE_IMAGE_FORMAT.FIF_PNG);
                s_formats.Add("tga", FREE_IMAGE_FORMAT.FIF_TARGA);
            }

            public static FREE_IMAGE_FORMAT Choose(string filename)
            {
                string extension = ExtractExt(filename);
                FREE_IMAGE_FORMAT imageFormat;
                if (extension != null && s_formats.TryGetValue(extension, out imageFormat))
                {
                    return imageFormat;
                }

                if (extension != null)
                    throw new InvalidOperationException(String.Format("Image Format \"{0}\" is not supported", extension));
                else
                    throw new InvalidOperationException(String.Format("Filename \"{0}\" is invalid.", filename));
            }

            private static string ExtractExt(string filename)
            {
                int dotPos = filename.LastIndexOf('.');
                if (dotPos < 0 || dotPos + 1 >= filename.Length)
                    return null;

                return filename.Substring(dotPos + 1).ToLower();
            }

        }

    }

}
