﻿
using System;

namespace Fart.Maths
{
    public static class Utils
    {
        public const float PI = 3.14159265358979323846f;
        public const float TWO_PI = 2 * PI;
        public const float INV_PI = 1 / PI;
        public const float INV_TWO_PI = 1 / TWO_PI;

        public const float EPISLON = 0.000001f;

        public static float Sqrtf(float value)
        {
            return (float)Math.Sqrt(value);
        }

        public static float Fabs(float value)
        {
            return (float)Math.Abs(value);
        }

        public static float Sinf(float value)
        {
            return (float)Math.Sin(value);
        }

        public static float Cosf(float value)
        {
            return (float)Math.Cos(value);
        }

        public static float Expf(float value)
        {
            return (float)Math.Exp(value);
        }

        public static float Decimal(float value)
        {
            return value - (float) Math.Truncate(value);
        }

        public static double Decimal(double value)
        {
            return value - Math.Truncate(value);
        }

        public static float SmoothStep(float a, float b, float x)
        {
            if (x < a) return 0.0f;
            else if (x >= b) return 1.0f;

            x = (x - a) / (b - a);
            return (x * x * (3.0f - 2.0f * x));
        }

        public static float SimpsonKernel(float d, float maxD)
        {
            float s = 1 - d / maxD;

            return 3.0f / (maxD * Utils.PI) * s * s;
        }

        // Compute the piecewise CDF from the given pdf
        public static float[] CreateCDF(float[] f, out float total)
        {
            if (f.Length == 0)
            {
                throw new ArgumentException("Empty probability array");
            }

            float[] cdf = new float[f.Length + 1];
            cdf[0] = 0;

            float delta = 1.0f / f.Length;
            for (int i = 0; i < f.Length; i++ )
            {
                if (f[i] < 0)
                {
                    throw new ArgumentException("Probabilities must not be less than zero");
                }

                cdf[i + 1] = cdf[i] + f[i] * delta;
            }

            // Normalize the CDF
            float c = cdf[cdf.Length - 1];
            for (int i = 1; i < cdf.Length; i++)
            {
                cdf[i] /= c;
            }

            total = c * f.Length;

            return cdf;            
        }

        // Use the Inversion Method to draw a sample from the given CDF.
        public static float SampleCDF(float[]f, float[] cdf, float total, Sample1 s, out float pdf)
        {
            int index = Array.BinarySearch(cdf, s.s);
            index = (index < 0 ? ~index : index) - 1;

            pdf = f[index] / total;

            float offset = (s.s - cdf[index]) / (cdf[index + 1] - cdf[index]);

            return (index + offset) / f.Length;
        }

        public static float PowerHeuristic(int nf, float fPdf, int ng, float gPdf)
        {
            float f = nf * fPdf, g = ng * gPdf;

            return (f * f) / (f * f + g * g);
        }

        public static void CoordSystem(Vector3 v0, out Vector3 v1, out Vector3 v2)
        {
            if (Fabs(v0.x) > Fabs(v0.y))
            {
                float invLen = 1.0f / Sqrtf(v0.x * v0.x + v0.z * v0.z);
                v1 = Vector3.Make(-v0.z * invLen, 0.0f, v0.x * invLen);
            } 
            else
            {
                float invLen = 1.0f / Sqrtf(v0.y * v0.y + v0.z * v0.z);
                v1 = Vector3.Make(0.0f, v0.z * invLen, -v0.y * invLen);
            }

            v2 = Vector3.Cross(v0, v1);
        }

        public static Vector3 SphericalToDir(float theta, float phi)
        {
            float sinTheta = Utils.Sinf(theta);
            float cosTheta = Utils.Cosf(theta);
            float sinPhi = Utils.Sinf(phi);
            float cosPhi = Utils.Cosf(phi);

            return Vector3.Make(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        public static Vector3 SphericalToDir(Vector3 normal, float theta, float phi)
        {
            Vector3 tangent, bitangent;
            Utils.CoordSystem(normal, out tangent, out bitangent);

            float sinTheta = Utils.Sinf(theta);
            float cosTheta = Utils.Cosf(theta);
            float sinPhi = Utils.Sinf(phi);
            float cosPhi = Utils.Cosf(phi);

            return tangent * (sinTheta * cosPhi) + bitangent * (sinTheta * sinPhi) + normal * cosTheta;
        }

        public static void DirToSpherical(Vector3 dir, out float theta, out float phi)
        {
            theta = (float) Math.Acos(dir.z);

            phi = (float) Math.Atan2(dir.y, dir.x);
        }

        public static void DirToSpherical(Vector3 normal, Vector3 dir, out float theta, out float phi)
        {
            Vector3 tangent, bitangent;
            CoordSystem(normal, out tangent, out bitangent);

            var transformed = Vector3.Make(tangent.x * dir.x + tangent.y * dir.y + tangent.z * dir.z,
                                           bitangent.x * dir.x + bitangent.y * dir.y + bitangent.z * dir.z,
                                           normal.x * dir.x + normal.y * dir.y + normal.z * dir.z);

            DirToSpherical(transformed.GetNormalized(), out theta, out phi);
        }

        // Uniformly distributed spherical point(direction).
        // See http://www.math.niu.edu/~rusin/known-math/96/sph.rand for details
        public static Vector3 UniformRandDir(Sample2 s)
        {
            double z = s.s0 * 2.0 - 1.0; // Range: [-1, 1]
            double t = s.s1 * 2.0 * Math.PI;
            double r = Math.Sqrt(1.0 - z * z);

            double x = r * Math.Cos(t);
            double y = r * Math.Sin(t);

            return Vector3.Make((float)x, (float)y, (float)z);
        }

        public static float UniformDirPDF()
        {
            return 1.0f / (4 * PI);
        }

        public static Vector3 UniformSampleCone(Sample2 s, float cosThetaMax)
        {
            float cosTheta = (1 - s.s0) * cosThetaMax + s.s0;
            float sinTheta = Sqrtf(1 - cosTheta * cosTheta);
            float phi = s.s1 * 2 * PI;

            return new Vector3(Cosf(phi) * sinTheta, Sinf(phi) * sinTheta, cosTheta);
        }

        public static Vector3 UniformSampleCone(Sample2 s, float cosThetaMax, Vector3 e0, Vector3 e1, Vector3 e2)
        {
            float cosTheta = (1 - s.s0) * cosThetaMax + s.s0;
            float sinTheta = Sqrtf(1 - cosTheta * cosTheta);
            float phi = s.s1 * 2 * PI;

            return e0 * Cosf(phi) * sinTheta + e1 * Sinf(phi) * sinTheta + e2 * cosTheta;
        }

        public static float UniformConePDF(float cosThetaMax)
        {
            return 1.0f / (2.0f * PI * (1.0f - cosThetaMax));
        }

        public static void ConcentricSampleDisk(float u1, float u2, out float dx, out float dy)
        {
            float r, theta;
            // Map uniform random numbers to [-1 , 1]^2
            float sx = 2 * u1 - 1;
            float sy = 2 * u2 - 1;

            // Map square to (r, theta)
            // Handle degeneracy at the origin
            if (sx == 0.0f && sy == 0.0f)
            {
                dx = 0.0f;
                dy = 0.0f;

                return;
            }
            if (sx >= -sy)
            {
                if (sx > sy)
                {
                    // Handle first region of disk
                    r = sx;
                    if (sy > 0.0f)
                        theta = sy / r;
                    else
                        theta = 8.0f + sy / r;
                }
                else
                {
                    // Handle second region of disk
                    r = sy;
                    theta = 2.0f - sx / r;
                }
            }
            else
            {
                if (sx <= sy)
                {
                    // Handle third region of disk
                    r = -sx;
                    theta = 4.0f - sy / r;
                }
                else
                {
                    // Handle fourth region of disk
                    r = -sy;
                    theta = 6.0f + sx / r;
                }
            }

            theta *= PI / 4.0f;
            dx = r * Cosf(theta);
            dy = r * Sinf(theta);
        }

        public static Vector3 CosineSampleHemisphere(Sample2 sample)
        {
            Vector3 ret = new Vector3();
            
            ConcentricSampleDisk(sample.s0, sample.s1, out ret.x, out ret.y);
            ret.z = Sqrtf(Math.Max(0, 1 - ret.x * ret.x - ret.y * ret.y));

            return ret;
        }

        public static Vector3 CosineSampleHemisphere(Vector3 normal, Sample2 sample)
        {
            Vector3 dir = CosineSampleHemisphere(sample);

            Vector3 tangent, bitangent;
            CoordSystem(normal, out tangent, out bitangent);

            return Vector3.Make(tangent.x * dir.x + bitangent.x * dir.y + normal.x * dir.z,
                                tangent.y * dir.x + bitangent.y * dir.y + normal.y * dir.z,
                                tangent.z * dir.x + bitangent.z * dir.y + normal.z * dir.z);
        }

        public static float CosineHemispherePDF(float costheta)
        {
            return costheta * INV_PI;
        }

        public static Vector3 UniformSampleHemisphere(Sample2 sample)
        {
            float z = sample.s0;
            float r = Utils.Sqrtf(Math.Max(0, 1 - z * z));

            float phi = 2 * PI * sample.s1;
            float x = r * Cosf(phi);
            float y = r * Sinf(phi);

            return Vector3.Make(x, y, z);
        }

        public static float UniformHemispherePDF()
        {
            return INV_PI / 2;
        }
    }
}
