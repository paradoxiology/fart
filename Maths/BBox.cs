﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fart.Maths
{
    public struct BBox
    {
        public Vector3 Lower;
        public Vector3 Upper;

        public BBox(Vector3 lower, Vector3 upper)
        {
            Lower = lower;
            Upper = upper;
        }

        public BBox(Vector3 center, float extent)
        {
            Vector3 extents = Vector3.Make(extent, extent, extent);;

            Lower = center - extents;
            Upper = center + extents;
        }

        public static BBox Create()
        {
            return new BBox(Vector3.Make(float.MaxValue, float.MaxValue, float.MaxValue),
                            Vector3.Make(float.MinValue, float.MinValue, float.MinValue));
        }

        public void UnionPoint(Vector3 point)
        {
            Vector3 lowerTemp = Lower;
            Vector3 upperTemp = Upper;

            for (int axis = 0; axis < 3; axis++)
            {
            	if (Lower[axis] > point[axis])
            	{
                    lowerTemp[axis] = point[axis];
            	}

                if (Upper[axis] < point[axis])
            	{
                    upperTemp[axis] = point[axis];
            	}
            }

            Lower = lowerTemp;
            Upper = upperTemp;
        }

        public void UnionPoints(Vector3[] points)
        {
            foreach (var point in points)
            {
                UnionPoint(point);
            }
        }

        public void Union(BBox other)
        {
            UnionPoint(other.Lower);
            UnionPoint(other.Upper);
        }

        public BBox MakeCube()
        {
            Vector3 extents = Extents;

            float maxExtent = Math.Max(extents[0], Math.Max(extents[1], extents[2]));

            return new BBox(Center, maxExtent);
        }

        public bool Split(int axis, float s, out BBox left, out BBox right)
        {
            if (Upper[axis] >= s && Lower[axis] <= s)
            {
	            Vector3 interUpper = Upper;
	            interUpper[axis] = s;
	
	            Vector3 interLower = Lower;
	            interLower[axis] = s;
	
	            left = new BBox(Lower, interUpper);
	            right = new BBox(interLower, Upper);

                return true;
            }

            left = right = new BBox();
            return false;
        }

        public bool Contains(Vector3 point)
        {
            return !(point.x < Lower.x ||
                     point.y < Lower.y ||
                     point.z < Lower.z ||
                     point.x > Upper.x ||
                     point.y > Upper.y ||
                     point.z > Upper.z);
        }

        public bool OverlapSphere(Vector3 center, float radius)
        {
            // Use Separate Axis Theorem to determine if the sphere is overlapping with the BBox
            float dmin = 0;

            if (center.x < Lower.x)
            {
                dmin += (center.x - Lower.x) * (center.x - Lower.x);
            }
            else if (center.x > Upper.x)
            {
                dmin += (center.x - Upper.x) * (center.x - Upper.x);
            }

            if (center.y < Lower.y)
            {
                dmin += (center.y - Lower.y) * (center.y - Lower.y);
            }
            else if (center.y > Upper.y)
            {
                dmin += (center.y - Upper.y) * (center.y - Upper.y);
            }

            if (center.z < Lower.z)
            {
                dmin += (center.z - Lower.z) * (center.z - Lower.z);
            }
            else if (center.z > Upper.z)
            {
                dmin += (center.z - Upper.z) * (center.z - Upper.z);
            }

            return dmin < radius * radius;
        }

        public Vector3 Center
        {
            get
            {
                return (Lower + Upper) * 0.5f;
            }
        }

        public Vector3 Extents
        {
            get
            {
                return (Upper - Lower) * 0.5f;
            }
        }

        public Vector3 Size
        {
            get
            {
                return Upper - Lower;
            }
        }

        public float SquaredRadius
        {
            get
            {
                return (Upper - Lower).SquaredLength * 0.25f;
            }
        }

        public float Radius
        {
            get
            {
                return (Upper - Lower).Length * 0.5f;
            }
        }

        public float Volume
        {
            get
            {
                Vector3 dimentions = Upper - Lower;

                return dimentions.x * dimentions.y * dimentions.z;
            }
        }
    }
}
