﻿using System;

namespace Fart.Maths
{
    public sealed class RegularSpectralCurve: SpectralCurve
    {
        float[] m_spectrum;
        float m_minLambda, m_maxLambda;
        float m_delta;

        public RegularSpectralCurve(float[] spectrum, float minLambda, float maxLambda)
        {
            if (spectrum == null || spectrum.Length == 0)
            {
                throw new ArgumentException("Empty Spectrum.");
            }

            if (maxLambda < minLambda)
            {
                throw new ArgumentException("Max Lambda should always be greater than or equal to Min Lambda");
            }

            m_spectrum = spectrum;
            m_minLambda = minLambda;
            m_maxLambda = maxLambda;

            m_delta = (maxLambda - minLambda) / (spectrum.Length - 1);
        }

        public override float Sample(float lambda)
        {
            if (lambda < m_minLambda || lambda > m_maxLambda)
            {
                return 0;
            }

            float x = (lambda - m_minLambda) / m_delta;
            int a = (int) x;
            int b = Math.Min(m_spectrum.Length - 1, a + 1);

            float t = x - a;
            return m_spectrum[a] * (1 - t) + m_spectrum[b] * t;
        }
    }
}
