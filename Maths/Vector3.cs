﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace Fart.Maths
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector3
    {
        public float x, y, z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 Make(float x, float y, float z)
        {
            return new Vector3(x, y, z);
        }

        public float Length
        {
            get
            {
                return Utils.Sqrtf(x * x + y * y + z * z);
            }
        }

        public float SquaredLength
        {
            get
            {
                return x * x + y * y + z * z;
            }
        }

        public float this[int index]
        {
            set
            {
                Debug.Assert(index >= 0 && index < 3, "Attempt to access Vector3 linear indexer out of bounds.");

                unsafe
                {
                    fixed (float* pVec = &this.x)
                    {
                        *(pVec + index) = value;
                    }
                }
            }

            get
            {
                Debug.Assert(index >= 0 && index < 3, "Attempt to access Vector3 linear indexer out of bounds.");

                unsafe
                {
                    fixed (float* pVec = &this.x)
                    {
                        return *(pVec + index);
                    }
                }
            }
        }

        // Notice the arrows:
        //          normal
        //       \    ^   /> outgoing
        //        \   |  /
        //         \  | /
        // incoming \>|/
        public static Vector3 Reflect(Vector3 normal, Vector3 incoming)
        {
	        float LN2 = 2.0f * Dot(normal, incoming);
	        Vector3 LN2normal = Mul(normal, LN2);
	        Vector3 outgoing = incoming - LN2normal;
            return outgoing;
        }

        public static float SquaredDist(Vector3 v0, Vector3 v1)
        {
            float xDiff = v0.x - v1.x;
            float yDiff = v0.y - v1.y;
            float zDiff = v0.z - v1.z;

            return (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
        }

        public Vector3 GetNormalized()
        {
            float length = Length;

            x /= length;
            y /= length;
            z /= length;

            return this;
        }

        public static Vector3 Normalize(Vector3 v)
        {
            float length = v.Length;

            return new Vector3(v.x / length, v.y / length, v.z / length);
        }

        public static Vector3 operator+(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
        }

        public static Vector3 Subtract(Vector3 v0, Vector3 v1)
        {
            return new Vector3(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z);
        }

        public static Vector3 operator-(Vector3 lhs, Vector3 rhs)
        {
            return Subtract(lhs, rhs);
        }

        public static Vector3 Mul(Vector3 v, float t)
        {
            return new Vector3(v.x * t, v.y * t, v.z * t);
        }

        public static Vector3 operator*(Vector3 lhs, float rhs)
        {
            return Mul(lhs, rhs);
        }

        public static Vector3 operator*(float lhs, Vector3 rhs)
        {
            return Mul(rhs, lhs);
        }


        public static float Dot(Vector3 v0, Vector3 v1)
        {
            return (v0.x * v1.x + v0.y * v1.y + v0.z * v1.z);
        }

        public static float AbsDot(Vector3 v0, Vector3 v1)
        {
            return Utils.Fabs(Dot(v0, v1));
        }

        public static float operator*(Vector3 lhs, Vector3 rhs)
        {
            return Dot(lhs, rhs);
        }

        public static Vector3 Cross(Vector3 v0, Vector3 v1)
        {
            return new Vector3((v0.y * v1.z) - (v0.z * v1.y),
                                (v0.z * v1.x) - (v0.x * v1.z),
                                (v0.x * v1.y) - (v0.y * v1.x));
        }

        public static Vector3 operator%(Vector3 lhs, Vector3 rhs)
        {
            return Cross(lhs, rhs);
        }

        public static Vector3 operator-(Vector3 v)
        {
            // Not using explicit negation(-x) is because it can generate negative 0 when divided will produce -Infinity which
            // can cause invalid BIH Tree traversal.
            return new Vector3(0 - v.x, 0 - v.y, 0 - v.z);
        }

        public override string ToString()
        {
            return String.Format("({0}, {1}, {2})", x, y, z);
        }
    }
}
