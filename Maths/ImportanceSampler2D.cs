﻿using System;

namespace Fart.Maths
{
    public sealed class ImportanceSampler2D
    {
        ImportanceSampler1D m_marginalSamplers;
        ImportanceSampler1D[] m_conditionalSamplers;

        public ImportanceSampler2D(float[][] f)
        {
            float[] colSums = new float[f.Length];

            m_conditionalSamplers = new ImportanceSampler1D[f.Length];

            for (int u = 0; u < f.Length; u++)
            {
                m_conditionalSamplers[u] = new ImportanceSampler1D(f[u]);

                colSums[u] = m_conditionalSamplers[u].Sum;                
            }

            m_marginalSamplers = new ImportanceSampler1D(colSums);
        }

        public Vector2 Sample(Sample2 s, out float pdf)
        {
            float pdfU;
            float u = m_marginalSamplers.Sample(new Sample1(s.s0), out pdfU);

            int rowIndex = (int) Math.Min(m_conditionalSamplers.Length - 1, u * m_conditionalSamplers.Length);
            var rowSampler = m_conditionalSamplers[rowIndex];

            float pdfV;
            float v = rowSampler.Sample(new Sample1(s.s1), out pdfV);

            pdf = pdfU * pdfV;

            return Vector2.Make(u, v);
        }

        public float PDF(int uIndex, int vIndex)
        {
            float pdfU = m_marginalSamplers.PDF(uIndex);
            float pdfV = m_conditionalSamplers[uIndex].PDF(vIndex);

            return pdfU * pdfV;
        }
    }
}
