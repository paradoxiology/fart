﻿
namespace Fart.Maths
{
    public static class QMC
    {
        public const int MAX_ORDER = 15;

        private const int NUM = 128;
        private static int[][] s_sigma = new int[NUM][];
        private static int[] s_primes = new int[NUM];

        static QMC()
        {
            s_primes[0] = 2;
            for (int i = 1; i < s_primes.Length; i++)
            {
                s_primes[i] = NextPrime(s_primes[i - 1]);
            }

            int[][] table = new int[s_primes[s_primes.Length - 1] + 1][];
            table[2] = new int[2];
            table[2][0] = 0;
            table[2][1] = 1;

            for (int i = 3; i <= s_primes[s_primes.Length - 1]; i++)
            {
                table[i] = new int[i];
                if ((i & 1) == 0)
                {
                    int[] prev = table[i >> 1];
                    for (int j = 0; j < prev.Length; j++)
                    {
                        table[i][j] = 2 * prev[j];
                    }

                    for (int j = 0; j < prev.Length; j++)
                    {
                        table[i][prev.Length + j] = 2 * prev[j] + 1;
                    }
                }
                else
                {
                    int[] prev = table[i - 1];
                    int med = (i - 1) >> 1;
                    for (int j = 0; j < med; j++)
                    {
                        table[i][j] = prev[j] + ((prev[j] >= med) ? 1 : 0);
                    }

                    table[i][med] = med;
                    for (int j = 0; j < med; j++)
                    {
                        table[i][med + j + 1] = prev[j + med] + ((prev[j + med] >= med) ? 1 : 0);
                    }
                }
            }

            for (int i = 0; i < s_primes.Length; i++)
            {
                int p = s_primes[i];

                s_sigma[i] = table[p];
            }
        }

        public static double GetHalton(int dim, uint i)
        {
            switch (dim)
            {
            case 0:
                {
                    i = (i << 16) | (i >> 16);
                    i = ((i & 0x00ff00ff) << 8) | ((i & 0xff00ff00) >> 8);
                    i = ((i & 0x0f0f0f0f) << 4) | ((i & 0xf0f0f0f0) >> 4);
                    i = ((i & 0x33333333) << 2) | ((i & 0xcccccccc) >> 2);
                    i = ((i & 0x55555555) << 1) | ((i & 0xaaaaaaaa) >> 1);

                    return (double) (i & 0xFFFFFFFFL) / (double) 0x100000000L;
                }
            case 1:
                {
                    double v = 0;
                    double inv = 1.0 / 3;
                    double p;
                    uint n;
                    for (p = inv, n = i; n != 0; p *= inv, n /= 3)
                    {
                        v += (n % 3) * p;
                    }

                    return v;
                }
            default:
                {
                    uint rd_base = (uint) s_primes[dim];
                    int[] scramble = s_sigma[dim];
                    double v = 0;
                    double inv = 1.0 / rd_base;
                    double p;
                    uint n;
                    for (p = inv, n = i; n != 0; p *= inv, n /= rd_base)
                    {
                        v += scramble[n % rd_base] * p;
                    }

                    return v;
                }
            }
        }

        public static double GetHammersley(int dim, uint i, int m)
        {
            if (dim == 0)
            {
                return (double) i / m;
            }
            else
            {
                return GetHalton(dim - 1, i);
            }
        }

        public static uint GenerateSeed(int sx, int sy, int order)
        {
            int j = sx & ((1 << order) - 1);
            int k = sy & ((1 << order) - 1);

            uint s = GetSigma((uint) k, order);
            uint i = (uint) (j << order) + s;
            return i;
        }

        public static uint GetSigma(uint i, int order)
        {
            i = (i << 16) | (i >> 16);
            i = ((i & 0x00ff00ff) << 8) | ((i & 0xff00ff00) >> 8);
            i = ((i & 0x0f0f0f0f) << 4) | ((i & 0xf0f0f0f0) >> 4);
            i = ((i & 0x33333333) << 2) | ((i & 0xcccccccc) >> 2);
            i = ((i & 0x55555555) << 1) | ((i & 0xaaaaaaaa) >> 1);

            return i >> (32 - order);
        }

        private static int NextPrime(int p)
        {
            p = p + (p & 1) + 1;
            while (true)
            {
                int div = 3;
                bool isPrime = true;
                while (isPrime && ((div * div) <= p))
                {
                    isPrime = ((p % div) != 0);
                    div += 2;
                }

                if (isPrime)
                {
                    return p;
                }

                p += 2;
            }
        }
    }
}
