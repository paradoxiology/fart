﻿using System;

namespace Fart.Maths
{
    /**
     * This class allows spectral curves to be defined from irregularly sampled
     * data. Note that the wavelength array is assumed to be sorted low to high. Any
     * values beyond the defined range will simply be extended to infinity from the
     * end points. Points inside the valid range will be linearly interpolated
     * between the two nearest samples. No explicit error checking is performed, but
     * this class will run into IndexOutOfBound exception if the
     * array lengths don't match.
     */
    public sealed class IrregularSpectralCurve: SpectralCurve
    {
        float[] m_waveLengths;
        float[] m_amplitudes;

        public IrregularSpectralCurve(float[] waveLengths, float[] amplitudes)
        {
            m_waveLengths = waveLengths;
            m_amplitudes = amplitudes;

            if (m_waveLengths.Length != m_amplitudes.Length)
            {
                throw new ArgumentException("The lengths of wavelength and amplitude data don't match.");
            }
        }

        public override float Sample(float lambda)
        {
            if (m_waveLengths.Length == 0)
            {
                return 0;
            }

            if (m_waveLengths.Length == 1 || lambda < m_waveLengths[0])
            {
                return m_amplitudes[0];
            }

            int index = Array.BinarySearch(m_waveLengths, lambda);
            index = (index < 0 ? ~index : index) - 1;
            if (index < m_waveLengths.Length - 1)
            {
                float t = (lambda - m_waveLengths[index]) / (m_waveLengths[index + 1] - m_waveLengths[index]);

                return m_amplitudes[index] * (1 - t) + m_amplitudes[index + 1] * t;
            }
            else
            {
                return m_amplitudes[m_amplitudes.Length - 1];
            }
        }
    }
}
