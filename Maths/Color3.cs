﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fart.Maths
{
    public struct Color3: IEquatable<Color3>
    {
        public float r, g, b;

        public static readonly Color3 Black = Color3.Make(0.0f, 0.0f, 0.0f);
        public static readonly Color3 White = Color3.Make(1.0f, 1.0f, 1.0f);

        public float Intensity
        {
            get
            {
                return 0.3f * r + 0.59f * g + 0.11f * b;
            }
        }

        public float Average
        {
            get
            {
                return (r + g + b) / 3;
            }
        }

        public Color3(float r, float g, float b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public static Color3 Make(float r, float g, float b)
        {
            return new Color3(r, g, b);
        }

        public Color3 Saturate()
        {
            r = (r < 0.0f) ? 0.0f : ((r > 1.0f) ? 1.0f : r);
            g = (g < 0.0f) ? 0.0f : ((g > 1.0f) ? 1.0f : g);
            b = (b < 0.0f) ? 0.0f : ((b > 1.0f) ? 1.0f : b);

            return this;
        }


        public static float Dot(Color3 lhs, Color3 rhs)
        {
            return lhs.r * rhs.r + lhs.g * rhs.g + lhs.b * rhs.b;
        }

        public static Color3 operator+(Color3 lhs, Color3 rhs)
        {
            return new Color3(lhs.r + rhs.r, lhs.g + rhs.g, lhs.b + rhs.b);
        }

        public static Color3 operator-(Color3 lhs, Color3 rhs)
        {
            return new Color3(lhs.r - rhs.r, lhs.g - rhs.g, lhs.b - rhs.b);
        }

        public static Color3 operator*(Color3 lhs, Color3 rhs)
        {
            return new Color3(lhs.r * rhs.r, lhs.g * rhs.g, lhs.b * rhs.b);
        }

        public static Color3 operator *(Color3 color, float factor)
        {
            return new Color3(color.r * factor, color.g * factor, color.b * factor);
        }

        public static Color3 operator/(Color3 color, float scale)
        {
            return color * (1.0f / scale);
        }

        public static Color3 operator/(Color3 lhs, Color3 rhs)
        {
            return new Color3(lhs.r / rhs.r, lhs.g / rhs.g, lhs.b / rhs.b);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", r, g, b);
        }

        public bool Equals(Color3 other)
        {
            return r == other.r && g == other.g && b == other.b;
        }

        public static bool operator==(Color3 lhs, Color3 rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator!=(Color3 lhs, Color3 rhs)
        {
            return !lhs.Equals(rhs);
        }

        public override bool Equals(object obj)
        {
            if (obj is Color3)
            {
                return Equals((Color3) obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return r.GetHashCode() + b.GetHashCode() + g.GetHashCode();
        }
    }
}
