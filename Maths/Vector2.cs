﻿
using System;

namespace Fart.Maths
{
    public struct Vector2: IEquatable<Vector2>
    {
        public float x, y;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 Make(float x, float y)
        {
            return new Vector2(x, y);
        }

        public static Vector2 operator+(Vector2 lhs, Vector2 rhs)
        {
            return new Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
        }

        public static Vector2 operator-(Vector2 lhs, Vector2 rhs)
        {
            return new Vector2(lhs.x - rhs.x, lhs.y - rhs.y);
        }

        public static Vector2 Mul(Vector2 v, float t)
        {
            return new Vector2(v.x * t, v.y * t);
        }

        public static Vector2 operator*(Vector2 lhs, float rhs)
        {
            return Mul(lhs, rhs);
        }

        public float this[int index]
        {
            set
            {
                if (index == 0)
                    x = value;
                else if (index == 1)
                    y = value;
                else
                    throw new ArgumentOutOfRangeException();
            }

            get
            {
                if (index == 0)
                    return x;
                else if (index == 1)
                    return y;
                else
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", x, y);
        }

        public bool Equals(Vector2 other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2)
            {
                return Equals((Vector2) obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() + y.GetHashCode();
        }
    }
}
