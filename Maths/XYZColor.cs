﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fart.Maths
{
    public struct XYZColor
    {
        public float X, Y, Z;

        public Color3 RGB
        {
            get
            {
                // Multiply XYZ by the conventional matrix
                return Color3.Make(3.240479f * X - 1.537150f * Y - 0.498535f * Z,
                                   -0.969256f * X + 1.875992f * Y + 0.041556f * Z,
                                   0.055648f * X - 0.204043f * Y + 1.057311f * Z);
            }
        }

        public XYZColor(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
    }
}
