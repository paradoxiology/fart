﻿using System;
using System.Linq;

namespace Fart.Maths
{
    public sealed class ImportanceSampler1D
    {
        float m_sumF;
        float[] m_pdf;
        float[] m_cdf;

        public float Sum
        {
            get
            {
                return m_sumF;
            }
        }

        public float[] PDFs
        {
            get
            {
                return m_pdf;
            }
        }

        public float[] CDFs
        {
            get
            {
                return m_cdf;
            }
        }

        public ImportanceSampler1D(float[] f)
        {
            if (f.Length == 0)
            {
                throw new ArgumentException("Empty input array");
            }

            m_sumF = f.Sum();

            m_pdf = f.ToArray();
            for (int i = 0; i < m_pdf.Length; i++)
            {
                m_pdf[i] /= m_sumF;
            }

            m_cdf = new float[m_pdf.Length + 1];
            m_cdf[0] = 0;
            for (int i = 0; i < m_pdf.Length; i++)
            {
                if (m_pdf[i] < 0)
                {
                    throw new ArgumentException("Probabilities must not be less than zero");
                }

                m_cdf[i + 1] = m_cdf[i] + m_pdf[i];
            }

            m_cdf[m_cdf.Length - 1] = 1;
        }

        public float Sample(Sample1 s, out float pdf)
        {
            int index = Array.BinarySearch(m_cdf, s.s);
            index = Math.Max(0, (index < 0 ? ~index : index) - 1);

            pdf = m_pdf[index];

            float offset = (s.s - m_cdf[index]) / (m_cdf[index + 1] - m_cdf[index]);

            return (index + offset) / m_pdf.Length;
        }

        public int SampleIndex(Sample1 s, out float pdf)
        {
            int index = Array.BinarySearch(m_cdf, s.s);
            index = (index < 0 ? ~index : index) - 1;

            pdf = m_pdf[index];

            return index;
        }

        public float PDF(int index)
        {
            if (index >= 0 && index < m_pdf.Length)
            {
	            return m_pdf[index];
            }

            return 0;
        }
    }
}
