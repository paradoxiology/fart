﻿using System;
using System.Diagnostics;

namespace Fart.Maths
{
    public class Ray
    {
        private Vector3 m_orig;
        private Vector3 m_dir;
        private float m_segLength = float.MaxValue;

        public const float MIN_T = Utils.EPISLON * 10000;
        
        public Ray(Vector3 orig, Vector3 dir)
        {
            m_orig = orig;
            m_dir = dir;

            // Make sure the direction is normalized.
            Debug.Assert(Math.Abs(1.0f - m_dir.Length) < Utils.EPISLON);
        }

        public Ray(Vector3 orig, Vector3 dir, float segmentLength)
            : this(orig, dir)
        {
            m_segLength = segmentLength;
        }


        public Ray(Vector3 orig, Vector3 normal, float theta, float phi, float segmentLength)
        {
            m_orig = orig;

            m_dir = Utils.SphericalToDir(normal, theta, phi);

            m_segLength = segmentLength;
        }

        public Vector3 Direction
        {
            get { return m_dir;  }
        }

        public Vector3 Origin
        {
            get { return m_orig;  }

            set { m_orig = value; }
        }

        public float SegmentLength
        {
            get { return m_segLength;  }
        }

        public Vector3 ReachTo(float t)
        {
            return m_orig + m_dir * t;
        }

        public override string ToString()
        {
            return String.Format("(o: {0}, d: {1})", m_orig, m_dir);
        }
    }
}
