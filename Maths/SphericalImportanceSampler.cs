﻿using System;

namespace Fart.Maths
{
    public sealed class SphericalImportanceSampler
    {
        int m_uSize, m_vSize;
        float m_jTerm;
        ImportanceSampler2D m_sampler;

        public SphericalImportanceSampler(float[][] planarFunc)
        {
            m_uSize = planarFunc.Length;
            m_vSize = planarFunc[0].Length;

            for (int v = 0; v < m_vSize; v++)
            {
                float theta = Utils.PI * (v + 0.5f) / m_vSize;
                float sinTheta = Utils.Sinf(theta);

                for (int u = 0; u < m_uSize; u++)
                {
                    float phi = 2 * Utils.PI * (u) / m_uSize;

                    planarFunc[u][v] *= sinTheta;
                }
            }

            m_jTerm = (m_uSize * m_vSize) / (2 * Utils.PI * Utils.PI);

            m_sampler = new ImportanceSampler2D(planarFunc);
        }

        public SphericalImportanceSampler(Func<float, float, float> sphericalFunc, int uSize, int vSize)
        {
            float[][] planarFunc = new float[uSize][];
            for (int u = 0; u < uSize; u++)
            {
                planarFunc[u] = new float[vSize];
            }

            m_uSize = uSize;
            m_vSize = vSize;

            for (int v = 0; v < vSize; v++)
            {
                float theta = Utils.PI * (v + 0.5f) / m_vSize;
                float sinTheta = Utils.Sinf(theta);

            	for (int u = 0; u < uSize; u++)
            	{
                    float phi = 2 * Utils.PI * (u) / m_uSize;

                    planarFunc[u][v] = sphericalFunc(theta, phi) * sinTheta;
            	}
            }

            m_jTerm = (m_uSize * m_vSize) / (2 * Utils.PI * Utils.PI);

            m_sampler = new ImportanceSampler2D(planarFunc);
        }

        public Vector2 Sample(Sample2 s, out float pdf)
        {
            var res = m_sampler.Sample(s, out pdf);

            float theta = (res.y + 0.5f / m_vSize) * Utils.PI;
            if (Math.Abs(theta) < Utils.EPISLON)
            {
                pdf = 0;

                return Vector2.Make(0, 0);
            }

            float sinTheta = Utils.Sinf(theta);
            float jacobian = m_jTerm / Utils.Sinf(theta);

            pdf *= jacobian;

            return Vector2.Make(theta, res.x * Utils.TWO_PI);
        }

        public float PDF(float theta, float phi)
        {
            if (Math.Abs(theta) < Utils.EPISLON)
            {
                return 0;
            }

            if (phi < 0)
            {
                phi = Utils.TWO_PI - phi;
            }

            int uIndex = Math.Min(m_uSize - 1, (int) (phi * m_uSize * Utils.INV_TWO_PI));
            int vIndex = Math.Min(m_vSize - 1, (int) (theta * m_vSize * Utils.INV_PI));

            float jacobian = m_jTerm / Utils.Sinf(theta);

            return m_sampler.PDF(uIndex, vIndex) * jacobian;
        }
    }
}
