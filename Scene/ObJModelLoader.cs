﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Fart.Maths;
using Fart.Geometry;
using Fart.Shading;
using System.Windows.Forms;

namespace Fart.Scene
{
    public class ObJModelLoader
    {
        List<Vector3>  m_verts = new List<Vector3>();
        List<Vector3>  m_norms = new List<Vector3>();
        List<Vector2>  m_uvs = new List<Vector2>();

        List<MeshModel> m_models = new List<MeshModel>();

        string m_modelPath;

        ObjMaterial m_currentMaterial = null;
        Dictionary<string, ObjMaterial> m_materials = new Dictionary<string, ObjMaterial>();

        enum FileType
        {
            Obj_File,
            Mat_File
        }

        enum VertexType
        {
            VERTEX,
            NORMAL,
            TEXCOORD
        }

        public ObJModelLoader(string filename)
        {
            m_modelPath = Path.GetDirectoryName(filename);

            LoadFile(filename, FileType.Obj_File);
        }

        public List<MeshModel> LoadModels()
        {
            Debug.Assert(m_models.Count > 0);

            return m_models;            
        }

        private void LoadFile(string filename, FileType fileType)
        {
            try
            {
	            using (StreamReader reader = new StreamReader(filename))
	            {
	                while (!reader.EndOfStream)
	                {
	                    string line = reader.ReadLine();
	                    char[] sep = { ' ' };
	                    string[] tokens = line.Split(sep, StringSplitOptions.RemoveEmptyEntries);
	
	                    if (fileType == FileType.Obj_File)
	                    {
	                        ProcessObjLine(tokens);
	                    }
	                    else
	                    {
	                        ProcessMatLine(tokens);
	                    }
	                }
	            }
            }
            catch (IOException e)
            {
                MessageBox.Show(e.Message, "Error Loading Model", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ProcessObjLine(string[] tokens)
        {
            if (tokens != null && tokens.Length > 0)
            {
                switch (tokens[0])
                {
                case "v":
                    AddVector(tokens, VertexType.VERTEX);
                	break;

                case "vt":
                    AddVector(tokens, VertexType.TEXCOORD);
                    break;

                case "vn":
                    AddVector(tokens, VertexType.NORMAL);
                    break;

                case "f":
                    AddFace(tokens);
                    break;

                case "g":
                    AddModel(tokens);
                    break;

                case "mtllib":
                    LoadFile(Path.Combine(m_modelPath, string.Join(" ", tokens, 1, tokens.Length - 1)), FileType.Mat_File);
                    break;

                case "usemtl":
                    AddMaterial(tokens);
                    break;

                default:
                    break;
                }
            }
        }

        private void AddVector(string[] tokens, VertexType vertType)
        {
            if (vertType == VertexType.VERTEX || vertType == VertexType.NORMAL)
            {
                Debug.Assert(tokens.Length == 4);

                float xx = float.Parse(tokens[1]);
                float yy = float.Parse(tokens[2]);
                float zz = float.Parse(tokens[3]);

                List<Vector3> compos;
                if (vertType == VertexType.VERTEX)
                    compos = m_verts;
                else
                    compos = m_norms;

                compos.Add(Vector3.Make(xx, yy, zz));
            } 
            else
            {
                Debug.Assert(tokens.Length == 3 || tokens.Length == 4);

                float u = float.Parse(tokens[1]);
                float v = float.Parse(tokens[2]);

                m_uvs.Add(Vector2.Make(u, v));
            }
        }

        private void AddFace(string[] tokens)
        {
            Debug.Assert(tokens.Length >= 4);
            Debug.Assert(m_models.Count > 0);

            string[] v0Token = tokens[1].Split('/');
            string[] v1Token = tokens[2].Split('/');
            string[] v2Token = tokens[3].Split('/');

            // Extract Vertices
            Vector3 v0 = m_verts[int.Parse(v0Token[0]) - 1];
            Vector3 v1 = m_verts[int.Parse(v1Token[0]) - 1];
            Vector3 v2 = m_verts[int.Parse(v2Token[0]) - 1];

            Vector3 n0, n1, n2;
            Vector2 t0, t1, t2;

            Vector3 faceNormal = ((v1 - v0) % (v2 - v0)).GetNormalized();
            n0 = n1 = n2 = faceNormal;

            t0 = t1 = t2 = Vector2.Make(0.0f, 0.0f);

            if (v0Token.Length >= 3)
            {
                // Extract Texture Coordinates.
                if (!string.IsNullOrEmpty(v0Token[1]))
                {
                    t0 = m_uvs[int.Parse(v0Token[1]) - 1];
                    t1 = m_uvs[int.Parse(v1Token[1]) - 1];
                    t2 = m_uvs[int.Parse(v2Token[1]) - 1];
                }

                if (!string.IsNullOrEmpty(v0Token[2]))
                {
                    n0 = m_norms[int.Parse(v0Token[2]) - 1];
                    n1 = m_norms[int.Parse(v1Token[2]) - 1];
                    n2 = m_norms[int.Parse(v2Token[2]) - 1];
                }
            }

            MeshModel lastModel = m_models[m_models.Count - 1];

            lastModel.AddFace(new Triangle(v0, v1, v2,
                                           n0, n1, n2,
                                           t0, t1, t2, faceNormal));
        }

        private void AddModel(string[] tokens)
        {
            m_models.Add(new MeshModel());
        }

        class ObjMaterial
        {
            public Color3 Kd = Color3.Black;
            public Color3 Ks = Color3.Black;
            public float Ns = 0;
            public float Ni = 1;
            public int IlluminationModel = 2;

            public Material ToMaterial()
            {
                if (Ns == 0 && Kd != Color3.Black)
                {
                    return new DiffuseMaterial(Kd);
                }
                else if(Ns > 0 && Ks != Color3.Black)
                {
                    return new Phong(Kd, Ks, Ns);
                }
                else if (Kd != Color3.Black)
                {
                    return new DiffuseMaterial(Kd);
                }

                return new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
            }
        }

        private void AddMaterial(string[] tokens)
        {
            var matName = tokens[1];

            ObjMaterial objMat;
            if (m_materials.TryGetValue(matName, out objMat))
            {
                var lastModel = m_models[m_models.Count - 1];
                if (lastModel.Material != null)
                {
                    AddModel(null);
                }

                lastModel = m_models[m_models.Count - 1];
                lastModel.Material = objMat.ToMaterial();
            }
        }

        void ProcessMatLine(string[] tokens)
        {
            if (tokens != null && tokens.Length > 0)
            {
                switch (tokens[0].TrimStart('\t'))
                {
                case "newmtl":
                    AddNewMtl(tokens);
                	break;
                case "Kd":
                    {
                        Debug.Assert(m_currentMaterial != null);
                        float r = float.Parse(tokens[1]),
                              g = float.Parse(tokens[2]),
                              b = float.Parse(tokens[3]);
                        m_currentMaterial.Kd = Color3.Make(r, g, b);
                    }
                    break;
                case "Ks":
                    {
                        Debug.Assert(m_currentMaterial != null);
                        float r = float.Parse(tokens[1]),
                              g = float.Parse(tokens[2]),
                              b = float.Parse(tokens[3]);
                        m_currentMaterial.Ks = Color3.Make(r, g, b);
                    }
                    break;
                case "Ns":
                    {
                        Debug.Assert(m_currentMaterial != null);
                        float shiness = float.Parse(tokens[1]);
                        m_currentMaterial.Ns = shiness;
                    }
                    break;
                default:
                    break;
                }
            }
        }

        void AddNewMtl(string[] tokens)
        {
            m_currentMaterial = new ObjMaterial();

            m_materials.Add(tokens[1], m_currentMaterial);
        }
    }
}
