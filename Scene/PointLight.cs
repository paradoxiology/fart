﻿using System;
using System.Collections.Generic;

using Fart.Maths;
using Fart.Shading;
using Fart.Geometry;

namespace Fart.Scene
{
    public sealed class PointLight : ILight
    {
        public Vector3 Position { get; private set; }
        private Color3 m_intensity;

        public PointLight(Vector3 pos, Color3 intensity)
        {
            Position = pos;
            m_intensity = intensity;
        }

        public Color3 GetRadiance()
        {
            return m_intensity;
        }

        public Color3 GetPower()
        {
            return m_intensity * 4.0f * Utils.PI;
        }

        public bool IsDelta()
        {
            return true;
        }

        public bool Intersect(Ray ray)
        {
            return false;
        }

        public bool Intersect(Ray ray, out InterState interState)
        {
            interState = null;

            return false;
        }
        public List<Ray> GetVisibilityRays(Vector3 pos, Vector3 normal)
        {
            List<Ray> rays = new List<Ray>(1);

            rays.Add(new Ray(pos + normal * Ray.MIN_T, Vector3.Normalize(Position - pos), (Position - pos).Length));

            return rays;
        }

        public LightColor SampleLight(Vector3 position, Vector3 normal, Sample2 s, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            var dir = position - Position;
            wi = -Vector3.Normalize(dir);
            visiRay = new Ray(position + normal * Ray.MIN_T, wi, dir.Length);
            pdf = 1;

            return new LightColor(this, Color3.White / Vector3.SquaredDist(position, Position));
        }

        public LightSampleInfo SampleLight(Vector3 position, Vector3 normal, Sample2 s)
        {
            Vector3 wi;
            float lightPdf;
            Ray visiRay;

            LightColor L = SampleLight(position, normal, s, out wi, out lightPdf, out visiRay);

            return new LightSampleInfo { visiRay = visiRay, L = L, pdf = lightPdf };
        }

        public LightColor ScatterLight(World world, Sample2 s0, Sample2 s1, out float pdf, out Ray lightRay)
        {
            Vector3 dir = Utils.UniformRandDir(s1);
            pdf = 1.0f / (4.0f * Utils.PI);
            lightRay = new Ray(Position, dir);

            return new LightColor(this);
        }

        public LightColor SampleLightDirect(Vector3 position, Vector3 normal, out Vector3 wi, out float pdf, out Ray visiRay)
        {
 	        throw new NotImplementedException();
        }

        public float PDF(Vector3 position, Vector3 wi)
        {
            return 0;
        }
    }
}
