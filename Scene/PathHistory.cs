﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fart.Scene
{
    public sealed class PathHistory
    {
        List<PathType> m_history;

        public PathType Last
        {
            get
            {
                return m_history.Last();
            }
        }

        public int Count
        {
            get
            {
                return m_history.Count;
            }
        }

        public PathHistory(PathType pathType)
        {
            m_history = new List<PathType>();
            m_history.Add(pathType);
        }

        public PathHistory(PathHistory history, PathType pathType)
        {
            m_history = new List<PathType>(history.m_history);
            m_history.Add(pathType);
        }

        public void Clear()
        {
            m_history.Clear();
        }

        public void Push(PathType pathType)
        {
            m_history.Add(pathType);
        }

        public void Pop()
        {
            m_history.RemoveAt(m_history.Count - 1);
        }

        public bool Containis(PathType pathType)
        {
            return m_history.Contains(pathType);
        }

        public int FindFirstPathType(PathType pathType)
        {
            return m_history.IndexOf(pathType);
        }

        public int FindLastPathType(PathType pathType)
        {
            return m_history.LastIndexOf(pathType);
        }

        public int[] FindPaths(PathType pathType)
        {
            List<int> indices = new List<int>();

            for (int i = 0; i < m_history.Count; i++)
            {
            	if (m_history[i] == pathType)
            	{
                    indices.Add(i);
            	}
            }

            return indices.ToArray();
        }
    }
}
