﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

using Fart.Geometry;
using Fart.Maths;

namespace Fart.Scene
{
    public sealed class BIH
    {
        enum Axis: int
        {
            X_AXIS,
            Y_AXIS,
            Z_AXIS,
            LEAF_AXIS
        }

        sealed class BIHNode 
        {
            public Axis Axis;

            public BIHNode Left, Right;

            public int LeftIndex, RightIndex;

            public float LeftPlane, RightPlane;

            public bool IsClipNode
            {
                get
                {
                    return Left != null && Left == Right;
                }
            }
        }

        List<ShadedShape> m_shapes = new List<ShadedShape>();
        ShadedShape[] m_shapeArray;

        BBox m_bound;
        BIHNode m_root;

        public BBox Bound
        {
            get
            {
                return m_bound;
            }
        }

        public BIH()
        {
            m_bound = BBox.Create();
        }

        public void AddShape(ShadedShape shape)
        {
            m_shapes.Add(shape);

            m_bound.Union(shape.Shape.GetBounding());
        }

        public void AddShapes(List<ShadedShape> shapes)
        {
            m_shapes.AddRange(shapes);

            foreach (var shape in shapes)
            {
                m_bound.Union(shape.Shape.GetBounding());
            }
        }

        public void Build()
        {
            if (m_shapes.Count > 0)
            {
	            m_shapeArray = m_shapes.ToArray();
	            m_shapes = null;


                m_root = CreateNode(0, m_shapeArray.Length - 1, m_bound, m_bound, 0);
            }
        }

        public bool Intersect(Ray ray, out InterState interState, out ShadedShape interShape)
        {
            if (m_root != null)
            {
                return InterectNode(m_root, ray, Utils.EPISLON, ray.SegmentLength, out interState, out interShape);
            } 
            else
            {
                interState = null;
                interShape = new ShadedShape();

                return false;
            }
        }

        public bool Intersect(Ray ray)
        {
            if (m_root != null)
            {
                return InterectNode(m_root, ray, Utils.EPISLON, ray.SegmentLength);
            }

            return false;
        }

        private bool InterectNode(BIHNode node, Ray ray, float minT, float maxT, out InterState interState, out ShadedShape interShape)
        {
            if (minT > ray.SegmentLength)
            {
                interState = null;
                interShape = new ShadedShape();

                return false;
            }

            if (node.Axis == Axis.LEAF_AXIS)
            {
                return IntersectLeaf(ray, node, out interState, out interShape);
            }

            float oneOverDir =  1 / ray.Direction[(int) node.Axis];

            float nearClip = (node.LeftPlane - ray.Origin[(int) node.Axis]) * oneOverDir;
            float farClip = (node.RightPlane - ray.Origin[(int) node.Axis]) * oneOverDir;

            BIHNode nearNode = node.Left;
            BIHNode farNode = node.Right;

            if (ray.Direction[(int) node.Axis] < 0)
            {
                float tmp = nearClip;
                nearClip = farClip;
                farClip = tmp;

                BIHNode tmpNode = nearNode;
                nearNode = farNode;
                farNode = tmpNode;
            }

            if (node.IsClipNode)
            {
                minT = Math.Max(minT, nearClip);
                maxT = Math.Min(maxT, farClip);

                if (minT <= maxT)
                {
                    return InterectNode(node.Left, ray, minT, maxT, out interState, out interShape);
                } 
                else
                {
                    interState = null;
                    interShape = new ShadedShape();

                    return false;
                }
            } 
            else
            {
	            if (minT > nearClip && maxT < farClip)
	            {
	                interState = null;
	                interShape = new ShadedShape();
	
	                return false;
	            }
	
	            if ((farNode != null && minT > nearClip) || nearNode == null)
	            {
	                // Far node only
	
	                minT = Math.Max(minT, farClip);
	                return InterectNode(farNode, ray, Math.Max(minT, 0), Math.Min(maxT, ray.SegmentLength), out interState, out interShape);
	            }
	            else if ((nearNode != null && maxT < farClip) || farNode == null)
	            {
	                // Near node only
	
	                return InterectNode(nearNode, ray, Math.Max(minT, 0), Math.Min(maxT, nearClip), out interState, out interShape);
	            }
	            else
	            {
	                // Two nodes need to test for intersection.
	
	                // Try the near node first
	                bool hitFar = false;
	                bool hitNear = InterectNode(nearNode, ray, Math.Max(minT, 0), Math.Min(maxT, nearClip), out interState, out interShape);
	                if (!hitNear || interState.Distance > Math.Max(minT, farClip))
	                {
	                    // If the near node doesn't intersect with the ray, or the hit distance is greater
	                    // than the far plane, then test the far node.
	
	                    InterState tmpInter;
	                    ShadedShape tmpShape;
	
	                    if (InterectNode(farNode, ray, Math.Max(minT, farClip), Math.Min(maxT, ray.SegmentLength), out tmpInter, out tmpShape))
	                    {
	                        hitFar = true;
	
	                        if (interState == null || tmpInter.Distance < interState.Distance)
	                        {
		                        interState = tmpInter;
		                        interShape = tmpShape;
	                        }
	                    }
	                }
	
	                return hitNear || hitFar;
	            }
            }
        }

        private bool IntersectLeaf(Ray ray, BIHNode leaf, out InterState interState, out ShadedShape interShape)
        {
            Debug.Assert(leaf.LeftIndex >= 0 && leaf.RightIndex >= 0);

            interState = null;
            interShape = new ShadedShape();

            bool inter = false;
            float closest = float.MaxValue;
            InterState tempState;
            for (int i = leaf.LeftIndex; i <= leaf.RightIndex; i++)
            {
                if (m_shapeArray[i].Shape.Intersect(ray, out tempState))
                {
                    inter = true;

                    if (tempState.Distance < closest)
                    {
                        closest = tempState.Distance;

                        interState = tempState;
                        interShape = m_shapeArray[i];
                    }
                }
            }

            return inter;
        }

        private bool InterectNode(BIHNode node, Ray ray, float minT, float maxT)
        {
            if (minT > ray.SegmentLength)
            {
                return false;
            }

            if (node.Axis == Axis.LEAF_AXIS)
            {
                return IntersectLeaf(ray, node);
            }

            float oneOverDir = 1 / ray.Direction[(int) node.Axis];

            float nearClip = (node.LeftPlane - ray.Origin[(int) node.Axis]) * oneOverDir;
            float farClip = (node.RightPlane - ray.Origin[(int) node.Axis]) * oneOverDir;

            BIHNode nearNode = node.Left;
            BIHNode farNode = node.Right;

            if (ray.Direction[(int) node.Axis] < 0)
            {
                float tmp = nearClip;
                nearClip = farClip;
                farClip = tmp;

                BIHNode tmpNode = nearNode;
                nearNode = farNode;
                farNode = tmpNode;
            }

            if (node.IsClipNode)
            {
                minT = Math.Max(minT, nearClip);
                maxT = Math.Min(maxT, farClip);

                if (minT <= maxT)
                {
                    return InterectNode(node.Left, ray, minT, maxT);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (minT > nearClip && maxT < farClip)
                {
                    return false;
                }

                if ((farNode != null && minT > nearClip) || nearNode == null)
                {
                    // Far node only

                    minT = Math.Max(minT, farClip);
                    return InterectNode(farNode, ray, Math.Max(minT, 0), Math.Min(maxT, ray.SegmentLength));
                }
                else if ((nearNode != null && maxT < farClip) || farNode == null)
                {
                    // Near node only

                    return InterectNode(nearNode, ray, Math.Max(minT, 0), Math.Min(maxT, nearClip));
                }
                else
                {
                    // Two nodes need to test for intersection.

                    // Try the near node first
                    if (!InterectNode(nearNode, ray, Math.Max(minT, 0), Math.Min(maxT, nearClip)))
                    {
                        return InterectNode(farNode, ray, Math.Max(minT, farClip), Math.Min(maxT, ray.SegmentLength));
                    }

                    return true;
                }
            }
        }

        private bool IntersectLeaf(Ray ray, BIHNode leaf)
        {
            Debug.Assert(leaf.LeftIndex >= 0 && leaf.RightIndex >= 0);

            for (int i = leaf.LeftIndex; i <= leaf.RightIndex; i++)
            {
                if (m_shapeArray[i].Shape.Intersect(ray))
                {
                    return true;
                }
            }

            return false;
        }

        private BIHNode CreateNode(int leftIndex, int rightIndex, BBox shapeBound, BBox nodeVolume, int depth)
        {
            if (depth > 38 || rightIndex - leftIndex < 3 || nodeVolume.Volume < Utils.EPISLON)
            {
                return CreateLeaf(leftIndex, rightIndex);
            }

//             BBox shapeBound = GetNodeShapeBound(leftIndex, rightIndex);
            Axis splitAxis = GetDominanceAxis(nodeVolume.Extents);

            if (shapeBound.Extents[(int) splitAxis] * 1.1f < nodeVolume.Extents[(int) splitAxis])
            {
                // Make a clip node. Node containing relatively small shapes than its nodeVolume.
                BIHNode node = new BIHNode();
                node.Axis = splitAxis;

                node.LeftPlane = shapeBound.Lower[(int) splitAxis];
                node.RightPlane = shapeBound.Upper[(int) splitAxis];

                node.Left = node.Right = CreateNode(leftIndex, rightIndex, shapeBound, shapeBound, depth + 1);

                return node;
            }

            float split = GetSplitPlane(nodeVolume, shapeBound, out splitAxis);
            int pivot = PartitionNodeShapes(leftIndex, rightIndex, split, splitAxis);

            if (pivot < leftIndex)
            {
                // Right only
                nodeVolume.Lower[(int) splitAxis] = split;

                BIHNode newNode = new BIHNode();
                newNode.Axis = splitAxis;
                newNode.LeftPlane = newNode.RightPlane = shapeBound.Lower[(int) splitAxis];
                newNode.Right = CreateNode(leftIndex, rightIndex, shapeBound, nodeVolume, depth + 1);

                return newNode;
            } 
            else if (pivot > rightIndex)
            {
                // Left only
                nodeVolume.Upper[(int) splitAxis] = split;

                BIHNode newNode = new BIHNode();
                newNode.Axis = splitAxis;
                newNode.RightPlane = newNode.LeftPlane = shapeBound.Upper[(int) splitAxis];
                newNode.Left = CreateNode(leftIndex, rightIndex, shapeBound, nodeVolume, depth + 1);

                return newNode;
            }
            else
            {
                BIHNode node = new BIHNode();
                node.Axis = splitAxis;

                BBox leftVolume = nodeVolume;
                leftVolume.Upper[(int) splitAxis] = split;
                BBox leftShapeBound = GetNodeShapeBound(leftIndex, Math.Max(leftIndex, pivot - 1));
                node.LeftPlane = leftShapeBound.Upper[(int) splitAxis];

                BBox rightVolume = nodeVolume;
                rightVolume.Lower[(int) splitAxis] = split;
                BBox rightShapeBound = GetNodeShapeBound(pivot, rightIndex);
                node.RightPlane = rightShapeBound.Lower[(int) splitAxis];

                if (rightIndex - leftIndex > 32)
                {
	                Parallel.Invoke(
                        () => node.Right = CreateNode(pivot, rightIndex, rightShapeBound, rightVolume, depth + 1),
                        () => node.Left = CreateNode(leftIndex, Math.Max(leftIndex, pivot - 1), leftShapeBound, leftVolume, depth + 1)
	                );
                } 
                else
                {
                    node.Right = CreateNode(pivot, rightIndex, rightShapeBound, rightVolume, depth + 1);
                    node.Left = CreateNode(leftIndex, Math.Max(leftIndex, pivot - 1), leftShapeBound, leftVolume, depth + 1);
                }

                return node;
            }
        }

        private BIHNode CreateLeaf(int leftIndex, int rightIndex)
        {
            BIHNode leaf = new BIHNode();

            leaf.Axis = Axis.LEAF_AXIS;
            leaf.Left = leaf.Right = null;

            leaf.LeftPlane = leaf.RightPlane = 0.0f;

            leaf.LeftIndex = leftIndex;
            leaf.RightIndex = rightIndex;

            return leaf;
        }

        private int PartitionNodeShapes(int leftIndex, int rightIndex, float splitPlane, Axis splitAxis)
        {
            Debug.Assert(leftIndex <= rightIndex);

            int low = leftIndex, high = rightIndex;

            while (low < high)
            {
                while (low <= rightIndex && m_shapeArray[low].Shape.GetBounding().Center[(int) splitAxis] <= splitPlane)
                {
                    low++;
                }
                while (high >= leftIndex && m_shapeArray[high].Shape.GetBounding().Center[(int) splitAxis] > splitPlane)
                    high--;
                if (low < high)
                {
                    SwapShape(low, high);
                }
            }

            int pivot = (low == leftIndex && high < low)? high: low;

            return pivot;
        }

        private int CompareShapeWithSplit(Shape shape, float split, Axis splitAxis)
        {
            BBox shapeBound = shape.GetBounding();

            float lower = shapeBound.Lower[(int) splitAxis], upper = shapeBound.Upper[(int) splitAxis];

            if (upper < split)
            {
                return -1;
            }
            else if (lower > split)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private void SwapShape(int i, int j)
        {
            var tmp = m_shapeArray[j];
            m_shapeArray[j] = m_shapeArray[i];
            m_shapeArray[i] = tmp;
        }

        private BBox GetNodeShapeBound(int leftIndex, int rightIndex)
        {
            Debug.Assert(leftIndex <= rightIndex);

            BBox nodeBox = m_shapeArray[leftIndex].Shape.GetBounding();

            for (int i = leftIndex; i <= rightIndex; i++)
            {
                nodeBox.Union(m_shapeArray[i].Shape.GetBounding());
            }

            return nodeBox;
        }

//         private float GetSplitPlane(BBox volume, BBox shapeBound, out Axis splitAxis)
//         {
//             // Use global heuristics to find the split plane, it uniformly subdivides a bounding volume until the plane collides
//             // a shape in the volume.
// 
//             Axis domAxis = GetDominanceAxis(volume.Extents);
// 
//             float candidate = volume.Center[(int) domAxis];
// 
// //             if (candidate < shapeBound.Lower[(int) domAxis])
// //             {
// //                 volume.Lower[(int) domAxis] = candidate;
// // 
// //                 return GetSplitPlane(ref volume, shapeBound, out splitAxis);
// //             }
// //             else if (candidate > shapeBound.Upper[(int) domAxis])
// //             {
// //                 volume.Upper[(int) domAxis] = candidate;
// // 
// //                 return GetSplitPlane(ref volume, shapeBound, out splitAxis);
// //             }
// //             else
//             {
//                 splitAxis = domAxis;
// 
//                 return candidate;
//             }
//         }

        private float GetSplitPlane(BBox volume, BBox shapeBound, out Axis splitAxis)
        {
            // Use global heuristics to find the split plane, it uniformly subdivides a bounding volume until the plane collides
            // a shape in the volume.

            Axis lastAxis = GetDominanceAxis(volume.Extents);
            float lastCandidate = float.NaN;

            do 
            {
	            Axis domAxis = GetDominanceAxis(volume.Extents);
                if (lastAxis != domAxis)
                {
                    splitAxis = lastAxis;

                    return lastCandidate;
                }

	            float candidate = volume.Center[(int) domAxis];

	            if (candidate < shapeBound.Lower[(int) domAxis])
	            {
	                volume.Lower[(int) domAxis] = candidate;
	            }
	            else if (candidate > shapeBound.Upper[(int) domAxis])
	            {
	                volume.Upper[(int) domAxis] = candidate;
	            }
	            else
	            {
                    splitAxis = lastAxis;
	
	                return !float.IsNaN(lastCandidate)? lastCandidate: candidate;
	            }

                lastAxis = domAxis;
                lastCandidate = candidate;
            } while (true);
        }


        private static Axis GetDominanceAxis(Vector3 extents)
        {
            Axis axis = Axis.X_AXIS;

            if (extents[1] > extents[(int) axis])
            {
                axis = Axis.Y_AXIS;
            }

            if (extents[2] > extents[(int) axis])
            {
                axis = Axis.Z_AXIS;
            }

            return axis;
        }

    }
}
