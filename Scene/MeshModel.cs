﻿using System.Collections.Generic;
using System.Text;

using Fart.Geometry;
using Fart.Maths;
using Fart.Shading;

namespace Fart.Scene
{
    public sealed class MeshModel: Model
    {
        private List<ShadedShape> m_faces = new List<ShadedShape>();
        private Matrix4 m_transf;
        private BBox m_bound = BBox.Create();
        private bool m_dirty = false;

        public int FaceCount
        {
            get { return m_faces.Count; }
        }

        public List<ShadedShape> Shapes
        {
            get { return m_faces; }
        }

        public MeshModel()
        {
            m_transf = Matrix4.Identity;
        }

        public MeshModel(Material material): this()
        {
            Material = material;
        }

        public void AddFace(Triangle face)
        {
            m_faces.Add(new ShadedShape(face, this, null));

            m_bound.Union(face.GetBounding());
        }

        public void AddFaces(List<Triangle> faces)
        {
            foreach (var face in faces)
            {
                AddFace(face);

                m_bound.Union(face.GetBounding());
            }
        }

        public Triangle GetFace(int index)
        {
            Clean();

            return (Triangle) m_faces[index].Shape;
        }

        public override float Area()
        {
            throw new System.NotImplementedException();
        }

        public override Vector3 SampleShape(Sample2 s, out Vector3 normal)
        {
            throw new System.NotImplementedException();
        }

        public override void Transform(Matrix4 transformation)
        {
            m_transf = transformation;
            m_dirty = true;
        }

        public override bool Intersect(Ray ray)
        {
            Clean();

            foreach (var face in m_faces)
            {
                if (face.Shape.Intersect(ray))
                {
                    return true;
                }
            }

            return false;
        }

        public override bool Intersect(Ray ray, out InterState interState)
        {
            Clean();

            interState = null;
            float closestDist = float.MaxValue;

            foreach (var face in m_faces)
            {
                InterState tempInter;

                if (face.Shape.Intersect(ray, out tempInter) && tempInter.Distance < closestDist)
                {
                    closestDist = tempInter.Distance;

                    interState = tempInter;
                }
            }

            return interState != null;
        }

        public override bool GetShadingState(World world, Ray ray, out ShadingState sState)
        {
            InterState iState = null;
            sState = null;
            bool inter = false;

            if (Intersect(ray, out iState))
            {
                sState = Material.GetShading(ray, iState);

                inter = true;
            }

            return inter;
        }
        
        private void TransformFaces(Matrix4 transformation)
        {
            Matrix4 normalTransformation = Matrix4.Transpose(Matrix4.Invert(transformation));

            foreach (var face in m_faces)
            {
                Triangle triangle = (Triangle) face.Shape;
                triangle.Transform(transformation, normalTransformation);
            }
        }

        private void Clean()
        {
            if (m_dirty)
            {
                TransformFaces(m_transf);

                m_dirty = false;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var face in m_faces)
            {
                sb.Append(face.ToString());
            }

            return sb.ToString();
        }

        public override BBox GetBounding()
        {
            return m_bound;
        }
    }
}
