﻿using System;
using System.Collections.Generic;
using Fart.Maths;

namespace Fart.Scene
{
    public sealed class MultiColor: ICloneable
    {
        Dictionary<ILight, LightColor> m_colorSet;

        public Color3 FinalColor
        {
            get
            {
                Color3 accum = Color3.Black;
                foreach (var color in m_colorSet.Values)
                {
                    accum += color.Color;
                }

                return accum;
            }
        }

        public MultiColor()
        {
            m_colorSet = new Dictionary<ILight, LightColor>();
        }

        public MultiColor(MultiColor other)
        {
            m_colorSet = new Dictionary<ILight, LightColor>(other.m_colorSet);
        }

        public MultiColor(LightColor initialLightColor): this()
        {
            Add(initialLightColor);
        }

        public void Add(LightColor lightColor)
        {
            LightColor lc;
            if (m_colorSet.TryGetValue(lightColor.Light, out lc))
            {
                m_colorSet[lightColor.Light] = new LightColor(lightColor.Light, lc.Factor + lightColor.Factor);
            }
            else
            {
                m_colorSet.Add(lightColor.Light, lightColor);
            }
        }

        public void Add(MultiColor multiColor)
        {
            if (multiColor != null)
            {
	            foreach (var color in multiColor.m_colorSet.Values)
	            {
	                LightColor lc;
	                if (m_colorSet.TryGetValue(color.Light, out lc))
	                {
	                    m_colorSet[color.Light] = new LightColor(color.Light, lc.Factor + color.Factor);
	                }
	                else
	                {
	                    m_colorSet.Add(color.Light, color);
	                }
	            }
            }
        }

        public void Scale(float factor)
        {
            if (m_colorSet.Count > 0)
            {
                LightColor[] colors = new LightColor[m_colorSet.Values.Count];
                m_colorSet.Values.CopyTo(colors, 0);

                foreach (var color in colors)
                {
                    m_colorSet[color.Light] = color * factor;
                }
            }
        }

        public void Scale(Color3 factor)
        {
            if (m_colorSet.Count > 0)
            {
	            LightColor[] colors = new LightColor[m_colorSet.Values.Count];
	            m_colorSet.Values.CopyTo(colors, 0);
	
	            foreach (var color in colors)
	            {
	                m_colorSet[color.Light] = color * factor;
	            }
            }
        }

        public void Clear()
        {
            m_colorSet.Clear();
        }

        public object Clone()
        {
            return new MultiColor(this);
        }
    }
}
