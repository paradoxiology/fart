﻿using System;
using System.Collections.Generic;

using Fart.Maths;
using Fart.Render;
using Fart.Shading;

namespace Fart.Scene
{
    public class RayTracer
    {
        private World m_world = new World();
        private PhotonMap m_photonMap;

        private Camera m_camera = new Camera();

        public World World
        {
            get
            {
                return m_world;
            }
        }

        public void PopulatePhotonMap()
        {
            if (m_photonMap == null)
            {
                m_photonMap = new PhotonMap(m_world, m_camera, 50000, 0, true);
            }
        }

        public void ResetImageSize(int width, int height)
        {
            m_camera.SetImageSize(width, height);

            m_camera.Lookat(Vector3.Make(0, 0, 7f), Vector3.Make(0, 0.0f, 0), Vector3.Make(0, 1, 0), 50);
//             m_camera.Lookat(Vector3.Make(0, 5.5f, 23), Vector3.Make(0, 2.5f, 0), Vector3.Make(0, 1, 0), 50);
//             m_camera.Lookat(Vector3.Make(17.16f, 4.01f, 20.88f), Vector3.Make(8.53f, 7.74f, -13.79f), Vector3.Make(0, 1, 0), 50);
//             m_camera.Lookat(Vector3.Make(-12.76f, 1.22f, 10.94f), Vector3.Make(-6.86f, 2.5f, 4.87f), Vector3.Make(0, 1, 0), 50); // House
//             m_camera.Lookat(Vector3.Make(24.784966f, 5.757578f, -12.429637f), Vector3.Make(8.926679f, 6.270847f, -4.413375f), Vector3.Make(0, 1, 0), 50); // Bathroom
//             m_camera.Lookat(Vector3.Make(-1.52f, -1.25f, 3.05f), Vector3.Make(-1.93f, -0.94f, 0.72f), Vector3.Make(0, 1, 0), 50); // room
//             m_camera.Lookat(Vector3.Make(1.540366f, 0.634223f, -5.701477f), Vector3.Make(3.788520f, 0.681328f, -5.935774f), Vector3.Make(0, 1, 0), 50); // living room
//             m_camera.Lookat(Vector3.Make(-7.79f, 0.30f, 0.15f), Vector3.Make(-6.5f, 0.62f, -0.52f), Vector3.Make(0, 1, 0), 50); // room
//             m_camera.Lookat(Vector3.Make(-570.263611f, -21.381039f, 119.374054f), Vector3.Make(-319.247742f, -17.491611f, -31.045340f), Vector3.Make(0, 1, 0), 50); // classroom
//             m_camera.Lookat(Vector3.Make(-14.794919f, 1.733617f, -0.102358f), Vector3.Make(0.835954f, 3.494807f, 0.539514f), Vector3.Make(0, 1, 0), 50); // sponza
//             m_camera.Lookat(Vector3.Make(1.514601f, 4.076441f, 6.085783f), Vector3.Make(15.104797f, 9.688437f, 1.278265f), Vector3.Make(0, 0, 1), 50); // conference
//             m_camera.Lookat(Vector3.Make(-45.333023f, 44.869217f, 5.891282f), Vector3.Make(-22.219265f, 5.833420f, 4.385010f), Vector3.Make(0, 1, 0), 50); // greeble
//             m_camera.Lookat(Vector3.Make(5.369462f, 1.048961f, 0.034411f), Vector3.Make(-2.517497f, 1.235661f, -0.020029f), Vector3.Make(0, 1, 0), 50); // red room
//             m_camera.Lookat(Vector3.Make(-0.641484f, 0.314410f, 9.534863f), Vector3.Make(1.154831f, 1.934295f, 1.280330f), Vector3.Make(0, 1, 0), 50); // hangar
//             m_camera.Lookat(Vector3.Make(2329.015625f, 643.162659f, 2745.119385f), Vector3.Make(1397.414673f, 748.189941f, 108.477371f), Vector3.Make(0, 1, 0), 50); // sunny room
//             m_camera.Lookat(Vector3.Make(217.616287f, 151.274719f, 170.890213f), Vector3.Make(278.258087f, 94.899689f, -567.703979f), Vector3.Make(0, 1, 0), 50); // apartment
//             m_camera.Lookat(Vector3.Make(20.982315f, 5.804523f, 11.568613f), Vector3.Make(7.828671f, 5.094508f, 4.950915f), Vector3.Make(0, 1, 0), 50); // room8
//             m_camera.Lookat(Vector3.Make(-1.769577f, 2.680518f, 11.838253f) * 20, Vector3.Make(3.530384f, 1.446890f, 0.668520f) * 20, Vector3.Make(0, 1, 0), 50); // fsroom
//             m_camera.Lookat(Vector3.Make(-4.733618f, -0.173801f, 20.414703f), Vector3.Make(-0.552882f, -1.451724f, 3.870608f), Vector3.Make(0, 1, 0), 50); // htroom
//             m_camera.Lookat(Vector3.Make(-30.965637f, 100.780327f, 36.983658f), Vector3.Make(2.763943f, 103.277657f, 5.192607f), Vector3.Make(0, 1, 0), 50); // binary kite
//             m_camera.Lookat(Vector3.Make(0.514946f, 140.481766f, 43.462208f), Vector3.Make(6.416333f, 106.001045f, 1.668210f), Vector3.Make(0, 1, 0), 50); // binary kite
        }

        public struct PathInfo
        {
            public int DirectSampleCount
            {
                get;
                set;
            }

            public int IndirectSampleCount
            {
                get;
                set;
            }

            public int GlossySampleCount
            {
                get;
                set;
            }

            public bool GlossyPath
            {
                get;
                set;
            }

            public PathInfo(int directCount, int indirectCount, int glossyCount): this()
            {
                DirectSampleCount = directCount;

                IndirectSampleCount = indirectCount;

                GlossySampleCount = glossyCount;

                GlossyPath = false;
            }

        }

        public MultiColor Trace(float x, float y, ISampler sampler, SampleState sampleState)
        {
            return Trace(new PathState(m_world, m_camera.GetRay(x, y), PathType.Eye), sampleState, new PathInfo(64, 128, 256), 0);
        }

        public MultiColor Trace(PathState pathState, SampleState sampleState, PathInfo pathInfo, int traceDepth)
        {
            PathType pathType = pathState.Type;
            if (pathType != PathType.Dead)
            {
                var sState = pathState.ShadingState;
                if (sState.Material != null)
                {
                    return GetRadiance(pathState, sampleState, pathInfo, traceDepth);
//                     return m_photonMap.VisualizePhotons(pathState.Wo, pathState.ShadingState, 0.85f);
//                     return m_importonMap.VisualizeImportons(m_world, pathState.ShadingState);
                }
                else if (sState.Light != null)
                {
                    if (pathType == PathType.Eye || pathType == PathType.Specular)
                    {
                        IEnvLight envLight = sState.Light as IEnvLight;
                        if (envLight != null)
                        {
                            return new MultiColor(envLight.GetRadiance(pathState.PathRay.Direction));
                        } 
                        else
                        {
                            return new MultiColor(new LightColor(sState.Light));
                        }
                    }
                }
            }

            return new MultiColor();
        }

        MultiColor GetRadiance(PathState pathState, SampleState sampleState, PathInfo pathInfo, int traceDepth)
        {
            var material = pathState.ShadingState.Material;

            if (traceDepth < 12)
            {
                var normal = pathState.ShadingState.Normal;

                if ((material.GetBSDFType() & BSDFType.Specular) != 0)
                {
                    MultiColor radiance = new MultiColor();

                    MaterialSampleInfo matInfo;

                    var transPath = pathState.GetMaterialPath(new Sample2(0.5f, 0.5f), BSDFType.Specular | BSDFType.Transmition, out matInfo);
                    var transRadiance = Trace(transPath, sampleState, pathInfo, traceDepth + 1);                    
                    if (transRadiance != null)
                    {
                        transRadiance.Scale(matInfo.fr);
                        radiance.Add(transRadiance);
                    }

                    //if (traceDepth > 0)
                    {
	                    var reflPath = pathState.GetMaterialPath(new Sample2(0.5f, 0.5f), BSDFType.Specular | BSDFType.Reflection, out matInfo);
	                    var reflRadiance = Trace(reflPath, sampleState, pathInfo, traceDepth + 1);
	                    if (reflRadiance != null)
	                    {
	                        reflRadiance.Scale(matInfo.fr);
	                        radiance.Add(reflRadiance);
	                    }
                    }

                    return radiance;
                } 
                else
                {
                    MultiColor radiance = new MultiColor();

	                if ((material.GetBSDFType() & BSDFType.Diffuse) != 0)
	                {
	                    radiance.Add(GetDirectRadiance(pathState, sampleState, pathInfo));
	
	                    var indirect = GetIndirectRadiance(pathState, sampleState, pathInfo);
	                    radiance.Add(indirect);
	
	                    var caustics = m_photonMap.GetCausticRadiance(pathState.Wo, pathState.ShadingState);
	                    radiance.Add(caustics);
	                }
	
	                if ((material.GetBSDFType() & BSDFType.Glossy) != 0)
	                {
	                    int glossySampleCount = pathInfo.GlossyPath ? 1 : pathInfo.GlossySampleCount;
	                    pathInfo.GlossyPath = true;
	
	                    MultiColor totalGlossyRadiance = new MultiColor();
	
	                    for (int j = 0; j < glossySampleCount; j++)
	                    {
	                        SampleState splittedSample = sampleState.SplitTrajectory(j, glossySampleCount);
	
	                        MaterialSampleInfo matInfo;
	                        var glossyPath = pathState.GetMaterialPath(splittedSample.PopSample2D(), BSDFType.Glossy, out matInfo);
                            if (glossyPath.Type != PathType.Dead)
	                        {
	                            var glossyRadiance = Trace(glossyPath, splittedSample.NextState(), pathInfo, traceDepth + 1);
	                            if (glossyRadiance != null)
	                            {
	                                glossyRadiance.Scale(matInfo.fr * Vector3.AbsDot(matInfo.wi, normal) / matInfo.bsdfPdf);
	
	                                totalGlossyRadiance.Add(glossyRadiance);
	                            }
	                        }
	                    }
	                    totalGlossyRadiance.Scale(1.0f / glossySampleCount);
	
	                    radiance.Add(totalGlossyRadiance);
	                }

                    return radiance;
                }
            }

            return null;
        }

        private MultiColor GetIndirectRadiance(PathState pathState, SampleState sampleState, PathInfo pathInfo)
        {
            MultiColor indirect = new MultiColor();

            int indirectSampleCount = pathInfo.GlossyPath ? Math.Max(1, pathInfo.IndirectSampleCount / pathInfo.GlossySampleCount) : pathInfo.IndirectSampleCount;
            for (int j = 0; j < indirectSampleCount; j++)
            {
                SampleState splittedSample = sampleState.SplitTrajectory(j, indirectSampleCount);

                indirect.Add(m_photonMap.FinalGather(pathState, splittedSample, 0));
            }
            indirect.Scale(1.0f / indirectSampleCount);

//             indirect = m_photonMap.ImportanceFinalGather(pathState, sampleState, indirectSampleCount);

            return indirect;
        }


        private MultiColor GetDirectRadiance(PathState pathState, SampleState sampleState, PathInfo pathInfo)
        {
            MultiColor directRadiance = new MultiColor();

            Vector3 position = pathState.ShadingState.Position;
            Vector3 normal = pathState.ShadingState.Normal;

            int sampleCount = pathInfo.GlossyPath ? Math.Max(1, pathInfo.DirectSampleCount / pathInfo.GlossySampleCount) : pathInfo.DirectSampleCount;
            if (sampleCount > 0)
            {
	            foreach (var light in m_world.Lights)
	            {
	                bool deltaLight = light.IsDelta();
	                LightColor lightRadiance = new LightColor(light, Color3.Black);

	                for (int j = 0; j < sampleCount; j++)
	                {
                        SampleState splittedSample = sampleState.SplitTrajectory(j, sampleCount);
	
	                    LightSampleInfo lightInfo;
	                    MaterialSampleInfo matInfo;
	                    var lightPath = pathState.GetVisibilityPath(splittedSample.PopSample2D(), light, out lightInfo, out matInfo);
                        float weight = deltaLight ? 1 : Utils.PowerHeuristic(sampleCount, lightInfo.pdf, sampleCount, matInfo.bsdfPdf);
                        if (weight > 0 && lightPath.Type != PathType.Dead)
	                    {
                            var radiance = lightInfo.L * matInfo.fr * Vector3.AbsDot(normal, matInfo.wi) * weight / lightInfo.pdf;
                            lightRadiance += radiance;
	                    }

	                    if (!deltaLight)
                        {
		                    var bsdfPath = pathState.GetLightProbingPath(splittedSample.PopSample2D(), splittedSample.PopSample1D(), light, out lightInfo, out matInfo);
                            weight = Utils.PowerHeuristic(sampleCount, matInfo.bsdfPdf, sampleCount, lightInfo.pdf);
                            if (weight > 0 && bsdfPath.Type != PathType.Dead)
		                    {
                                lightRadiance += lightInfo.L * matInfo.fr * Vector3.AbsDot(normal, matInfo.wi) * weight / matInfo.bsdfPdf;                     
		                    }
	                    }
	                }

                    directRadiance.Add(lightRadiance);
	            }
	            directRadiance.Scale(1.0f / sampleCount);
            }

            return directRadiance;
        }
    }
}
