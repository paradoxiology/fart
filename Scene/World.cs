﻿using System;
using System.Collections.Generic;

using Fart.Geometry;
using Fart.Maths;
using Fart.Render;
using Fart.Shading;

namespace Fart.Scene
{
    public class World
    {
        private List<ILight> m_lights = new List<ILight>();
        private List<Model> m_models = new List<Model>();

        public BIH m_bih = new BIH();

        public BBox Bound
        {
            get
            {
                return m_bih.Bound;
            }
        }

        public World()
        {
            ObJModelLoader loader = new ObJModelLoader("E:\\Projects\\Fart\\ajax box.obj");
            var meshModels = loader.LoadModels();
            foreach (var mesh in meshModels)
            {
//                 mesh.Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));

                m_models.Add(mesh);

                m_bih.AddShapes(mesh.Shapes);
            }

//             var sphere = new SphereModel(Vector3.Make(6.416333f, 106.001045f, 1.668210f), 10.65f);
//             sphere.Material = new Glass(1.6f);
//             m_models.Add(sphere);
//             m_bih.AddShape(sphere.GetShape());
// 
//             sphere = new SphereModel(Vector3.Make(-1, 0, 0), 0.8f);
//             sphere.Material = new Phong(Color3.Make(0.0f, 0.0f, 0.0f), Color3.Make(1f, 0.2f, 1f), 80);
//             m_models.Add(sphere);
//             m_bih.AddShape(sphere.GetShape());

// 
//             m_models[0].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
//             m_models[1].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
//             m_models[2].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.2f, 0.2f));
//             m_models[3].Material = new DiffuseMaterial(Color3.Make(0.2f, 0.8f, 0.2f));
//             m_models[4].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
//             m_models[5].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
//             m_models[6].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));

//             m_models[5].Material = new SpecularMaterial();
//             m_models[m_models.Count - 1].Material = new Glass(1.6f);
//             m_models[0].Material = new Glass(1.6f);
//             m_models[2].Material = new Glass(1.6f);

//             m_models[0].Material = new SpecularMaterial();
//             m_models[1].Material = new SpecularMaterial(1.5f);

//             m_models[2].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));
//             m_models[3].Material = new DiffuseMaterial(Color3.Make(0.8f, 0.2f, 0.2f));
//             m_models[4].Material = new DiffuseMaterial(Color3.Make(0.2f, 0.8f, 0.2f));
//             m_models[0].Material = new Phong(Color3.Make(0.0f, 0.0f, 0.0f), Color3.Make(1f, 1f, 1f), 50);
//             m_models[6].Material = new Phong(Color3.Make(0.0f, 0.0f, 0.0f), Color3.Make(1f, 1f, 1f), 100);
//             m_models[0].Material = new SpecularMaterial(1.5f);// new DiffuseMaterial(Color3.Make(0.8f, 0.8f, 0.8f));


//             Lights.Add(new PointLight(Vector3.Make(5.5f, 4.0f, 5.5f), Color3.White * 100.0f));
//             Lights.Add(new PointLight(Vector3.Make(-5.5f, 4.0f, 5.5f), Color3.White * 100.0f));
//             Lights.Add(new PointLight(Vector3.Make(0, 0, 2), Color3.Make(0.25f, 0.5f, 0.2f), Color3.White*0.3f));
//            Lights.Add(new PointLight(Vector3.Make(-1, 2, 0), Color3.Make(0.2f, 0.2f, 0.4f), Color3.White*0.3f));
//             Lights.Add(new SphereLight(Vector3.Make(0.5f, 1.5f, 0.95f), 0.5f, Color3.Make(5, 5, 5) * 3));
//             Lights.Add(new SphereLight(Vector3.Make(-0.3f, -0.5f, 1.1f), 0.5f, Color3.Make(4, 4, 7)*3));
//             Lights.Add(new SphereLight(Vector3.Make(-1.5f, 1.5f, 1), 0.25f, Color3.Make(5, 5, 5) * 5));
//             Lights.Add(new SphereLight(Vector3.Make(0, 1.5f, 8), 0.5f, Color3.Make(2, 2, 2) * 4));
//             Lights.Add(new SphereLight(Vector3.Make(5.5f, 4.0f, 5.5f), 0.85f, Color3.Make(5, 5, 5) * 20));
//             Lights.Add(new SphereLight(Vector3.Make(-8, 10, 18.5f), 1.0f, Color3.Make(5, 5, 5) * 5));

//              Lights.Add(new SphereLight(Vector3.Make(1, 2, 5), 0.255f, Color3.Make(5, 5, 5) * 5));

//             Lights.Add(new SphereLight(Vector3.Make(15, 12, 8.5f), 0.25f, Color3.Make(100, 100, 100)));
//             Lights.Add(new SphereLight(Vector3.Make(15, 15, -50), 15f, Color3.Make(80, 80, 80)));
            Lights.Add(new SphereLight(Vector3.Make(0.154f, 1.54f, 1.91f), 0.05f, Color3.Make(25, 25, 25) * 4));
//             Lights.Add(new SphereLight(Vector3.Make(0.154f, 1.54f, 0.12f), 0.05f, Color3.Make(25, 25, 25) * 4));
//             Lights.Add(new SphereLight(Vector3.Make(0.154f, 1.54f, -1.54f), 0.05f, Color3.Make(25, 25, 25) * 4));

//             Lights.Add(new DayLight(0.5f, 0, 5));
//             Lights.Add(new SunLight(0.5f, 0, 5));

            m_bih.Build();
        }

        public List<ILight> Lights
        {
            get { return m_lights; }
        }

        public List<Model> Models
        {
            get { return m_models; }
        }

        public bool GetShadingState(Ray ray, bool includeLights, out ShadingState sState)
        {
            InterState iState;
            ILight hitLight = null;
            Material hitMaterial = null;
            sState = null;

            bool interLight = false, interModel = false;

            ShadedShape interShape;
            if (m_bih.Intersect(ray, out iState, out interShape))
            {
                interModel = true;

                hitMaterial = interShape.Model.Material;
            }

            if (includeLights)
            {
                float closest = float.MaxValue;
                ILight closestLight = null;
                InterState lightInterState = null; ;

                foreach (var light in m_lights)
                {
                    InterState tempState;

                    if (light.Intersect(ray, out tempState))
                    {
                        interLight = true;

                        if (tempState.Distance < closest)
                        {
                            closest = tempState.Distance;
                            closestLight = light;

                            lightInterState = tempState;
                        }
                    }
                }

                if (interLight)
                {
                    if (!interModel || iState == null || closest < iState.Distance)
                    {
	                    hitLight = closestLight;
	                    iState = lightInterState;
	
	                    hitMaterial = null;
                    }
                }
            }

            if (interModel || interLight)
            {
                sState = new ShadingState(iState, hitMaterial, hitLight);
            }

            return interModel || interLight;
        }

        public bool CheckVisibility(Ray visiRay, Model subject)
        {
//             InterState iState;
//             ShadedShape interShape;

            if (m_bih.Intersect(visiRay))
            {
                return false;
            }

            return true;
        }
    }
}
