﻿using System;
using System.Collections.Generic;
using Fart.Geometry;
using Fart.Maths;
using Fart.Render;
using Fart.Shading;


namespace Fart.Scene
{
    public sealed class SphereLight: ILight
    {
        private Color3 m_emitRadiance; // emitted radiance of the light(flux per unit area per unit angle)
        private Sphere m_sphere;

        public SphereLight(Vector3 center, float radius, Color3 emitRadiance)
        {
            m_sphere = new Sphere(center, radius);
            m_emitRadiance = emitRadiance;
        }

        public void SetRadiance(Color3 radiance)
        {
            m_emitRadiance = radiance;
        }

        public Color3 GetRadiance()
        {
            return m_emitRadiance;
        }

        public bool IsDelta()
        {
            return false;
        }

        public Color3 GetPower()
        {
            return m_emitRadiance * m_sphere.Area() * Utils.PI;
        }

        public bool Intersect(Ray ray)
        {
            return m_sphere.Intersect(ray);
        }

        public bool Intersect(Ray ray, out InterState interState)
        {
            return m_sphere.Intersect(ray, out interState);
        }

        public LightColor SampleLight(Vector3 position, Vector3 normal, Sample2 s, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            Vector3 normalOnLight;
            Vector3 pointOnSphere = m_sphere.SampleShape(position, s, out normalOnLight);
            wi = pointOnSphere - position;
            float dis = wi.Length;
            pdf = m_sphere.PDF(position, wi.GetNormalized());
            visiRay = new Ray(position + normal * Ray.MIN_T, wi, dis);

//             return (Vector3.Dot(normalOnLight, -wi) > 0.0f) ? new LightColor(this): new LightColor(this, Color3.Black); 
            return new LightColor(this);
        }

        public LightSampleInfo SampleLight(Vector3 position, Vector3 normal, Sample2 s)
        {
            Vector3 wi;
            float lightPdf;
            Ray visiRay;

            LightColor L = SampleLight(position, normal, s, out wi, out lightPdf, out visiRay);

            return new LightSampleInfo { visiRay = visiRay, L = L, pdf = lightPdf };
        }

        public LightColor ScatterLight(World world, Sample2 s0, Sample2 s1, out float pdf, out Ray lightRay)
        {
            Vector3 normalOnLight;
            Vector3 p = m_sphere.SampleShape(s1, out normalOnLight);
            Vector3 rndDir = Utils.UniformRandDir(s0);

            if (Vector3.Dot(normalOnLight, rndDir) < 0)
            {
                rndDir *= -1;
            }

            pdf = m_sphere.PDF(p) * (Utils.INV_PI / 2);
            lightRay = new Ray(p, rndDir);

            return new LightColor(this);
        }

        public LightColor SampleLightDirect(Vector3 position, Vector3 normal, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            wi = m_sphere.Center - position;
            float dis = wi.Length;
            pdf = m_sphere.PDF(position, wi.GetNormalized());
            visiRay = new Ray(position + normal * Ray.MIN_T, wi, dis);

            return new LightColor(this);
        }

        public float PDF(Vector3 position, Vector3 wi)
        {
            return m_sphere.PDF(position, wi);
        }

    }
}
