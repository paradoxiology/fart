﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;
using Fart.Shading;
using Fart.Geometry;

namespace Fart.Scene
{
    public sealed class SphereModel: Model
    {
        ShadedShape m_shadedShape;
        Sphere m_sphere;

        public SphereModel(Vector3 center, float radius)
        {
            m_sphere = new Sphere(center, radius);
            m_shadedShape = new ShadedShape(m_sphere, this, null);
        }

        public ShadedShape GetShape()
        {
            return m_shadedShape;
        }

        public override bool GetShadingState(World world, Ray ray, out ShadingState sState)
        {
            InterState interState;

            if (m_sphere.Intersect(ray, out interState))
            {
                sState = new ShadingState(interState, Material, null);

                return true;
            }

            sState = null;

            return false;
        }

        public override bool Intersect(Ray ray)
        {
            return m_sphere.Intersect(ray);
        }

        public override bool Intersect(Ray ray, out InterState interState)
        {
            return m_sphere.Intersect(ray, out interState);
        }

        public override void Transform(Matrix4 transformation)
        {
            throw new NotImplementedException();
        }

        public override float Area()
        {
            return m_sphere.Area();
        }

        public override Vector3 SampleShape(Sample2 s, out Vector3 normal)
        {
            return m_sphere.SampleShape(s, out normal);
        }

        public sealed override Vector3 SampleShape(Vector3 pos, Sample2 s, out Vector3 normal)
        {
            return m_sphere.SampleShape(pos, s, out normal);
        }

        public sealed override BBox GetBounding()
        {
            return m_sphere.GetBounding();
        }
    }
}
