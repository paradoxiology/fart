﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;
using Fart.Geometry;

namespace Fart.Scene
{
    public class SunLight: ILight
    {
        class Sun
        {
            // constant data
            private static readonly float[] solAmplitudes = { 165.5f, 162.3f, 211.2f,
                    258.8f, 258.2f, 242.3f, 267.6f, 296.6f, 305.4f, 300.6f, 306.6f,
                    288.3f, 287.1f, 278.2f, 271.0f, 272.3f, 263.6f, 255.0f, 250.6f,
                    253.1f, 253.5f, 251.3f, 246.3f, 241.7f, 236.8f, 232.1f, 228.2f,
                    223.4f, 219.7f, 215.3f, 211.0f, 207.3f, 202.4f, 198.7f, 194.3f,
                    190.7f, 186.3f, 182.6f };
            private static readonly RegularSpectralCurve solCurve = new RegularSpectralCurve(solAmplitudes, 380, 750);
            private static readonly float[] k_oWavelengths = { 300, 305, 310, 315, 320,
                    325, 330, 335, 340, 345, 350, 355, 445, 450, 455, 460, 465, 470,
                    475, 480, 485, 490, 495, 500, 505, 510, 515, 520, 525, 530, 535,
                    540, 545, 550, 555, 560, 565, 570, 575, 580, 585, 590, 595, 600,
                    605, 610, 620, 630, 640, 650, 660, 670, 680, 690, 700, 710, 720,
                    730, 740, 750, 760, 770, 780, 790, };
            private static readonly float[] k_oAmplitudes = { 10.0f, 4.8f, 2.7f, 1.35f,
                    .8f, .380f, .160f, .075f, .04f, .019f, .007f, .0f, .003f, .003f,
                    .004f, .006f, .008f, .009f, .012f, .014f, .017f, .021f, .025f,
                    .03f, .035f, .04f, .045f, .048f, .057f, .063f, .07f, .075f, .08f,
                    .085f, .095f, .103f, .110f, .12f, .122f, .12f, .118f, .115f, .12f,
                    .125f, .130f, .12f, .105f, .09f, .079f, .067f, .057f, .048f, .036f,
                    .028f, .023f, .018f, .014f, .011f, .010f, .009f, .007f, .004f, .0f,
                    .0f };
            private static readonly float[] k_gWavelengths = { 759, 760, 770, 771 };
            private static readonly float[] k_gAmplitudes = { 0, 3.0f, 0.210f, 0 };
            private static readonly float[] k_waWavelengths = { 689, 690, 700, 710, 720,
                    730, 740, 750, 760, 770, 780, 790, 800 };
            private static readonly float[] k_waAmplitudes = { 0f, 0.160e-1f, 0.240e-1f,
                    0.125e-1f, 0.100e+1f, 0.870f, 0.610e-1f, 0.100e-2f, 0.100e-4f,
                    0.100e-4f, 0.600e-3f, 0.175e-1f, 0.360e-1f };

            private static readonly IrregularSpectralCurve k_oCurve = new IrregularSpectralCurve(k_oWavelengths, k_oAmplitudes);
            private static readonly IrregularSpectralCurve k_gCurve = new IrregularSpectralCurve(k_gWavelengths, k_gAmplitudes);
            private static readonly IrregularSpectralCurve k_waCurve = new IrregularSpectralCurve(k_waWavelengths, k_waAmplitudes);


            public static SpectralCurve ComputeAttenuatedSunlight(float sunTheta, float turbidity)
            {
                float[] data = new float[91];  // (800 - 350) / 5  + 1 

                const double alpha = 1.3;
                const double lozone = 0.35;
                const double w = 2.0;
                double beta = 0.04608365822050 * turbidity - 0.04586025928522;

                // Relative optical mass
                double m = 1.0 / (Math.Cos(sunTheta) + 0.000940 * Math.Pow(1.6386 - sunTheta, -1.253));

                for (int i = 0, lambda = 350; lambda <= 800; i++, lambda += 5)
                {
                    // Rayleigh scattering
                    double tauR = Math.Exp(-m * 0.008735 * Math.Pow(lambda / 1000.0, -4.08));
                    // Aerosol (water + dust) attenuation
                    double tauA = Math.Exp(-m * beta * Math.Pow(lambda / 1000.0, -alpha));
                    // Attenuation due to ozone absorption
                    double tauO = Math.Exp(-m * k_oCurve.Sample(lambda) * lozone);
                    // Attenuation due to mixed gases absorption
                    double tauG = Math.Exp(-1.41 * k_gCurve.Sample(lambda) * m / Math.Pow(1.0 + 118.93 * k_gCurve.Sample(lambda) * m, 0.45));
                    // Attenuation due to water vapor absorption
                    double tauWA = Math.Exp(-0.2385 * k_waCurve.Sample(lambda) * w * m / Math.Pow(1.0 + 20.07 * k_waCurve.Sample(lambda) * w * m, 0.45));
                    // 100.0 comes from solAmplitudes begin in wrong units.
                    double amp = /* 100.0 * */solCurve.Sample(lambda) * tauR * tauA * tauO * tauG * tauWA;

                    data[i] = (float) amp;
                }

                return new RegularSpectralCurve(data, 350, 800);
            }
        }

        Color3 m_sunRadiance;
        Vector3 m_sunDir;


        public SunLight(float sunTheta, float sunPhi, float turbidity)
        {
            m_sunRadiance = Sun.ComputeAttenuatedSunlight(sunTheta, turbidity).XYZ.RGB * 0.001f;

            m_sunDir = Utils.SphericalToDir(Vector3.Make(0, 1, 0), sunTheta, sunPhi);
        }

        public Color3 GetRadiance()
        {
            return m_sunRadiance;
        }

        public Color3 GetPower()
        {
            return Color3.White * 4;
        }

        public bool IsDelta()
        {
            return true;
        }

        public LightColor SampleLight(Vector3 position, Vector3 normal, Sample2 s, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            throw new NotImplementedException();
        }

        public LightColor ScatterLight(World world, Sample2 s0, Sample2 s1, out float pdf, out Ray lightRay)
        {
            Vector3 v1, v2;
            Utils.CoordSystem(m_sunDir, out v1, out v2);

            float dx, dy;
            Utils.ConcentricSampleDisk(s1.s0, s1.s1, out dx, out dy);

            Vector3 worldCenter = world.Bound.Center;
            float worldSquaredRadius = world.Bound.SquaredRadius;
            float worldRadius = Utils.Sqrtf(worldSquaredRadius);
            Vector3 scenePoint = worldCenter + (v1 * dx + v2 * dy) * worldRadius;

            pdf = 1 / (Utils.PI * worldSquaredRadius);

            lightRay = new Ray(scenePoint + m_sunDir * worldRadius, -m_sunDir);

            return new LightColor(this);                        
        }

        public LightColor SampleLightDirect(Vector3 position, Vector3 normal, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            throw new NotImplementedException();
        }

        public LightSampleInfo SampleLight(Vector3 position, Vector3 normal, Sample2 s)
        {
            //if (Vector3.Dot(normal, m_sunDir) > 0)
            {
                Vector3 wc = m_sunDir;
                Vector3 wcX, wcY;
                Utils.CoordSystem(wc, out wcX, out wcY);

                Vector3 fuzzySunDir = Utils.UniformSampleCone(s, 0.9999f, wcX, wcY, wc);
                Ray visiRay = new Ray(position + normal * Ray.MIN_T, fuzzySunDir);

                return new LightSampleInfo { L = new LightColor(this), pdf = 1, visiRay = visiRay };
            }

//             return new LightSampleInfo();
        }

        public float PDF(Vector3 position, Vector3 wi)
        {
            return 0;
        }

        public bool Intersect(Ray ray)
        {
            return false;
        }

        public bool Intersect(Ray ray, out InterState interState)
        {
            interState = null;

            return false;
        }
    }
}
