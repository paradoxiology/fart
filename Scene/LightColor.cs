﻿using Fart.Maths;
using System;


namespace Fart.Scene
{
    public struct LightColor
    {
        public ILight Light
        {
            get;
            private set;
        }

        public Color3 Factor
        {
            get;
            set;
        }

        public Color3 Color
        {
            get
            {
                if (Light != null)
                {
                    return Light.GetRadiance() * Factor;
                }
                else
                {
                    return Color3.Black;
                }
            }
        }

        public LightColor(ILight light): this()
        {
            Light = light;
            Factor = Color3.White;
        }

        public LightColor(ILight light, Color3 factor): this()
        {
            Light = light;
            Factor = factor;
        }

        public static LightColor Add(LightColor lhs, LightColor rhs)
        {
            if (lhs.Light != rhs.Light)
            {
                throw new InvalidOperationException("Only radiance/color from the same light source can be added");
            }

            return new LightColor(lhs.Light, lhs.Factor + rhs.Factor);
        }

        public static LightColor operator+(LightColor lhs, LightColor rhs)
        {
            return Add(lhs, rhs);
        }

        public static LightColor Sub(LightColor lhs, LightColor rhs)
        {
            if (lhs.Light != rhs.Light)
            {
                throw new InvalidOperationException("Only radiance/color from the same light source can subtract each other");
            }

            return new LightColor(lhs.Light, lhs.Factor - rhs.Factor);
        }

        public static LightColor operator-(LightColor lhs, LightColor rhs)
        {
            return Sub(lhs, rhs);
        }

        public static LightColor Mul(LightColor lhs, LightColor rhs)
        {
            if (lhs.Light != rhs.Light)
            {
                throw new InvalidOperationException("Only radiance/color from the same light source can be multiplied");
            }

            return new LightColor(lhs.Light, lhs.Factor * rhs.Factor);
        }

        public static LightColor Mul(LightColor lhs, Color3 rhs)
        {
            return new LightColor(lhs.Light, lhs.Factor * rhs);
        }


        public static LightColor Scale(LightColor lhs, float factor)
        {
            return new LightColor(lhs.Light, lhs.Factor * factor);
        }

        public static LightColor operator*(LightColor lhs, LightColor rhs)
        {
            return Mul(lhs, rhs);
        }

        public static LightColor operator *(LightColor lhs, Color3 rhs)
        {
            return Mul(lhs, rhs);
        }

        public static LightColor operator*(LightColor lhs, float factor)
        {
            return Scale(lhs, factor);
        }

        public static LightColor operator/(LightColor lhs, float factor)
        {
            return Scale(lhs, 1.0f / factor);
        }

        public override bool Equals(object obj)
        {
            if (obj is LightColor)
            {
                LightColor other = (LightColor) obj;

                return this.Light == other.Light;
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Light != null)
            {
                return Light.GetHashCode();
            }
            else
            {
                return 0;
            }            
        }
    }
}
