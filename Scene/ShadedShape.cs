﻿
using Fart.Geometry;
using Fart.Shading;

namespace Fart.Scene
{
    public struct ShadedShape
    {
        public Shape Shape
        {
            get;
            private set;
        }

        public Model Model
        {
            get;
            private set;
        }

        public ILight Light
        {
            get;
            private set;
        }

        public ShadedShape(Shape shape, Model model, ILight light)
            : this()
        {
            Shape = shape;
            Model = model;
            Light = light;
        }

        public static implicit operator Shape(ShadedShape shadedTriangle)
        {
            return shadedTriangle.Shape;
        }

        public override string ToString()
        {
            return Shape.ToString();
        }
    }
}
