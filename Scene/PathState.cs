﻿using Fart.Maths;
using Fart.Shading;

namespace Fart.Scene
{
    public enum PathType
    {
        Dead,
        Eye,
        Light,
        LightProber,
        Visibility,
        Diffuse,
        Specular,
        Glossy
    }

    public sealed class PathState
    {
        bool m_traced;
        Vector3 wo;
        PathType m_pathType;
        Ray m_ray;
        World m_world;
        ShadingState m_shadingState;

        public PathType Type
        {
            get
            {
                Trace();

                return m_pathType;
            }

            private set
            {
                m_pathType = value;
            }
        }

        public ShadingState ShadingState
        {
            get
            {
                Trace();

                return m_shadingState;
            }
        }

        public Vector3 Wo
        {
            get
            {
                return wo;
            }
        }

        public Ray PathRay
        {
            get
            {
                return m_ray;
            }
        }

        public PathState()
        {
            m_pathType = PathType.Dead;

            m_traced = true;
        }

        public PathState(World world, Ray ray, PathType type)
        {
            m_traced = false;

            wo = -ray.Direction;
            m_ray = ray;
            m_world = world;

            m_pathType = type;
        }

        public PathState(World world, Ray ray, PathType type, bool trace):
            this(world, ray, type)
        {
            if (trace)
            {
                Trace();
            }
        }


        public void Offset(Vector3 offset)
        {
            if (m_ray != null)
            {
	            m_ray.Origin = m_ray.Origin + offset;
            }
        }

        PathType ChoosePathTypeFromBSDF(BSDFType bsdfType)
        {
            if ((bsdfType & BSDFType.Diffuse) != 0)
            {
                return PathType.Diffuse;
            }
            else if ((bsdfType & BSDFType.Glossy) != 0)
            {
                return PathType.Glossy;
            }
            else if ((bsdfType & BSDFType.Specular) != 0)
            {
                return PathType.Specular;
            }

            return PathType.Dead;
        }

        public PathState GetMaterialPath(BSDFType bsdfType, Vector3 wi, out MaterialSampleInfo matInfo)
        {
            if (m_shadingState.Material != null)
            {
                float bsdfPdf = m_shadingState.Material.PDF(m_shadingState, wo, wi);
                if (bsdfPdf != 0)
                {
                    var fr = m_shadingState.Material.f(bsdfType, m_shadingState, wi, wo);
                    if (fr != Color3.Black)
                    {
	                    matInfo = new MaterialSampleInfo { sampledType = bsdfType, bsdfPdf = bsdfPdf, fr = fr, wi = wi };

                        Vector3 rayOrig = m_shadingState.Position + wi * Ray.MIN_T;
	                    return new PathState(m_world, new Ray(rayOrig, wi), ChoosePathTypeFromBSDF(bsdfType));
                    }
                }
            }

            matInfo = new MaterialSampleInfo();

            return new PathState();
        }

        public PathState GetMaterialPath(Sample2 dirSample, Sample1 bsdfSample, out MaterialSampleInfo matInfo)
        {
            if (m_shadingState.Material != null)
            {
                matInfo = m_shadingState.Material.Sample(dirSample, 
                                                        bsdfSample, 
                                                        m_shadingState, 
                                                        wo);
                if (matInfo.bsdfPdf != 0 && matInfo.fr != Color3.Black)
                {
                    PathType pathType = ChoosePathTypeFromBSDF(matInfo.sampledType);

                    Vector3 rayOrig = m_shadingState.Position;
                    if ((matInfo.sampledType & BSDFType.Reflection) != 0)
                    {
                        rayOrig += (m_shadingState.GeoNormal + wo) * Ray.MIN_T;
                    } 
                    else
                    {
                        rayOrig += matInfo.wi * Ray.MIN_T;
                    }
                    return new PathState(m_world, new Ray(rayOrig, matInfo.wi), pathType);
                }
            }

            matInfo = new MaterialSampleInfo();

            return new PathState();
        }

        public PathState GetMaterialPath(Sample2 dirSample, BSDFType bsdfType, out MaterialSampleInfo matInfo)
        {
            if (m_shadingState.Material != null)
            {
                matInfo = m_shadingState.Material.Sample(dirSample,
                                                        bsdfType,
                                                        m_shadingState,
                                                        wo);
                if (matInfo.bsdfPdf != 0 && matInfo.fr != Color3.Black)
                {
                    PathType pathType = ChoosePathTypeFromBSDF(matInfo.sampledType);

                    Vector3 rayOrig = m_shadingState.Position;
                    if ((matInfo.sampledType & BSDFType.Specular) == 0)
                    {
                        rayOrig += m_shadingState.GeoNormal * Ray.MIN_T;
                    }
                    else
                    {
                        bool backFaced = matInfo.backFaced;
                        bool reflection = (matInfo.sampledType & BSDFType.Reflection) != 0;
                        bool transmition = (matInfo.sampledType & BSDFType.Transmition) != 0;

                        if ((!backFaced && reflection) || (backFaced && transmition))
                        {
                            rayOrig += m_shadingState.GeoNormal * Ray.MIN_T;
                        }
                        else if ((!backFaced && transmition) || (backFaced && reflection))
                        {
                            rayOrig -= m_shadingState.GeoNormal * Ray.MIN_T;
                        }
                    }
                    return new PathState(m_world, new Ray(rayOrig, matInfo.wi), pathType);
                }
            }

            matInfo = new MaterialSampleInfo();

            return new PathState();
        }

        public PathState GetLightProbingPath(Sample2 dirSample, Sample1 bsdfSample, ILight light, out LightSampleInfo lightInfo, out MaterialSampleInfo matInfo)
        {
            if (m_shadingState.Material != null)
            {
                matInfo = m_shadingState.Material.Sample(dirSample,
                                                         bsdfSample,
                                                         m_shadingState,
                                                         wo);

                if (matInfo.bsdfPdf > 0)
                {
                    Vector3 rayOrig = m_shadingState.Position + m_shadingState.GeoNormal * Ray.MIN_T;
	                var lightPath = new PathState(m_world, new Ray(rayOrig, matInfo.wi), PathType.LightProber);
	                float lightPdf = light.PDF(m_shadingState.Position, matInfo.wi);
	                if (lightPdf > 0 && lightPath.Type != PathType.Dead && lightPath.ShadingState.Light == light)
	                {
	                    var Le = new LightColor(light);
	                    var envLight = light as IEnvLight;
	                    if (envLight != null)
	                    {
	                        Le = envLight.GetRadiance(lightPath.PathRay.Direction);
	                        if (Le.Factor == Color3.Black)
	                        {
	                            lightPdf = 0;
	                        }
	                    }
	
	                    lightInfo = new LightSampleInfo { L = Le, pdf = lightPdf, visiRay = lightPath.PathRay };
	                    return lightPath;
	                }
                }
            }

            lightInfo = new LightSampleInfo();
            matInfo = new MaterialSampleInfo();

            return new PathState();
        }

        public PathState GetVisibilityPath(Sample2 areaSample, ILight light, out LightSampleInfo lightInfo, out MaterialSampleInfo matInfo)
        {
            lightInfo = light.SampleLight(m_shadingState.Position, m_shadingState.Normal, areaSample);

            Material material = m_shadingState.Material;
            if (lightInfo.pdf > 0 && material != null)
            {
                matInfo.backFaced = false;
                matInfo.sampledType = BSDFType.None;
                matInfo.wi = lightInfo.visiRay.Direction;
                matInfo.fr = material.f(m_shadingState, matInfo.wi, wo);
                matInfo.bsdfPdf = material.PDF(m_shadingState, wo, matInfo.wi);

                if (matInfo.bsdfPdf > 0)
                {
	                return new PathState(m_world, lightInfo.visiRay, PathType.Visibility);
                }
            }

            matInfo = new MaterialSampleInfo();

            return new PathState();
        }

        void Trace()
        {
            if (!m_traced)
            {
                switch (m_pathType)
	            {
	            case PathType.Visibility:
	                if (!m_world.CheckVisibility(m_ray, null))
	                {
                        m_pathType = PathType.Dead;
	                }
	                break;
	            case PathType.LightProber:
	                if (!m_world.GetShadingState(m_ray, true, out m_shadingState))
	                {
                        m_pathType = PathType.Dead;
	                }
	                break;
	            default:
	                if (!m_world.GetShadingState(m_ray, true, out m_shadingState))
	                {
                        m_pathType = PathType.Dead;
	                }
	            	break;
	            }
            }

            m_traced = true;
        }
    }
}
