﻿using System;

using Fart.Shading;
using Fart.Maths;

namespace Fart.Scene
{
    public abstract class Model: Geometry.Shape
    {
        public Material Material { get; set; }

        public abstract bool GetShadingState(World world, Ray ray, out ShadingState sState);
    }
}
