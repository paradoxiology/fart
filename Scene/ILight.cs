﻿using Fart.Geometry;
using Fart.Maths;

namespace Fart.Scene
{
    public struct LightSampleInfo
    {
        public Ray visiRay;
        public float pdf;
        public LightColor L;
    }

    public interface ILight: IIntersectable
    {
        bool IsDelta();

        Color3 GetRadiance();

        Color3 GetPower();

        // Sample the light from a known position, wi is the incoming light ray from the light src.
        LightColor SampleLight(Vector3 position, Vector3 normal, Sample2 s, out Vector3 wi, out float pdf, out Ray visiRay);

        // Get a light ray sample emitted from the light source, s0 is a sample for choosing a point in 
        // the light geometry, s1 is a sample for choosing direction.
        LightColor ScatterLight(World world, Sample2 s0, Sample2 s1, out float pdf, out Ray lightRay);

        // Return the direct illumination for the light(Whitted illumination).
        LightColor SampleLightDirect(Vector3 position, Vector3 normal, out Vector3 wi, out float pdf, out Ray visiRay);

        // Sample the light from a known position, wi is the incoming light ray from the light src.
        LightSampleInfo SampleLight(Vector3 position, Vector3 normal, Sample2 s);

        // Return the light PDF at position with the incoming direction wi.
        float PDF(Vector3 position, Vector3 wi);
    }
}
