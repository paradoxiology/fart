﻿using Fart.Maths;

namespace Fart.Scene
{
    public interface IEnvLight: ILight
    {
        LightColor GetRadiance(Vector3 dir);
    }
}
