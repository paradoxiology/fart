﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;


namespace Fart.Scene
{
    public class Camera
    {
        public Vector3 Position
        {
            get;
            private set;
        }

        Vector3 m_forward, m_up;

        public float FieldOfView
        {
            get;
            private set;
        }

        Vector3 m_right;

        float m_imageExtentX, m_imageExtentY;

        int m_imageWidth, m_imageHeight;

        public int ImageWidth
        {
            get
            {
                return m_imageWidth;
            }
        }

        public int ImageHeight
        {
            get
            {
                return m_imageHeight;
            }
        }

        public void SetImageSize(int width, int height)
        {
            m_imageWidth = width;

            m_imageHeight = height;
        }

        public void Lookat(Vector3 position, Vector3 target, Vector3 up, float fov)
        {
            if (m_imageWidth <= 0 || m_imageHeight <= 0)
            {
                throw new InvalidOperationException("Image size is not set properly!");
            }

            Position = position;
            m_up = up;

            m_forward = target - position;
            m_forward.GetNormalized();

            m_right = Vector3.Cross(m_forward, m_up);
            m_right.GetNormalized();

            m_up = Vector3.Cross(m_right, m_forward);

            float aspect = (float) m_imageWidth / m_imageHeight;
            m_imageExtentX = (float) Math.Tan(0.5 * fov * Math.PI /180.0);
            m_imageExtentY = (float) Math.Tan(0.5 * fov / aspect * Math.PI / 180.0);
            FieldOfView = fov;
        }

        public Ray GetRay(float x, float y)
        {
            float u = 2.0f * x / m_imageWidth - 1.0f;
            float v = 2.0f * y / m_imageHeight - 1.0f;

            Vector3 dir = m_right * u * m_imageExtentX - m_up * v * m_imageExtentY + m_forward;
            dir.GetNormalized();

            return new Ray(Position, dir);
        }
        
    }
}
