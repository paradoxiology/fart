﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fart.Maths;
using Fart.Geometry;

namespace Fart.Scene
{
    public class DayLight: IEnvLight
    {
        class Sky
        {
            // Distribution coefficients for the luminance(Y) distribution function
            readonly float[,] DCoeff_Y = { { 0.1787f, - 1.4630f},
                                           {-0.3554f,   0.4275f},
                                           {-0.0227f,   5.3251f},
                                           { 0.1206f, - 2.5771f},
                                           {-0.0670f,   0.3703f} };

            // Distribution coefficients for the x distribution function
            readonly float[,] DCoeff_x = { {-0.0193f, -0.2592f},
                                           {-0.0665f, 0.0008f},
                                           {-0.0004f, 0.2125f},
                                           {-0.0641f, -0.8989f},
                                           {-0.0033f, 0.0452f} };

            // Distribution coefficients for the y distribution function
            readonly float[,] DCoeff_y = { {-0.0167f, -0.2608f},
                                           {-0.0950f, 0.0092f},
                                           {-0.0079f, 0.2102f},
                                           {-0.0441f, -1.6537f},
                                           {-0.0109f, 0.0529f} };
            // Zenith x value
            readonly float[,] Zx = { {0.00166f, -0.00375f, 0.00209f, 0},
                                     {-0.02903f, 0.06377f, -0.03203f, 0.00394f},
                                     {0.11693f, -0.21196f, 0.06052f, 0.25886f} };
            // Zenith y value
            readonly float[,] Zy = { { 0.00275f, -0.00610f, 0.00317f, 0},
                                     {-0.04214f, 0.08970f, -0.04153f, 0.00516f},
                                     {0.15346f, -0.26756f, 0.06670f, 0.26688f} };
            
	        float m_T;	//Turbidity
	        float m_T2;	// Squared Turbidity

	        float m_thetaSun, m_phiSun;
            Vector3 m_sunDir;

	        float m_ZenithY, m_Zenithx, m_Zenithy;
            float[] m_YDCoeff = new float[5], m_xDCoeff = new float[5], m_yDCoeff = new float[5];

            public Sky(float thetaSun, float phiSun, float turbidity)
            {
                m_thetaSun = thetaSun;
                m_phiSun = phiSun;

                m_sunDir = Utils.SphericalToDir(Vector3.Make(0, 1, 0), thetaSun, phiSun);

                m_T = turbidity;
                m_T2 = m_T * m_T;

                float chi;

                // Zenith luminance
                chi = (4.0f / 9.0f - m_T / 120.0f) * (Utils.PI - 2.0f * m_thetaSun);
                m_ZenithY = (4.0453f * m_T - 4.9710f) * (float) Math.Tan(chi) - 0.2155f * m_T + 2.4192f;
                if (m_ZenithY < 0)
                {
                    m_ZenithY = -m_ZenithY;
                }

                // Zenith x
                m_Zenithx = Chromaticity(Zx);

                // Zenith y
                m_Zenithy = Chromaticity(Zy);

                // Y distribution coefficients
                m_YDCoeff[0] = DCoeff_Y[0, 0] * m_T + DCoeff_Y[0, 1];
                m_YDCoeff[1] = DCoeff_Y[1, 0] * m_T + DCoeff_Y[1, 1];
                m_YDCoeff[2] = DCoeff_Y[2, 0] * m_T + DCoeff_Y[2, 1];
                m_YDCoeff[3] = DCoeff_Y[3, 0] * m_T + DCoeff_Y[3, 1];
                m_YDCoeff[4] = DCoeff_Y[4, 0] * m_T + DCoeff_Y[4, 1];

                // x distribution coefficients
                m_xDCoeff[0] = DCoeff_x[0, 0] * m_T + DCoeff_x[0, 1];
                m_xDCoeff[1] = DCoeff_x[1, 0] * m_T + DCoeff_x[1, 1];
                m_xDCoeff[2] = DCoeff_x[2, 0] * m_T + DCoeff_x[2, 1];
                m_xDCoeff[3] = DCoeff_x[3, 0] * m_T + DCoeff_x[3, 1];
                m_xDCoeff[4] = DCoeff_x[4, 0] * m_T + DCoeff_x[4, 1];

                // y distribution coefficients
                m_yDCoeff[0] = DCoeff_y[0, 0] * m_T + DCoeff_y[0, 1];
                m_yDCoeff[1] = DCoeff_y[1, 0] * m_T + DCoeff_y[1, 1];
                m_yDCoeff[2] = DCoeff_y[2, 0] * m_T + DCoeff_y[2, 1];
                m_yDCoeff[3] = DCoeff_y[3, 0] * m_T + DCoeff_y[3, 1];
                m_yDCoeff[4] = DCoeff_y[4, 0] * m_T + DCoeff_y[4, 1];
            }

            public Color3 GetSkyColor(Vector3 dir)
            {
                float theta = (float) Math.Acos(dir.y);
                float gamma = (float) Math.Acos(Vector3.Dot(dir, m_sunDir));

                return CalculateSkyColor(theta, gamma);
            }

            public Color3 GetSkyColor(float theta, float phi)
            {
                float gamma = (float) Math.Acos(Vector3.Dot(Utils.SphericalToDir(Vector3.Make(0, 1, 0), theta, phi), m_sunDir));

                return CalculateSkyColor(theta, gamma);
            }

            // gamma is the angle between ray and the sun direction.
            Color3 CalculateSkyColor(float theta, float gamma)
            {
                float Y, x, y;

                // Sky luminance
                Y = PerezFunction(m_YDCoeff, theta, gamma, m_ZenithY);

                // Sky x
                x = PerezFunction(m_xDCoeff, theta, gamma, m_Zenithx);

                // Sky y
                y = PerezFunction(m_yDCoeff, theta, gamma, m_Zenithy);

                // CIE XYZ values
                float X, Z;


                // Scale Y a little bit to get proper RGB value
                //                 Y = 1.0f - Utils.Expf(-1.0f / 8.5f * Y);

                
	            Y = Math.Min(5000, Y);// *0.1f;

                X = (x / y) * Y;
                Z = ((1 - x - y) / y) * Y;

                // Multiply XYZ by the conventional matrix
                return Color3.Make(3.240479f * X - 1.537150f * Y - 0.498535f * Z,
                                   -0.969256f * X + 1.875992f * Y + 0.041556f * Z,
                                   0.055648f * X - 0.204043f * Y + 1.057311f * Z);

            }

            float Chromaticity(float[,] ZCoeff)
            {
              // Theta, squared Theta and cubed Theta  
              float t1 = m_thetaSun;
              float t2 = t1*t1;
              float t3 = t2 * t1;

              float result = (ZCoeff[0, 0] * t3 + ZCoeff[0, 1] * t2 + ZCoeff[0, 2] * t1 + ZCoeff[0, 3]) * m_T2 +
                             (ZCoeff[1, 0] * t3 + ZCoeff[1, 1] * t2 + ZCoeff[1, 2] * t1 + ZCoeff[1, 3]) * m_T +
                             (ZCoeff[2, 0] * t3 + ZCoeff[2, 1] * t2 + ZCoeff[2, 2] * t1 + ZCoeff[2, 3]);

              return result;
            }

            float PerezFunction(float[] DCoeff, float theta, float gamma, float lvz)
            {
	            float A = DCoeff[0], B = DCoeff[1], C = DCoeff[2], D = DCoeff[3], E = DCoeff[4];

                float num = (1.0f + A * Utils.Expf(B / Utils.Cosf(theta))) * (1.0f + C * Utils.Expf(D * gamma) + E * Utils.Cosf(gamma) * Utils.Cosf(gamma));

                float den = (1.0f + A * Utils.Expf(B)) * (1.0f + C * Utils.Expf(D * m_thetaSun) + E * Utils.Cosf(m_thetaSun) * Utils.Cosf(m_thetaSun));

	            return lvz * num / den;
            }
        }

        Sky m_sky;
        SphericalImportanceSampler m_importanceSampler;

        public DayLight(float sunTheta, float sunPhi, float turbidity)
        {
            m_sky = new Sky(sunTheta, sunPhi, turbidity);

            m_importanceSampler = new SphericalImportanceSampler((theta, phi) => {
                if (theta < Utils.PI / 2)
                {
                    return m_sky.GetSkyColor(theta, phi).Intensity;
                }
                else
                {
                    return 0;
                }
            }, 256, 128);
        }

        public LightColor GetRadiance(Vector3 dir)
        {
            if (dir.y > 0)
            {
                var skyColor = m_sky.GetSkyColor(dir);
                float averageColor = skyColor.Average;
                if (float.IsNaN(averageColor) || float.IsInfinity(averageColor))
                {
                    // TODO: Occasionally GetRadiance returns NaN, gotta investigate why...
                    // and the annoying thing is that it only happens when the renderer running without
                    // being attached by a debugger, I guess it's because the JITer produces different native code
                    // in two different situations.

                    skyColor = Color3.Black;
                }

                return new LightColor(this, skyColor);
            } 
            else
            {
                return new LightColor(this, Color3.Black);
            }
        }

        public Color3 GetRadiance()
        {
            return Color3.White;
        }

        public Color3 GetPower()
        {
            return Color3.White;
        }

        public bool IsDelta()
        {
            return false;
        }

        public LightColor SampleLight(Vector3 position, Vector3 normal, Sample2 s, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            throw new NotImplementedException();
        }

        public LightColor ScatterLight(World world, Sample2 s0, Sample2 s1, out float pdf, out Ray lightRay)
        {
            float lightPdf;
            Vector2 lightSphericalDir = m_importanceSampler.Sample(s0, out lightPdf);
            Vector3 lightDir = Utils.SphericalToDir(Vector3.Make(0, 1, 0), lightSphericalDir.x, lightSphericalDir.y);

            var Le = GetRadiance(lightDir);
            if (Le.Factor == Color3.Black)
            {
                pdf = 0;
                lightRay = new Ray(Vector3.Make(0, 0, 0), Vector3.Make(1, 0, 0));

                return Le;
            }

            Vector3 v1, v2;
            Utils.CoordSystem(lightDir, out v1, out v2);

            float dx, dy;
            Utils.ConcentricSampleDisk(s1.s0, s1.s1, out dx, out dy);

            Vector3 worldCenter = world.Bound.Center;
            float worldSquaredRadius = world.Bound.SquaredRadius;
            float worldRadius = Utils.Sqrtf(worldSquaredRadius);
            Vector3 scenePoint = worldCenter + (v1 * dx + v2 * dy) * worldRadius;

            float scenePdf = 1 / (Utils.PI * worldSquaredRadius);

            pdf = lightPdf * scenePdf;
            lightRay = new Ray(scenePoint + lightDir * worldRadius, -lightDir);

            return Le;    
        }

        public LightColor SampleLightDirect(Vector3 position, Vector3 normal, out Vector3 wi, out float pdf, out Ray visiRay)
        {
            throw new NotImplementedException();
        }

        public LightSampleInfo SampleLight(Vector3 position, Vector3 normal, Sample2 s)
        {
            float pdf;
            Vector2 sphericalDir = m_importanceSampler.Sample(s, out pdf);

            Vector3 visiDir = Utils.SphericalToDir(Vector3.Make(0, 1, 0), sphericalDir.x, sphericalDir.y);
            Ray visiRay = new Ray(position + normal * Ray.MIN_T, visiDir, float.MaxValue * 0.5f);

            var Le = GetRadiance(visiDir);
            if (Le.Factor == Color3.Black)
            {
                pdf = 0;
            }

            return new LightSampleInfo { L = Le, pdf = pdf, visiRay = visiRay };
        }

        public float PDF(Vector3 position, Vector3 wi)
        {
            if (wi.y > 0)
            {
	            float theta, phi;
	            Utils.DirToSpherical(Vector3.Make(0, 1, 0), wi, out theta, out phi);
	
	            return m_importanceSampler.PDF(theta, phi);
            } 
            else
            {
                return 0;
            }
        }

        public bool Intersect(Ray ray)
        {
            return true;
        }

        public bool Intersect(Ray ray, out InterState interState)
        {
            var zero = Vector3.Make(0, 0, 0);

            interState = new InterState(zero, zero, zero, Vector2.Make(0, 0), float.MaxValue * 0.5f, null);

            return true;
        }
    }
}
