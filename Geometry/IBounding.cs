﻿
using Fart.Maths;

namespace Fart.Geometry
{
    interface IBounding: IIntersectable
    {
        bool Inside(Vector3 point);

        bool Overlap(IBounding bounding);
    }
}
