﻿
using Fart.Maths;

namespace Fart.Geometry
{
    public abstract class Shape: IIntersectable
    {
        public abstract bool Intersect(Ray ray);

        public abstract bool Intersect(Ray ray, out InterState interState);

        public abstract void Transform(Matrix4 transformation);

        public abstract BBox GetBounding();

        public abstract float Area();

        // Return a sample point on the shape, it also gives the point's normal.
        public abstract Vector3 SampleShape(Sample2 s, out Vector3 normal);

        public virtual Vector3 SampleShape(Vector3 pos, Sample2 s, out Vector3 normal)
        {
            return SampleShape(s, out normal);
        }

        public virtual float PDF(Vector3 pointOnShape)
        {
            return 1.0f / Area();
        }
        
        // PDF for a ray from pos with dir wi to hit the shape.
        // Essentially this computes the projected area of the Shape on a unit sphere.
        public virtual float PDF(Vector3 pos, Vector3 wi)
        {
            InterState iState;
            Ray ray = new Ray(pos, wi);

            if (!Intersect(ray, out iState))
            {
                return 0;
            }
            else
            {
                float squaredDis = iState.Distance * iState.Distance;
                float cosTheta = Vector3.AbsDot(iState.ShadingNormal, wi);

                return squaredDis / (cosTheta * Area());
            }
        }
    }
}
