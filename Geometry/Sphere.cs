﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fart.Maths;

namespace Fart.Geometry
{
    public sealed class Sphere: Shape
    {
        public Vector3 Center { get; private set; }
        public float Radius { get; private set; }

        public Sphere(Vector3 c, float r)
        {
            Center = c;
            Radius = r;
        }

        public override void Transform(Matrix4 transformation)
        {
            Center = transformation * Center;
        }

        public sealed override BBox GetBounding()
        {
            return new BBox(Center, Radius);
        }

        public sealed override bool Intersect(Ray ray)
        {
            Vector3 oc = Center - ray.Origin;
            float l2oc = oc.SquaredLength;
            float r2 = Radius * Radius;
            float t = 0.0f;

            if (l2oc < r2)
            {
                return true;
            }
            else
            {
                float tca = Vector3.Dot(oc, ray.Direction);
                if (tca < 0) // points away from the sphere
                {
                    return false;
                }

                float l2hc = (r2 - l2oc) + (tca * tca);
                if (l2hc <= 0)
                {
                    return false;
                }
            }

            if (t > ray.SegmentLength || t < Ray.MIN_T)
            {
                return false;
            }

            return true;
        }

        public sealed override bool Intersect(Ray ray, out InterState interState)
        {
            interState = null;

            Vector3 oc = Center - ray.Origin;
            float l2oc = oc.SquaredLength;
            float r2 = Radius * Radius;
            float t = 0.0f;

            if (l2oc < r2)
            {
                float tca = Vector3.Dot(oc, ray.Direction);
                float l2hc = (r2 - l2oc) + tca * tca;
                t = tca + Utils.Sqrtf(l2hc);
            }
            else
            {
                float tca = Vector3.Dot(oc, ray.Direction);
                if (tca < 0) // points away from the sphere
                {
                    return false;
                }

		        float l2hc = (r2 - l2oc) + (tca*tca);
		        if (l2hc > 0)
                {
                    t = tca - Utils.Sqrtf(l2hc);
                }
                else
                {
                    return false;
                }
            }

            if (t > ray.SegmentLength || t < Ray.MIN_T)
            {
                return false;
            }

            Vector3 pos = ray.ReachTo(t);
            Vector3 nor = (pos - Center) * (1 / Radius);
            interState = new InterState(pos, nor, nor, new Vector2(), t, this);

            return true;
        }

        public sealed override Vector3 SampleShape(Sample2 s, out Vector3 normal)
        {
            normal = Utils.UniformRandDir(s);
            return Center +  normal * Radius;
        }

        public sealed override float Area()
        {
            return 4.0f * Utils.PI * Radius * Radius;
        }

        public sealed override Vector3 SampleShape(Vector3 pos, Sample2 s, out Vector3 normal)
        {
            Vector3 pc = pos - Center;
            Vector3 wc = Vector3.Normalize(-pc);
            Vector3 wcX, wcY;
            Utils.CoordSystem(wc, out wcX, out wcY);

            if (pc.SquaredLength - Radius*Radius < 1e-4f)
            {
                return SampleShape(s, out normal);
            }

            float cosThetaMax = Utils.Sqrtf(Math.Max(0, 1 - Radius * Radius / pc.SquaredLength));

            Vector3 ps;
            Ray ray = new Ray(pos, Utils.UniformSampleCone(s, cosThetaMax, wcX, wcY, wc));
            InterState iState;
            if (!Intersect(ray, out iState))
            {
                ps = Center - wc * Radius;
            }
            else
            {
                ps = iState.Position;
            }

            normal = Vector3.Normalize(ps - Center);

            return ps;
        }

        public sealed override float PDF(Vector3 pos, Vector3 wi)
        {
            float squaredDis = (pos - Center).SquaredLength;

            if (squaredDis - Radius * Radius < 1e-4f)
            {
                return base.PDF(pos, wi);
            } 
            else
            {
                float cosThetaMax = Utils.Sqrtf(Math.Max(0, 1 - Radius * Radius / squaredDis));

                return Utils.UniformConePDF(cosThetaMax);
            }
        }

    }
}
