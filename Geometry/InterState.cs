﻿
using Fart.Maths;

namespace Fart.Geometry
{
    public class InterState
    {
        public Vector3 Position { get; private set;  }
        public Vector3 ShadingNormal { get; private set; }
        public Vector3 GeoNormal { get; private set; }
        public Vector2 TexCoord { get; private set;  }
        public Shape InterShape { get; private set; }
        public float Distance { get; private set; }

        public bool Hit 
        {
            get
            {
                return InterShape != null;
            }
        }
        public InterState(Vector3 normal)
        {
            ShadingNormal = normal;
            GeoNormal = normal;
        }

        public InterState(Vector3 pos, Vector3 shadingNor, Vector3 geometricNor, Vector2 tex, float dist, Shape shape)
        {
            Position = pos;
            ShadingNormal = shadingNor;
            GeoNormal = geometricNor;
            TexCoord = tex;
            Distance = dist;
            InterShape = shape;
        }
    }
}
