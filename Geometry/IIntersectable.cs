﻿
using Fart.Maths;

namespace Fart.Geometry
{
    public interface IIntersectable
    {
        bool Intersect(Ray ray);

        bool Intersect(Ray ray, out InterState interState);
    }
}
