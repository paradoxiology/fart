﻿using System;
using System.Text;

using Fart.Maths;

namespace Fart.Geometry
{
    public sealed class Triangle: Shape, ICloneable
    {
        private Vector3 v0, v1, v2; // Vertices
        private Vector3 n0, n1, n2; // Normals
        private Vector2 t0, t1, t2; // Texture Coordinates
        private Vector3 faceNormal;

        public Triangle(Vector3 v0, Vector3 v1, Vector3 v2,
                        Vector3 n0, Vector3 n1, Vector3 n2,
                        Vector2 t0, Vector2 t1, Vector2 t2,
                        Vector3 faceNormal)
        {
            this.v0 = v0;
            this.v1 = v1;
            this.v2 = v2;

            this.n0 = n0;
            this.n1 = n1;
            this.n2 = n2;

            this.t0 = t0;
            this.t1 = t1;
            this.t2 = t2;

            this.faceNormal = faceNormal;
        }

        public Vector3 GetVertex(int index)
        {
            // TODO: Maybe inefficient.
            Vector3[] verts = { v0, v1, v2 };

            return verts[index];
        }

        public Vector3 GetNormal(int index)
        {
            Vector3[] norms = { n0, n1, n2 };

            return norms[index];
        }

        public Vector2 GetTexCoord(int index)
        {
            Vector2[] texs = { t0, t1, t2 };

            return texs[index];
        }

        public override void Transform(Matrix4 transformation)
        {
            Matrix4 normalTransformation = Matrix4.Transpose(Matrix4.Invert(transformation));

            Transform(transformation, normalTransformation);
        }

        public void Transform(Matrix4 transformation, Matrix4 normalTransformation)
        {
            v0 = transformation * v0;
            v1 = transformation * v1;
            v2 = transformation * v2;

            n0 = normalTransformation * n0;
            n1 = normalTransformation * n1;
            n2 = normalTransformation * n2;

            faceNormal = normalTransformation * faceNormal;
        }

        public sealed override BBox GetBounding()
        {
            BBox box = BBox.Create();

            box.UnionPoint(v0);
            box.UnionPoint(v1);
            box.UnionPoint(v2);

            return box;
        }

        public sealed override bool Intersect(Ray ray)
        {
            return RayIntersect(ray);
        }

        public sealed override bool Intersect(Ray ray, out InterState interState)
        {
            float b1, b2, t;

            interState = null;

            bool inter = RayIntersect(ray, out b1, out b2, out t);

            if (inter)
            {
                Vector3 normal = n0 *(1.0f - b1 - b2) + n1 * b1 + n2 * b2;
                Vector2 tex = t0 * (1.0f - b1 - b2) + t1 * b1 + t2 * b2;

                interState = new InterState(ray.ReachTo(t), normal.GetNormalized(), faceNormal, tex, t, this);

                return true;
            }

            return false;
        }

        private bool RayIntersect(Ray ray)
        {
            Vector3 edge1 = Vector3.Subtract(v1, v0);
            Vector3 edge2 = Vector3.Subtract(v2, v0);

            Vector3 pVec = Vector3.Cross(ray.Direction, edge2);
            float det = Vector3.Dot(edge1, pVec);
            if (Math.Abs(det) < Utils.EPISLON)
                return false;

            float invDet = 1.0f / det;

            Vector3 tVec = Vector3.Subtract(ray.Origin, v0);
            float b1 = Vector3.Dot(tVec, pVec) * invDet;
            if (b1 < 0.0f || b1 > 1)
                return false;

            Vector3 qVec = Vector3.Cross(tVec, edge1);
            float b2 = Vector3.Dot(ray.Direction, qVec) * invDet;
            if (b2 < 0.0f || b1 + b2 > 1)
                return false;

            float t = Vector3.Dot(edge2, qVec) * invDet;
            if (t > Utils.EPISLON && t < ray.SegmentLength)
                return true;
            else
                return false;
        }

        private bool RayIntersect(Ray ray, out float b1, out float b2, out float t)
        {
            b1 = b2 = t = 0.0f;

            Vector3 edge1 = Vector3.Subtract(v1, v0);
            Vector3 edge2 = Vector3.Subtract(v2, v0);

            Vector3 pVec = Vector3.Cross(ray.Direction, edge2);
            float det = Vector3.Dot(edge1, pVec);
            if (Math.Abs(det) < Utils.EPISLON)
                return false;

            float invDet = 1.0f / det;

            Vector3 tVec = Vector3.Subtract(ray.Origin, v0);
            b1 = Vector3.Dot(tVec, pVec) * invDet;
            if (b1 < 0.0f || b1 > 1)
                return false;

            Vector3 qVec = Vector3.Cross(tVec, edge1);
            b2 = Vector3.Dot(ray.Direction, qVec) * invDet;
            if (b2 < 0.0f || b1 + b2 > 1)
                return false;

            t = Vector3.Dot(edge2, qVec) * invDet;
            if (t > Utils.EPISLON && t < ray.SegmentLength)
            {
                return true;
            }

            return false;
        }

        public override Vector3 SampleShape(Sample2 s, out Vector3 normal)
        {
            // Transform the sample to a uniform barycentric coordinates
            float ss0 = Utils.Sqrtf(s.s0);
            float b1 = 1.0f - ss0;
            float b2 = s.s1 - ss0;

            Vector3 p = v0 * (1.0f + b1 - b2) + v1 * b1 + v2 * b2;
            normal = Vector3.Cross(v1 - v0, v2 - v0).GetNormalized();

            return p;
        }

        public override float Area()
        {
            Vector3 edge1 = v1 - v0;
            Vector3 edge2 = v2 - v0;

            return 0.5f * Vector3.Cross(edge1, edge2).Length;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("f:\n");

//             sb.AppendFormat("\tv0: {0}; n0: {1}; t0: {2}\n", v0, n0, t0);
//             sb.AppendFormat("\tv1: {0}; n1: {1}; t1: {2}\n", v1, n1, t1);
//             sb.AppendFormat("\tv2: {0}; n2: {1}; t2: {2}\n", v2, n2, t2);
            sb.AppendFormat("[{0}, {1}, {2}]", v0, v1, v2);

            return sb.ToString();
        }

    }
}
