﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Fart.Maths;
using Fart.Scene;
using Fart.Render;

namespace Fart
{
    public partial class MainWindow : Form
    {
        private Thread m_renderThread;
        private Task m_renderTask;
        private TaskManager m_taskManager;
        private int m_imageWidth, m_imageHeight;
        private Stopwatch m_stopWatch = new Stopwatch();
        private RayTracer m_tracer;
        private UniformSampler m_sampler = new UniformSampler();
        private FastBitmap m_displayImage;

        private volatile bool m_pauseRender = false;
        

        public MainWindow()
        {
            InitializeComponent();

            var taskPolicy = new TaskManagerPolicy(TaskManager.Default.Policy.MinProcessors,
                                                   TaskManager.Default.Policy.IdealProcessors,
                                                   ThreadPriority.BelowNormal);
            m_taskManager = new TaskManager(taskPolicy);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_renderTask != null)
            {
                m_renderTask.CancelAndWait(1000);
            }
        }

        private void Render()
        {
            if (m_renderThread != null && m_renderThread.IsAlive)
            {
                return;
            }

            Stopwatch t = new Stopwatch();

            if (m_tracer == null)
            {
                t.Start();
                m_tracer = new RayTracer();
                t.Stop();
                OutputText(string.Format("Time taken to build the BIH tree: {0}\r\n", t.Elapsed));
            }

            m_imageWidth = picBox.Size.Width;
            m_imageHeight = picBox.Size.Height;

            if (picBox.Image != null)
            {
                picBox.Image.Dispose();
            }
            m_displayImage = new FastBitmap(m_imageWidth, m_imageHeight);
            picBox.Image = m_displayImage;

            m_tracer.ResetImageSize(m_imageWidth, m_imageHeight);

            t.Reset();
            t.Start();
            m_tracer.PopulatePhotonMap();
            t.Stop();
            OutputText(string.Format("Time taken to build the photon map: {0}\r\n", t.Elapsed));

            renderBtn.Enabled = true;
            renderBtn.Text = "Pause";

            // Spawn another thread to do the rendering
            m_renderThread = new Thread(new ThreadStart(RenderThread));
            m_renderThread.Start();
        }

        private void RenderThread()
        {
            try
            {
                // Disable Screen Saver
                ScreenSaver.SetScreenSaverActive(0);

                Fart.Render.Image image = new Fart.Render.Image();
                image.ResetSize(m_imageWidth, m_imageHeight);

	            m_stopWatch.Start();

                m_renderTask = Task.Create(_ =>
                {
                    Parallel.For(0, m_imageHeight, (h, state) =>
                    {
                        for (int w = 0; w < m_imageWidth; w++)
                        {
                            while (m_pauseRender)
                            {
                                Thread.Sleep(1000);
                            }

                            //uint ii = (uint) (QMC.GetHalton(0, (uint)(w + h * m_imageHeight)) * m_imageHeight * m_imageWidth) + (uint)i;
                            //uint ii = (uint)(w + h * m_imageHeight) + (uint)i;
                            uint ii = QMC.GenerateSeed(w, h, 15);

                            SampleState sampleState = new SampleState(ii, 2, 0, 0);

//                             float sx = (float) QMC.GetHalton(0, (uint) (i + 1) % 16) - 0.5f;
//                             float sy = (float) QMC.GetHalton(1, (uint) (i + 1) % 16) - 0.5f;
// 
                            float weight = 1;// GaussianFilter(sx, sy);//(1 - Math.Abs(sx * 2)) * (1 - Math.Abs(sy * 2));  // Triangle Filter

                            if (!m_renderTask.IsCanceled)
                            {
                                image.AddToPixel(w, h, weight, m_tracer.Trace(w, h, m_sampler, sampleState));
                            }
                            else
                            {
                                state.Stop();
                                break;
                            }
                        }

                        if (!m_renderTask.IsCanceled)
                        {
	                        Color[] scanLine = image.GetScanline(h);
	                         
	                        UpdateLinePixels(h, scanLine);
                        }
                    });
                }, m_taskManager);
                m_renderTask.Wait();

                UpdateProgressbar(1);
	
	            m_stopWatch.Stop();
	
	            OutputText(string.Format("Time taken to render: {0}\r\n", m_stopWatch.Elapsed));
	
	            m_stopWatch.Reset();

                if (!m_renderTask.IsCanceled)
                {
                    image.SaveToBitmap(m_displayImage);
                    UpdateBitmap(m_displayImage);
                }

                //renderBtn.Text = "Render"; // TODO: cross thread access!!!
            }
            catch (System.Threading.AggregateException e)
            {
                if (!(e.InnerExceptions[0] is System.Threading.Tasks.TaskCanceledException))
                {
                	MessageBox.Show(e.InnerExceptions[0].Message);
                }
            }
            finally
            {
                ScreenSaver.SetScreenSaverActive(1);
            }
        }


        float GaussianFilter(float sx, float sy)
        {
            double alpha = -0.5;
            double expv = Math.Exp(alpha * 0.5 * 0.5);

            return (float) ((Math.Exp(alpha * sx * sx) - expv) * (Math.Exp(alpha * sy * sy) - expv));
        }


        private delegate void invokeRefresh();

        private void RefreshPicBox()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new invokeRefresh(RefreshPicBox));

                return;
            }
            
            picBox.Refresh();
        }

        private delegate void invokeUpdateBitmap(FastBitmap bitmap);

        private void UpdateBitmap(FastBitmap bitmap)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new invokeUpdateBitmap(UpdateBitmap), bitmap);

                return;
            }

            picBox.Image = bitmap;
        }

        private delegate void invokeUpdateLinePixels(int h, Color[] pixelLine);

        private void UpdateLinePixels(int h, Color[] pixelLine)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new invokeUpdateLinePixels(UpdateLinePixels), h, pixelLine);

                return;
            }

            lock (m_displayImage)
            {
                m_displayImage.LockPixels();

                for (int w = 0; w < pixelLine.Length; w++)
                {
                    m_displayImage.SetPixel(w, h, pixelLine[w]);
                }

                m_displayImage.UnlockPixels();
            }
                        
        }


        private delegate void invokeOutputText(string txt);

        private void OutputText(string txt)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new invokeOutputText(OutputText), txt);

                return;
            }

            output.AppendText(txt);
        }

        private delegate void invokeUpdateProgressbar(float percentage);

        private void UpdateProgressbar(float percentage)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new invokeUpdateProgressbar(UpdateProgressbar), percentage);

                return;
            }

            output.AppendText(string.Format("{0}\r\n", percentage));
            m_progressBar.Value = (int) (percentage * 100);
        }
        
        private void quitBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void renderBtn_Click(object sender, EventArgs e)
        {
            if (m_renderThread == null || !m_renderThread.IsAlive)
            {
                renderBtn.Enabled = false;

	            Render();
            }
            else
            {
                m_pauseRender = !m_pauseRender;

                renderBtn.Text = m_pauseRender ? "Render" : "Pause";
            }
        }

        private void saveImageBtn_Click(object sender, EventArgs e)
        {
            string filepath;
            int i = 0;
            do 
            {
                filepath = string.Format("image{0:000}.png", i++);
            } while (System.IO.File.Exists(filepath));

            if (picBox.Image != null)
            {
                picBox.Image.Save(filepath);
            }

        }

        private void change_btn_Click(object sender, EventArgs e)
        {
            lock (picBox.Image)
            {
                Fart.Scene.SphereLight sl = (Fart.Scene.SphereLight) m_tracer.World.Lights[0];
                sl.SetRadiance(Color3.Make(4, 7, 4));
            }

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (m_displayImage != null)
            {
	            lock (m_displayImage)
	            {
	                picBox.Refresh();
	            }
            }
        }
    }
}
