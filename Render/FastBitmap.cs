﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Fart.Render
{
    public sealed unsafe class FastBitmap: IDisposable
    {
        private struct PixelData
        {
            public byte B;
            public byte G;
            public byte R;
        }

        private Bitmap _bitmap;
        private int _width;
        private BitmapData _bitmapData = null;
        private byte* _pBase = null;
        private PixelData* _pInitPixel = null;
        private Point _size;
        private bool _locked = false;

        public FastBitmap(int width, int height)
        {
            _bitmap = new Bitmap(width, height);
            _size = new Point(width, height);
        }

        private PixelData* this[int x, int y]
        {
            get { return (PixelData*) (_pBase + y * _width + x * sizeof(PixelData)); }
        }

        public Color GetPixel(int x, int y)
        {
            PixelData* data = this[x, y];
            return Color.FromArgb(data->R, data->G, data->B);
        }

        public void SetPixel(int x, int y, Color c)
        {
            PixelData* data = this[x, y];
            data->R = c.R;
            data->G = c.G;
            data->B = c.B;
        }

        public static implicit operator System.Drawing.Image(FastBitmap bmp)
        {
            return bmp._bitmap;
        }

        public static implicit operator Bitmap(FastBitmap bmp)
        {
            return bmp._bitmap;
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void LockPixels()
        {
            if (_locked) throw new InvalidOperationException("Already locked");

            Rectangle bounds = new Rectangle(0, 0, _bitmap.Width, _bitmap.Height);

            // Figure out the number of bytes in a row. This is rounded up to be a multiple 
            // of 4 bytes, since a scan line in an image must always be a multiple of 4 bytes
            // in length. 
            _width = bounds.Width * sizeof(PixelData);
            if (_width % 4 != 0) _width = 4 * (_width / 4 + 1);

            _bitmapData = _bitmap.LockBits(bounds, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            _pBase = (byte*) _bitmapData.Scan0.ToPointer();
            _locked = true;
        }

        private void InitCurrentPixel()
        {
            _pInitPixel = (PixelData*) _pBase;
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void UnlockPixels()
        {
            if (!_locked) throw new InvalidOperationException("Not currently locked");

            _bitmap.UnlockBits(_bitmapData);
            _bitmapData = null;
            _pBase = null;
            _locked = false;
        }

        public void Dispose()
        {
            if (_locked) UnlockPixels();
        }
    }

}
