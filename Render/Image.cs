﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading;

using Fart.Maths;
using Fart.Scene;

namespace Fart.Render
{
    class Image: ICloneable
    {
        MultiColor[,] m_pixels;
        float[,] m_weights;

        int m_iterationCount = 1;

        // guess of average screen maximum brightness
        static readonly float DISPLAY_LUMINANCE_MAX = 10.0f;

        // ITU-R BT.709 standard RGB luminance weighting
        static readonly Color3 RGB_LUMINANCE = Color3.Make(0.2126f, 0.7152f, 0.0722f);

        // ITU-R BT.709 standard gamma
        static readonly float GAMMA_ENCODE = 0.45f;

        float m_screenLuminance = DISPLAY_LUMINANCE_MAX;

        public int Width
        {
            get
            {
                if (m_pixels != null)
                {
                    return m_pixels.GetLength(0);
                }
                else
                {
                    return 0;
                }
            }
        }

        public int Height
        {
            get
            {
                if (m_pixels != null)
                {
                    return m_pixels.GetLength(1);
                }
                else
                {
                    return 0;
                }
            }
        }

        public void ResetSize(int width, int height)
        {
            m_pixels = new MultiColor[width, height];
            for (int i = 0; i < m_pixels.GetLength(0); i++)
            {
                for (int j = 0; j < m_pixels.GetLength(1); j++)
                {
                    m_pixels[i, j] = new MultiColor();
                }
            }

            m_weights = new float[width, height];

            m_iterationCount = 1;
        }

        public void IncrementIteration()
        {
            m_iterationCount += 1;
        }

        public void AddToPixel(int x, int y, float weight, MultiColor color)
        {
            if (color != null)
            {
	            color.Scale(weight);
	            m_pixels[x, y].Add(color);
	
            }

            m_weights[x, y] = m_weights[x, y] + weight;
        }

        public Color[] GetScanline(int y)
        {
            int width = m_pixels.GetLength(0);
            Color[] colors = new Color[width];

            CalculateTonemapping();

            for (int x = 0; x < width; x++)
            {
                float weightedScale = 1;// / m_weights[x, y];
                Color3 mapped = m_pixels[x, y].FinalColor * (weightedScale * m_screenLuminance);

                mapped = GarmaCorrect(mapped);

                int r = Math.Min((int) (mapped.r * 255.0f + 0.5f), 255);
                int g = Math.Min((int) (mapped.g * 255.0f + 0.5f), 255);
                int b = Math.Min((int) (mapped.b * 255.0f + 0.5f), 255);

                colors[x] = Color.FromArgb(r, g, b);
            }

            return colors;
        }

        public void SaveToBitmap(FastBitmap bitmap)
        {
            CalculateTonemapping();

            lock (bitmap)
            {
                try
                {
	                bitmap.LockPixels();
	
	                Parallel.For(0, m_pixels.GetLength(1), y =>
	                {
	                    for (int x = 0; x < m_pixels.GetLength(0); x++)
	                    {
                            float weightedScale = 1;// / m_weights[x, y];
                            Color3 mapped = m_pixels[x, y].FinalColor * (weightedScale * m_screenLuminance);
	
	                        mapped = GarmaCorrect(mapped);
	
	                        int r = Math.Min((int) (mapped.r * 255.0f + 0.5f), 255);
	                        int g = Math.Min((int) (mapped.g * 255.0f + 0.5f), 255);
	                        int b = Math.Min((int) (mapped.b * 255.0f + 0.5f), 255);
	
	                        bitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
	                    }
	                });
                }
                finally
                {
                    bitmap.UnlockPixels();
                }
            }
        }

        private Color3 GarmaCorrect(Color3 color)
        {
            // Perform gamma compression
            return Color3.Make((float) Math.Pow(color.r > 0? color.r: 0, GAMMA_ENCODE),
                               (float) Math.Pow(color.g > 0? color.g: 0, GAMMA_ENCODE),
                               (float) Math.Pow(color.b > 0? color.b: 0, GAMMA_ENCODE));
        }

        private void CalculateTonemapping()
        {
            // calculate log mean luminance
            float logMeanLuminance = 1e-4f;

            float sumOfLogs = 0;
            Parallel.For(0, m_pixels.GetLength(1), () => 0.0f, (h, loop) =>
            {
                for (int w = 0; w < m_pixels.GetLength(0); w++)
                {
                    float weightedScale = 1;// / m_weights[w, h];
                    float Y = Color3.Dot(m_pixels[w, h].FinalColor, RGB_LUMINANCE) * weightedScale;

                    loop.ThreadLocalState += (float) Math.Log10(Y > 1e-4f ? Y : 1e-4f);
                }
            },
            partial_sum =>
            {
                float originalSum = 0, newSum = 0;
                do 
                {
                    originalSum = sumOfLogs;
                    newSum = originalSum + partial_sum;
                } while (Interlocked.CompareExchange(ref sumOfLogs, newSum, originalSum) != originalSum);
            });

            logMeanLuminance = (float) Math.Pow(10, sumOfLogs / m_pixels.Length);

            // (what do these mean again? (must check the tech paper...))
            float a = 1.219f + (float) Math.Pow( DISPLAY_LUMINANCE_MAX * 0.25, 0.4 );
            float b = 1.219f + (float) Math.Pow( logMeanLuminance, 0.4 );

            m_screenLuminance = (float) Math.Pow( a / b, 2.5 ) / DISPLAY_LUMINANCE_MAX;
        }

        public object Clone()
        {
            Image cloned = new Image();
            cloned.m_iterationCount = m_iterationCount;
            cloned.m_pixels = (MultiColor[,]) m_pixels.Clone();

            return cloned;
        }
    }
}
