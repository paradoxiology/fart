﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Collections;

using Fart.Maths;

namespace Fart.Render
{
    sealed class Octree<NodeData>
    {
        enum NodeSite
        {
            TOP_LEFT_FRONT,
            TOP_LEFT_BACK,
            TOP_RIGHT_BACK,
            TOP_RIGHT_FRONT,
            BOTTOM_LEFT_FRONT,
            BOTTOM_LEFT_BACK,
            BOTTOM_RIGHT_BACK,
            BOTTOM_RIGHT_FRONT
        }

        sealed class OctNode 
        {
            public float Extent
            {
                get;
                private set;
            }

            public Vector3 Center
            {
                get;
                private set;
            }

            public OctNode[] Children = new OctNode[8];

            public ConcurrentQueue<NodeData> Data = new ConcurrentQueue<NodeData>(); // A better choice may be ConcurrentBag

            public OctNode()
            {
                for (int i = 0; i < Children.Length; i++)
                {
                    Children[i] = null;
                }
            }

            public OctNode(BBox bound): this()
            {
                SetBound(bound);
            }

            public void SetBound(BBox bound)
            {
                Center = bound.Center;

                Vector3 extents = bound.Extents;

                Extent = Math.Max(extents.x, Math.Max(extents.y, extents.z));
            }

            public BBox GetBound()
            {
                Vector3 extents = Vector3.Make(Extent, Extent, Extent);

                Vector3 lower = Center - extents;
                Vector3 upper = Center + extents;

                return new BBox(lower, upper);
            }

            public IEnumerable<NodeData> IterateData()
            {
                foreach (var d in Data)
                {
                    yield return d;
                }

                for (int i = 0; i < 8; i++)
                {
                	if (Children[i] != null)
                	{
                        foreach (var d in Children[i].IterateData())
                        {
                            yield return d;
                        }
                	}
                }
            }

            public static Vector3 GetChildCenter(BBox parentBound, NodeSite childNodeSite)
            {
                Vector3 center = parentBound.Center;
                Vector3 halfExtents = parentBound.Extents * 0.5f;

                switch (childNodeSite)
                {
                case NodeSite.TOP_LEFT_FRONT:
                	return center + Vector3.Make(-halfExtents.x, halfExtents.y, halfExtents.z);

                case NodeSite.TOP_LEFT_BACK:
                    return center + Vector3.Make(-halfExtents.x, halfExtents.y, -halfExtents.z);

                case NodeSite.TOP_RIGHT_BACK:
                    return center + Vector3.Make(halfExtents.x, halfExtents.y, -halfExtents.z);

                case NodeSite.TOP_RIGHT_FRONT:
                    return center + Vector3.Make(halfExtents.x, halfExtents.y, halfExtents.z);

                case NodeSite.BOTTOM_LEFT_FRONT:
                    return center + Vector3.Make(-halfExtents.x, -halfExtents.y, halfExtents.z);

                case NodeSite.BOTTOM_LEFT_BACK:
                    return center + Vector3.Make(-halfExtents.x, -halfExtents.y, -halfExtents.z);

                case NodeSite.BOTTOM_RIGHT_BACK:
                    return center + Vector3.Make(halfExtents.x, -halfExtents.y, -halfExtents.z);

                case NodeSite.BOTTOM_RIGHT_FRONT:
                    return center + Vector3.Make(halfExtents.x, -halfExtents.y, halfExtents.z);
                }

                return Vector3.Make(0, 0, 0);
            }

            public static NodeSite SelectNodeSite(Vector3 parentCenter, Vector3 position)
            {
                Vector3 delta = position - parentCenter;

                int xSign = Math.Sign(delta.x), ySign = Math.Sign(delta.y), zSign = Math.Sign(delta.z);

                if (xSign > 0)
                {
                    if (ySign > 0)
                    {
                        if (zSign > 0)
                        {
                            return NodeSite.TOP_RIGHT_FRONT;
                        } 
                        else
                        {
                            return NodeSite.TOP_RIGHT_BACK;
                        }
                    } 
                    else
                    {
                        if (zSign > 0)
                        {
                            return NodeSite.BOTTOM_RIGHT_FRONT;
                        }
                        else
                        {
                            return NodeSite.BOTTOM_RIGHT_BACK;
                        }
                    }
                }
                else
                {
                    if (ySign > 0)
                    {
                        if (zSign > 0)
                        {
                            return NodeSite.TOP_LEFT_FRONT;
                        }
                        else
                        {
                            return NodeSite.TOP_LEFT_BACK;
                        }
                    }
                    else
                    {
                        if (zSign > 0)
                        {
                            return NodeSite.BOTTOM_LEFT_FRONT;
                        }
                        else
                        {
                            return NodeSite.BOTTOM_LEFT_BACK;
                        }
                    }
                }
            }
        }

        int m_maxDepth;
        OctNode m_root = new OctNode();

        public Octree(BBox worldBound, int maxDepth)
        {
            m_root.SetBound(worldBound);

            m_maxDepth = maxDepth;
        }

        public void AddData(NodeData data, Vector3 position, float range)
        {
            InternalAddData(m_root, data, position, range, 0, true);
        }

        public void Lookup(Vector3 position, Action<ConcurrentQueue<NodeData>> action)
        {
            if (m_root.GetBound().Contains(position))
            {
                InternalLookup(m_root, position, action);
            }
        }

        public void Lookup(Vector3 position, float range, Action<ConcurrentQueue<NodeData>> action)
        {
            if (m_root.GetBound().OverlapSphere(position, range))
            {
                InternalLookup(m_root, position, range, action);
            }
        }

        public IEnumerable<NodeData> IterateData()
        {
            return m_root.IterateData();
        }

        private void InternalAddData(OctNode node, NodeData data, Vector3 position, float range, int depth, bool syncNewNode)
        {
            if (depth == m_maxDepth || node.Extent < range)
            {
                node.Data.Enqueue(data);

                return;
            }

            BBox nodeBound = node.GetBound();
            float childExtent = node.Extent * 0.5f;
            for (int i = 0; i < 8; i++)
            {
                NodeSite site = (NodeSite) i;

                Vector3 childCenter = OctNode.GetChildCenter(nodeBound, site);
                BBox childBound = new BBox(childCenter, childExtent);

                if (childBound.OverlapSphere(position, range))
                {
                    OctNode childNode = node.Children[i];

                    if (childNode == null)
                    {
                        // Locally create a new node for the child.
                        childNode = new OctNode(childBound);
                        // No need to synchronize new node for the local node.
                        InternalAddData(childNode, data, position, range, depth + 1, false);

                        if (!syncNewNode)
                        {
                            // If new node synchronization is not required, assign the new node
                            // to the child immediately.
                            node.Children[i] = childNode;
                        } 
                        else
                        {
                            // Atomically assign the new node to the child
	                        if (Interlocked.CompareExchange(ref node.Children[i], childNode, null) != null)
                            {
                                // If a new node has already been given to the child in another thread, discard
                                // the local new node and re-insert the data to the child
                                InternalAddData(node.Children[i], data, position, range, depth + 1, true);
                            }
                        }
                    }
                    else
                    {
                        InternalAddData(childNode, data, position, range, depth + 1, syncNewNode);
                    }
                }
            }
        }

        private void InternalLookup(OctNode node, Vector3 position, Action<ConcurrentQueue<NodeData>> action)
        {
            if (!node.Data.IsEmpty)
            {
	            action(node.Data);
            }

//             for (int i = 0; i < 8; i++)
//             {
//             	if (node.Children[i] != null)
//             	{
//                     var childBound = node.Children[i].GetBound();
//                     if (childBound.Contains(position))
//                     {
//                         InternalLookup(node.Children[i], position, action);
//                     }
//             	}
//             }
            NodeSite site = OctNode.SelectNodeSite(node.Center, position);
            if (node.Children[(int) site] != null)
            {
                InternalLookup(node.Children[(int) site], position, action);
            }
        }

        private void InternalLookup(OctNode node, Vector3 position, float range, Action<ConcurrentQueue<NodeData>> action)
        {
            if (!node.Data.IsEmpty)
            {
                action(node.Data);
            }

            for (int i = 0; i < 8; i++)
            {
            	if (node.Children[i] != null)
            	{
                    var childBound = node.Children[i].GetBound();
                    if (childBound.OverlapSphere(position, range))
                    {
                        InternalLookup(node.Children[i], position, range, action);
                    }
            	}
            }
        }
    }
}
