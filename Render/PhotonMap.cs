﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

using Fart.Geometry;
using Fart.Maths;
using Fart.Scene;
using Fart.Shading;

namespace Fart.Render
{
    public sealed class PhotonMap
    {
        sealed class Photon
        {
            public Vector3 Direction;   // Points away from the hit point

            public LightColor Alpha;

            public Photon()
            {
                Direction = new Vector3(0, 0, 0);

                Alpha = new LightColor();
            }

            public Photon(Vector3 direction, LightColor alpha)
            {
                Direction = direction;

                Alpha = alpha;
            }
        }

        sealed class RadiancePhoton
        {
            public Vector3 Normal;

            public MultiColor RadianceOut = null;

            public RadiancePhoton()
            {
                Normal = new Vector3(0, 0, 0);
            }

            public RadiancePhoton(Vector3 normal)
            {
                Normal = normal;
            }
        }

        UniformSampler m_sampler = new UniformSampler();

        ImportonMap m_importonMap;

        KDTree<Photon> m_globalTree;
        KDTree<Photon> m_causticTree;

        float m_maxSearchRadius;

        int m_globalPhotonPaths = 0;
        int m_causticPhotonPaths = 0;

        int m_maxGlobalPhotons;
        int m_maxCausticPhotons;

        KDTree<RadiancePhoton> m_radiancePhotonTree;
        List<Color3> m_radianceSurfaceRelectance;

        readonly float COS_IMPORTANCE_GATHER_ANGLE = Utils.Cosf(Utils.PI / 10);

        [ThreadStatic]
        static List<Vector3> s_photonDirs;

        [ThreadStatic]
        static NearestPhotonSet<Photon> s_nearPhotonSet;

        public PhotonMap(World world, Camera camera, int maxGlobalPhotonCount, int maxCausticPhotonCount, bool precomputeRadiance)
        {
            Vector3 worldSize = world.Bound.Size;
            m_maxSearchRadius = 0.095f * Math.Min(worldSize.x, Math.Min(worldSize.y, worldSize.z));

            m_globalTree = new KDTree<Photon>(m_maxSearchRadius);
            m_causticTree = new KDTree<Photon>(m_maxSearchRadius);

            m_maxGlobalPhotons = maxGlobalPhotonCount;
            m_maxCausticPhotons = maxCausticPhotonCount;

            if (precomputeRadiance)
            {
                m_radiancePhotonTree = new KDTree<RadiancePhoton>(m_maxSearchRadius);

                m_radianceSurfaceRelectance = new List<Color3>(maxGlobalPhotonCount / 2);
            }

            m_importonMap = new ImportonMap(world, camera, 850502, m_maxSearchRadius * 3.5f);

            ScatterPhotons(world, maxGlobalPhotonCount, maxCausticPhotonCount);

            Parallel.Invoke(
                () => m_globalTree.BuildTree(),
                () => m_causticTree.BuildTree()
            );

            if (precomputeRadiance)
            {
                PrecomputeRadiancePhotons(m_maxSearchRadius);
            }
        }

        public MultiColor VisualizePhotons(Vector3 wo, ShadingState shadingState, float maxDiatance)
        {
            var globalRadiance = EstimateRadiance(m_globalTree, m_globalPhotonPaths, wo, shadingState, m_maxSearchRadius);
            var causticRadiance = EstimateRadiance(m_causticTree, m_causticPhotonPaths, shadingState.Position, shadingState.Normal, maxDiatance);

            if (globalRadiance != null)
            {
                globalRadiance.Add(causticRadiance);

                return globalRadiance;
            }
            else
            {
                return causticRadiance;
            }                        
        }

        public MultiColor ImportanceFinalGather(PathState pathState, SampleState sampleState, int finaGatherCount)
        {
            // TODO: Handle specular materials
            ShadingState shadingState = pathState.ShadingState;
            if (shadingState.Material != null)
            {
                MultiColor gatheredRadiance = new MultiColor();

                var photonDirs = GetPhotonDirections(m_globalTree, shadingState.Position, shadingState.Normal, m_maxSearchRadius * 2);
	
	            for (int i = 0; i < finaGatherCount; i++)
	            {
                    SampleState splittedState = sampleState.SplitTrajectory(i, finaGatherCount);

                    MaterialSampleInfo matInfo;
                    var gatherPath = pathState.GetMaterialPath(splittedState.PopSample2D(), BSDFType.Diffuse, out matInfo);

                    // back step a little bit to avoid surface acne
                    gatherPath.Offset(pathState.Wo * Ray.MIN_T);

                    var radiance = TraceFinalGatherPath(gatherPath, splittedState, 0);
                    if (radiance != null)
                    {
                        float photonPDF = GetPhotonPDF(photonDirs, matInfo.wi);
                        float weight = Utils.PowerHeuristic(finaGatherCount, matInfo.bsdfPdf, finaGatherCount, photonPDF);

                        // Use Multiple Importance Sampling to weight the PDF
                        radiance.Scale(matInfo.fr * Vector3.AbsDot(matInfo.wi, shadingState.Normal) * weight / matInfo.bsdfPdf);
                        gatheredRadiance.Add(radiance);
                    }

                    if (photonDirs.Count > 0)
                    {
	                    Sample2 dirSample = splittedState.PopSample2D();
	                    int chosenPhoton = Math.Min(photonDirs.Count - 1, (int) Math.Floor(splittedState.PopSample1D().s * photonDirs.Count));
	                    Vector3 wx, wy;
	                    Utils.CoordSystem(photonDirs[chosenPhoton], out wx, out wy);
	                    Vector3 wi = Utils.UniformSampleCone(dirSample, COS_IMPORTANCE_GATHER_ANGLE, wx, wy, photonDirs[chosenPhoton]);
	                    float bsdfPDF = shadingState.Material.PDF(shadingState, pathState.Wo, wi);
	                    if (bsdfPDF != 0)
	                    {
                            var photonPath = pathState.GetMaterialPath(BSDFType.Diffuse, wi, out matInfo);
	                        var photonRadiance = TraceFinalGatherPath(photonPath, splittedState, 0);
	                        if (photonRadiance != null)
	                        {
		                        float photonPDF = GetPhotonPDF(photonDirs, wi);
		                        float weight = Utils.PowerHeuristic(finaGatherCount, photonPDF, finaGatherCount, bsdfPDF);
		
		                        if (matInfo.fr == Color3.Black)
                                {
                                    continue;
                                }

                                // Use Multiple Importance Sampling to weight the PDF
                                photonRadiance.Scale(matInfo.fr * Vector3.AbsDot(wi, shadingState.Normal) * weight / photonPDF);
		                        gatheredRadiance.Add(photonRadiance);
	                        }
	                    }
                    }
                }

                gatheredRadiance.Scale(1.0f / finaGatherCount);
                return gatheredRadiance;
            }

            return null;
        }
        
        public MultiColor FinalGather(PathState pathState, SampleState sampleState, int currentDepth)
        {
            if (pathState.ShadingState.Material != null)
            {
                MaterialSampleInfo matInfo;
                PathState gatherPath;
                if (currentDepth == 0)
                {
                     gatherPath = pathState.GetMaterialPath(sampleState.PopSample2D(), BSDFType.Diffuse, out matInfo);
                } 
                else
                {
                    gatherPath = pathState.GetMaterialPath(sampleState.PopSample2D(), sampleState.PopSample1D(), out matInfo);
                }

                // back step a little bit to avoid surface acne
                gatherPath.Offset(pathState.Wo * Ray.MIN_T * 2);

                var radiance = TraceFinalGatherPath(gatherPath, sampleState.NextState(), currentDepth);
                if (radiance != null)
                {
                    radiance.Scale(matInfo.fr * Vector3.AbsDot(matInfo.wi, pathState.ShadingState.Normal) / matInfo.bsdfPdf);

                    return radiance;
                }
            }

            return null;
        }

        public MultiColor TraceFinalGatherPath(PathState gatherPath, SampleState sampleState, int currentDepth)
        {
            ShadingState gatherState = gatherPath.ShadingState;

            MultiColor radiance = null;

            if (gatherState != null && gatherState.Material != null)
            {
                bool shortFGRay = gatherState.InterState.Distance < m_maxSearchRadius;
                bool specular = (gatherState.Material.GetBSDFType() & BSDFType.Specular) != 0;

                if (currentDepth >= 4 || (!shortFGRay && !specular))
                {
                    if (m_radiancePhotonTree != null)
                    {
                        var radiancePhoton = LocateNearestRadiancePhoton(gatherState.Position, gatherState.Normal, m_maxSearchRadius);

                        if (radiancePhoton.RadianceOut != null)
                        {
                            radiance = (MultiColor) radiancePhoton.RadianceOut.Clone();
                        }
                    }
                    else
                    {
                        radiance = EstimateRadiance(m_globalTree, m_globalPhotonPaths, gatherPath.Wo, gatherState, m_maxSearchRadius);
                        radiance.Add(EstimateRadiance(m_causticTree, m_causticPhotonPaths, gatherPath.Wo, gatherState, m_maxSearchRadius));
                    }

                    return radiance;
                }
                else
                {
                    if (specular)
                    {
                        return FinalGather(gatherPath, sampleState, currentDepth + 1);
                    }
                    else if (shortFGRay)
                    {
	                    // Send some secondary final gather rays if the first hit is too close to the shading point
	                    // (to avoid light leaking).
                        int sampleCount = Math.Max(1, 4 / (currentDepth + 1));

                        MultiColor refinedRadiance = new MultiColor();
                        for (int i = 0; i < sampleCount; i++)
                        {
                            var splittedSample = sampleState.SplitTrajectory(i, sampleCount);

                            MaterialSampleInfo matInfo;
                            var refinedPath = gatherPath.GetMaterialPath(splittedSample.PopSample2D(), splittedSample.PopSample1D(), out matInfo);
                            refinedPath.Offset(gatherPath.Wo * Ray.MIN_T * 2);  // back step a little bit to avoid surface acne

                            var gatheredRadiance = TraceFinalGatherPath(refinedPath, splittedSample.NextState(), currentDepth + 1);
                            if (gatheredRadiance != null)
                            {
                                gatheredRadiance.Scale(matInfo.fr * Vector3.AbsDot(matInfo.wi, gatherPath.ShadingState.Normal) / matInfo.bsdfPdf);

                                refinedRadiance.Add(gatheredRadiance);
                            }
                        }
                        refinedRadiance.Scale(1.0f / sampleCount);

                        return refinedRadiance;
                    }
                }
            }

            return null;
        }

        public MultiColor GetCausticRadiance(Vector3 wo, ShadingState shadingState)
        {
            if (shadingState.Material != null)
            {
                Material material = shadingState.Material;

                MultiColor radiance = new MultiColor();

                CheckNearestPhotonSet(350);
                m_causticTree.Lookup(new KDTree<Photon>.Lookuper(shadingState.Position, m_maxSearchRadius), s_nearPhotonSet.AddPhoton);

                var nearPhotons = s_nearPhotonSet.GetPhotons();
                if (nearPhotons.Count > 0)
                {
	                foreach (var nearPhoton in nearPhotons)
	                {
                        var photon = nearPhoton.photon;
                        if (Vector3.Dot(photon.Direction, shadingState.Normal) > 0)
                        {
                            float weight = Utils.SimpsonKernel(nearPhoton.sqauredDistance, s_nearPhotonSet.MaxSquaredCoverRange);
                            radiance.Add(photon.Alpha * material.f(shadingState, photon.Direction, wo) * weight);
                        }
                    }

                    radiance.Scale(8.0f / m_causticPhotonPaths);
	
	                return radiance;
                }
            }

            return null;
        }

        List<Vector3> GetPhotonDirections(KDTree<Photon> photonTree, Vector3 position, Vector3 normal, float maxDistance)
        {
            if (s_photonDirs == null)
            {
                s_photonDirs = new List<Vector3>();
            }

            do 
            {
                s_photonDirs.Clear();

	            photonTree.Lookup(new KDTree<Photon>.Lookuper(position, maxDistance), (photon, photonPos, _, __) =>
	            {
	                if (Vector3.Dot(photon.Direction, normal) > 0)
	                {
		                s_photonDirs.Add(photon.Direction);
	                }
	            });

                maxDistance *= 2;
            } while (s_photonDirs.Count < 50);

            return s_photonDirs;
        }

        float GetPhotonPDF(List<Vector3> photonDirs, Vector3 wi)
        {
            if (photonDirs.Count > 0)
            {
	            float photonPDF = 0;
	            float conePDF = Utils.UniformConePDF(COS_IMPORTANCE_GATHER_ANGLE);
	
	            foreach (var photonDir in photonDirs)
	            {
	                if (Vector3.Dot(photonDir, wi) > COS_IMPORTANCE_GATHER_ANGLE * 0.999f)
	                {
                        // Increase the PDF if the photon direction is within the given angle
	                    photonPDF += conePDF;
	                }
	            }
	
	            return photonPDF / photonDirs.Count;
            }

            return 0;
        }

        class NearestPhotonSet<PhotonType>
        {
            public struct NearPhoton: IComparable<NearPhoton>
            {
                public float sqauredDistance;

                public PhotonType photon;

                public int CompareTo(NearPhoton other)
                {
                    return sqauredDistance.CompareTo(other.sqauredDistance);
                }
            }

            int m_maxLookup;
            int m_found;
            List<NearPhoton> m_photons;

            public float MaxSquaredCoverRange
            {
                get;
                private set;
            }

            public NearestPhotonSet(int maxPhotonLookup)
            {
                m_photons = new List<NearPhoton>(maxPhotonLookup);

                m_maxLookup = maxPhotonLookup;
                m_found = 0;
            }

            public void Reset(int maxPhotonLookup)
            {
                m_photons.Clear();

                MaxSquaredCoverRange = 0;
                m_maxLookup = maxPhotonLookup;
                m_found = 0;
            }

            public void AddPhoton(PhotonType photon, Vector3 position, float squaredDist, KDTree<PhotonType>.Lookuper lookuper)
            {
                if (m_found++ < m_maxLookup)
                {
                    if (MaxSquaredCoverRange < squaredDist)
                    {
                        MaxSquaredCoverRange = squaredDist;
                    }

                    // Neighbor list is still not full, add the newly found photon to the array.
                    m_photons.Add(new NearPhoton { sqauredDistance = squaredDist, photon = photon });

                    if (m_photons.Count == m_maxLookup)
                    {
                        m_photons.Sort();

                        var farPhoton = m_photons[m_photons.Count - 1];

                        // Tighten up the search radius to the farthest range so far
                        lookuper.MaxSquaredRadius = farPhoton.sqauredDistance;

                        MaxSquaredCoverRange = farPhoton.sqauredDistance;
                    }
                }
                else
                {
                    // Neighbor list is full, we want to find the k nearest ones.

                    // Discard the farthest one.
                    m_photons.RemoveAt(m_photons.Count - 1);

                    // Insert a new photon and makes sure the list is still ordered.
                    var nearPhoton = new NearPhoton { sqauredDistance = squaredDist, photon = photon };
                    int insertIndex = m_photons.BinarySearch(nearPhoton);
                    if (insertIndex < 0)
                    {
                        // bitwise invert the index to find the next insertion index if it is negative
                        insertIndex = ~insertIndex;
                    }
                    m_photons.Insert(insertIndex, nearPhoton); // The insertion preserve the order of the list.

                    var farPhoton = m_photons[m_photons.Count - 1];

                    // Tighten up the search radius to the farthest range so far
                    lookuper.MaxSquaredRadius = farPhoton.sqauredDistance;

                    MaxSquaredCoverRange = farPhoton.sqauredDistance;
                }
            }

            public List<NearPhoton> GetPhotons()
            {
                return m_photons;
            }
        }

        // Make sure the thread local photon set is initialized properly.
        static void CheckNearestPhotonSet(int nearestCount)
        {
            if (s_nearPhotonSet != null)
            {
                s_nearPhotonSet.Reset(nearestCount);
            }
            else
            {
                s_nearPhotonSet = new NearestPhotonSet<Photon>(nearestCount);
            }
        }

        MultiColor EstimateRadiance(KDTree<Photon> photonTree, int photonPaths, Vector3 wo, ShadingState shadingState, float maxDiatance)
        {
            if (shadingState.Material != null)
            {
                Material material = shadingState.Material;

	            MultiColor radiance = new MultiColor();

                CheckNearestPhotonSet(150);
                photonTree.Lookup(new KDTree<Photon>.Lookuper(shadingState.Position, maxDiatance), s_nearPhotonSet.AddPhoton);

                // TODO: Get rid of the glossy check later!!!
                if ((material.GetBSDFType() & BSDFType.Diffuse) != 0 && (material.GetBSDFType() & BSDFType.Glossy) == 0)
                {
                    foreach (var nearPhoton in s_nearPhotonSet.GetPhotons())
	                {
	                    var photon = nearPhoton.photon;
	                    if (Vector3.Dot(photon.Direction, shadingState.Normal) > 0)
	                    {
                            float weight = Utils.SimpsonKernel(nearPhoton.sqauredDistance, s_nearPhotonSet.MaxSquaredCoverRange);
	                        radiance.Add(photon.Alpha  * weight);
	                    }
	                }
	
	                Color3 scale = material.GetReflectance() * (1.0f / photonPaths) * Utils.INV_PI;
	                radiance.Scale(scale);
                } 
                else
                {
                    foreach (var nearPhoton in s_nearPhotonSet.GetPhotons())
                    {
                        var photon = nearPhoton.photon;
                        if (Vector3.Dot(photon.Direction, shadingState.Normal) > 0)
                        {
                            float weight = Utils.SimpsonKernel(nearPhoton.sqauredDistance, s_nearPhotonSet.MaxSquaredCoverRange);
                            radiance.Add(photon.Alpha * material.f(BSDFType.Diffuse, shadingState, photon.Direction, wo) * weight);
                        }
                    }

                    radiance.Scale(1.0f / photonPaths);
                }
	
	            return radiance;
            }

            return null;
        }

        MultiColor EstimateRadiance(KDTree<Photon> photonTree, int photonPaths, Vector3 position, Vector3 normal, float maxDiatance)
        {
            MultiColor radiance = new MultiColor();

            float maxSquaredDist = maxDiatance * maxDiatance;

            photonTree.Lookup(new KDTree<Photon>.Lookuper(position, maxDiatance), (photon, photonPos, squaredDist, _) =>
            {
                if (Vector3.Dot(photon.Direction, normal) > 0)
                {
                    radiance.Add(photon.Alpha);
                }
            });

            radiance.Scale(1.0f / (maxSquaredDist * photonPaths * Utils.PI));

            return radiance;
        }

        public void PrecomputeRadiancePhotons(float maxDiatance)
        {
            Parallel.For(0, m_radiancePhotonTree.Count, i =>
            {
                RadiancePhoton radiancePhoton;
                Vector3 pos;

                m_radiancePhotonTree.GetData(i, out radiancePhoton, out pos);

                // Approximate the out going reflectance by ignoring directional variation(use hemisphere to hemisphere
                // reflectance), as high accuracy for final gather(second order) radiance is usually not needed.
                radiancePhoton.RadianceOut = EstimateIrradiance(m_globalTree, m_globalPhotonPaths, pos, radiancePhoton.Normal, maxDiatance);
                radiancePhoton.RadianceOut.Scale(m_radianceSurfaceRelectance[i] * Utils.INV_PI);

                m_radiancePhotonTree.SetData(i, radiancePhoton);
            });

            m_radianceSurfaceRelectance.Clear();
            m_radianceSurfaceRelectance = null;

            m_globalTree = null;


            m_radiancePhotonTree.BuildTree();
        }

        MultiColor EstimateIrradiance(KDTree<Photon> photonTree, int photonPaths, Vector3 position, Vector3 normal, float maxDistance)
        {
            MultiColor E = new MultiColor();

            CheckNearestPhotonSet(150);
            photonTree.Lookup(new KDTree<Photon>.Lookuper(position, maxDistance), s_nearPhotonSet.AddPhoton);

            if (s_nearPhotonSet.MaxSquaredCoverRange > 0)
            {
	            foreach (var nearPhoton in s_nearPhotonSet.GetPhotons())
	            {
	                var photon = nearPhoton.photon;
	
	                if (Vector3.Dot(photon.Direction, normal) > 0)
	                {
	                    E.Add(photon.Alpha);
	                }
	            }
	
	            E.Scale(1.0f / (Utils.PI * photonPaths * s_nearPhotonSet.MaxSquaredCoverRange));
            }

            return E;
        }

        RadiancePhoton LocateNearestRadiancePhoton(Vector3 position, Vector3 normal, float guessRadius)
        {
            RadiancePhoton nearestRadiance = new RadiancePhoton();

//             do 
//             {
	            m_radiancePhotonTree.Lookup(new KDTree<RadiancePhoton>.Lookuper(position, guessRadius), (radiancePhoton, _, squaredDist, lookuper) =>
	            {
                    if (Vector3.Dot(radiancePhoton.Normal, normal) >= 0)
	                {
	                    nearestRadiance = radiancePhoton;

                        lookuper.MaxSquaredRadius = squaredDist;
	                }
	            });

//                 guessRadius *= 2;
//             } while (nearestDist == float.MaxValue);

            return nearestRadiance;
        }

        sealed class PhotonStat
        {
            public volatile int PhotonShotCount = 0;
            public volatile int GlobalPhotonCount = 0;
            public volatile int CausticPhotonCount = 0;
            public volatile int RadiancePhotonCount = 0;

            public volatile bool GlobalFinished;
            public volatile bool CausticFinished;

            public PhotonStat(int maxGlobalPhotonCount, int maxCausticPhotonCount)
            {
                GlobalFinished = maxGlobalPhotonCount == 0;
                CausticFinished = maxCausticPhotonCount == 0;
            }
        }

        void DepositLocalPhoton(Photon photon, PhotonStat photonStat, ShadingState shadingState, bool causticPath)
        {
            float importance = causticPath? 1: m_importonMap.GetVisualImportance(shadingState.Position);

            if (m_sampler.GetSample1D().s < importance)
            {
                photon.Alpha /= importance;

	            if (causticPath)
	            {
	                // Deposit the photon to the caustic photon map, if it only traveled through specular paths.
	                if (!photonStat.CausticFinished)
	                {
	                    if (photonStat.CausticPhotonCount < m_maxCausticPhotons)
	                    {
	                        lock (m_causticTree)
	                        {
		                        m_causticTree.AddData(photon, shadingState.Position);
	
	                            ++photonStat.CausticPhotonCount;
	                        }
	                    }
	                    else
	                    {
	                        photonStat.CausticFinished = true;
	
	                        // It should be noted that the assignment for m_causticPhotonPaths(and all other *PhotonPaths) is
	                        // not strictly thread correct, as it's possible that one thread assigns it with a value and
	                        // another thread overwrites it with another value(which may even be smaller than the previous value).
	                        // However the absolute correctness is not necessary, as the value will usually be very large,
	                        // offset by one or two number is more than acceptable.
	                        Interlocked.Exchange(ref m_causticPhotonPaths, photonStat.PhotonShotCount);
	                    }
	                }
	            }
	            else
	            {
	                if (!photonStat.GlobalFinished)
	                {
		                if (photonStat.GlobalPhotonCount < m_maxGlobalPhotons)
		                {
	                        lock (m_globalTree)
		                    {
		                    	m_globalTree.AddData(photon, shadingState.Position);
	
	                            ++photonStat.GlobalPhotonCount;
		                    }
		                }
		                else
		                {
		                    photonStat.GlobalFinished = true;
	
	                        // See comment above
	                        Interlocked.Exchange(ref m_globalPhotonPaths, photonStat.PhotonShotCount);
		                }
	                }
	            }
	
	            if (m_radiancePhotonTree != null && photonStat.RadiancePhotonCount < m_maxGlobalPhotons && m_sampler.GetSample1D().s < 0.5f)
	            {
	                lock (m_radiancePhotonTree)
	                {
	                    m_radiancePhotonTree.AddData(new RadiancePhoton(shadingState.Normal), shadingState.Position);
		                m_radianceSurfaceRelectance.Add(shadingState.Material.GetReflectance());
	
	                    ++photonStat.RadiancePhotonCount;
	                }
	            }
            }
        }

#pragma warning disable 0420 // Disable Warning for atomically incrementing volatile variables by passing references

        void ScatterPhotons(World world, int maxGlobalPhotonCount, int maxCausticPhotonCount)
        {
            PhotonStat stat = new PhotonStat(maxGlobalPhotonCount, maxCausticPhotonCount);

            bool withRadiance = m_radiancePhotonTree != null;

            Parallel.ForEach(EmitPhoton(world), (emittion, loop) =>
            {
                if (stat.GlobalFinished && stat.CausticFinished)
                {
                    loop.Stop();
                }
                else
                {
                    Interlocked.Increment(ref stat.PhotonShotCount);

                    PathState photonPath = emittion.PhotonPath;
                    LightColor alpha = emittion.Alpha;

                    bool causticPath = false;

                    int photonDepth = 1;

                    while (photonPath.Type != PathType.Dead)
                    {
                        ShadingState shadingState = photonPath.ShadingState;
                        if (shadingState.Material == null)
                        {
                            break;
                        }

                        Vector3 wo = photonPath.Wo;

                        if ((shadingState.Material.GetBSDFType() & BSDFType.Specular) == 0)
                        {
                            if (Vector3.Dot(photonPath.PathRay.Direction, shadingState.Normal) > 0)
                            {
                                break;
                            }

                            // Only store photons, if they hit a non-specular BRDF.
                            DepositLocalPhoton(new Photon(photonPath.Wo, alpha), stat, shadingState, causticPath);

                            causticPath = false; // TODO: Check if this should only be set to false when a non specular BSDF is hit.
                        }

                        Sample2 dirSample;
                        Sample1 bsdfSample;
                        if (photonDepth == 1)
                        {
                            dirSample = emittion.SampleState.PopSample2D();
                            bsdfSample = emittion.SampleState.PopSample1D();
                        }
                        else
                        {
                            dirSample = m_sampler.GetSample2D();
                            bsdfSample = m_sampler.GetSample1D();
                        }

                        MaterialSampleInfo matInfo;
                        photonPath = photonPath.GetMaterialPath(dirSample, bsdfSample, out matInfo);
                        if (matInfo.fr.Equals(Color3.Black) || matInfo.bsdfPdf == 0)
                        {
                            break;
                        }

                        causticPath = ((photonDepth == 1) || causticPath) &&
                            ((matInfo.sampledType & BSDFType.Specular) != 0 || (matInfo.sampledType & BSDFType.Glossy) != 0);

                        LightColor newAlpha = alpha * matInfo.fr * (Vector3.AbsDot(matInfo.wi, shadingState.Normal) / matInfo.bsdfPdf);

                        // Perform Russian roulette to terminate the photon path.
                        float continueProb = Math.Min(1.0f, newAlpha.Factor.Intensity / alpha.Factor.Intensity);
                        if (photonDepth++ >= 10 || m_sampler.GetSample1D().s > continueProb)
                        {
                            break;
                        }
                        else
                        {
                            // Increase its weight to account for its lost contributions of the 
                            // paths that were terminated

                            alpha = newAlpha / continueProb;
                        }
                    }
                }
            });
        }

#pragma warning restore 0420

        struct PhotonEmittion
        {
            public LightColor Alpha;

            public PathState PhotonPath;

            public SampleState SampleState;
        }

        IEnumerable<PhotonEmittion> EmitPhoton(World world)
        {
            int lightCount = world.Lights.Count;
            float[] lightPower = new float[lightCount];
            for (int i = 0; i < lightCount; i++)
            {
                lightPower[i] = world.Lights[i].GetPower().Intensity;
            }

            float totalPower;
            float[] lightCDF = Utils.CreateCDF(lightPower, out totalPower);

            int shotCount = 0;
            while (true)
            {
                PhotonEmittion emittion;

                emittion.SampleState = new SampleState((uint) ++shotCount, 0, 0, 0);

                float lightPickedPDF;
                int whichLight = Math.Min(lightCount - 1,
                    (int) (lightCount * Utils.SampleCDF(lightPower, lightCDF, totalPower, emittion.SampleState.PopSample1D(), out lightPickedPDF)));

                float lightPDF;
                Ray photonRay;
                emittion.Alpha = world.Lights[whichLight].ScatterLight(world, emittion.SampleState.PopSample2D(), m_sampler.GetSample2D(), out lightPDF, out photonRay);
                if (emittion.Alpha.Factor.Equals(Color3.Black) || lightPDF == 0.0f)
                {
                    continue;
                }
                emittion.Alpha /= lightPDF * lightPickedPDF;
                emittion.PhotonPath = new PathState(world, photonRay, PathType.Light);

                yield return emittion;
            }
        }
    }
}
