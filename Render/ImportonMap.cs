﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fart.Scene;
using Fart.Maths;
using Fart.Shading;

namespace Fart.Render
{
    sealed class ImportonMap
    {
        struct Importon
        {
            public float Importance;

            public Importon(float importance)
            {
                Importance = importance;
            }
        }

        KDTree<Importon> m_importonTree;

        float m_searchRadius;

        [ThreadStatic]
        static KDTree<Importon>.KNNSearch s_knnSet;

        public ImportonMap(World world, Camera camera, int importonCount, float searchRadius)
        {
            m_importonTree = new KDTree<Importon>(searchRadius);

            m_searchRadius = searchRadius;

            ScatterImportons(world, camera);

            m_importonTree.BuildTree();
        }

        public float GetVisualImportance(Vector3 position)
        {
            if (s_knnSet != null)
            {
                s_knnSet.Reset(50);
            } 
            else
            {
                s_knnSet = new KDTree<Importon>.KNNSearch(50);
            }

            m_importonTree.Lookup(new KDTree<Importon>.Lookuper(position, m_searchRadius), s_knnSet.AddDataAction);

            float importance = 0;
            foreach (var importon in s_knnSet.GetData())
            {
                importance += importon.data.Importance;
            }

            return Math.Min(1, importance * 0.025f);
        }

        public MultiColor VisualizeImportons(World world, ShadingState shadingState)
        {
            float importance = GetVisualImportance(shadingState.Position);

            return new MultiColor(new LightColor(world.Lights[0], Color3.Make(importance, importance, importance)));
        }

        void ScatterImportons(World world, Camera camera)
        {
            var emittor = EmitImporton(world, camera).GetEnumerator();

            while (emittor.MoveNext())
            {
                var emittion = emittor.Current;

                PathState importonPath = emittion.ImportonPath;
                SampleState sampleState = emittion.SampleState;

                float importance = 1;

                while (importonPath.Type != PathType.Dead)
                {
                    ShadingState shadingState = importonPath.ShadingState;
                    if (shadingState.Material == null)
                    {
                        break;
                    }

                    Vector3 wo = importonPath.Wo;
                    if ((shadingState.Material.GetBSDFType() & BSDFType.Specular) == 0)
                    {
                        m_importonTree.AddData(new Importon(importance), shadingState.Position);
                    }

                    MaterialSampleInfo matInfo;
                    importonPath = importonPath.GetMaterialPath(sampleState.PopSample2D(), sampleState.PopSample1D(), out matInfo);

                    if (matInfo.bsdfPdf > 0 )
                    {
                        break;
                    }

                    importance *= matInfo.fr.Intensity;
                    if (importance < Utils.EPISLON)
                    {
                        break;
                    }
                }
            }
        }

        struct ImportonEmittion
        {
            public PathState ImportonPath;

            public SampleState SampleState;
        }

        IEnumerable<ImportonEmittion> EmitImporton(World world, Camera camera)
        {
            int shotCount = 0;
            while (shotCount++ < 8552)
        	{
                SampleState sampleState = new SampleState((uint) shotCount, 0, 0, 0);

                var sample = sampleState.PopSample2D();
                float w = sample.s0 * camera.ImageWidth, h = sample.s1 * camera.ImageHeight;
                Ray eyeRay = camera.GetRay(w, h);

                yield return new ImportonEmittion
                {
                    ImportonPath = new PathState(world, eyeRay, PathType.Eye),
                    SampleState = sampleState
                };
        	}
        }
    }
}
