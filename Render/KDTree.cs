﻿using System;
using System.Collections.Generic;
using System.Threading;

using Fart.Maths;

namespace Fart.Render
{
    sealed class KDTree<KDData>
    {
        private class KDNode<KDNodeData>
        {
            public KDNodeData Data;// { get; set; }

            public Vector3 Position;// { get; set; }

            public KDNode<KDNodeData> Left;// { get; set; }
            public KDNode<KDNodeData> Right;// { get; set; }

            public byte SepAxis = 0;

            public KDNode(KDNodeData data, Vector3 position)
            {
                Data = data;
                Position = position;

                Left = Right = null;
            }
        }

        private int m_dataCount;
        private List<KDNode<KDData>> m_nodes = new List<KDNode<KDData>>(512);
        private KDNode<KDData> m_root;
        private float m_queryRangeHint;
        private BBox m_rootBound = new BBox();

        public KDTree(float queryRangeHint)
        {
            m_queryRangeHint = queryRangeHint;
        }

        public void AddData(KDData data, Vector3 position)
        {
            m_nodes.Add(new KDNode<KDData>(data, position));

            m_rootBound.UnionPoint(position);
        }

        public void GetData(int index, out KDData data, out Vector3 position)
        {
            if (m_nodes == null)
            {
                throw new InvalidOperationException("Tree has been built");
            }

            KDNode<KDData> node = m_nodes[index];

            data = node.Data;
            position = node.Position;
        }

        public void SetData(int index, KDData data)
        {
            if (m_nodes == null)
            {
                throw new InvalidOperationException("Tree has been built");
            }

            m_nodes[index].Data = data;
        }

        public int Count
        {
            get
            {
                if (m_nodes != null)
                {
	                return m_nodes.Count;
                }
                else
                {
                    return m_dataCount;
                }
            }
        }

        public void Merge(KDTree<KDData> other)
        {
            if (m_root != null && other != null)
            {
                throw new InvalidOperationException("The tree has already been built, can not merge!");
            }

            m_nodes.AddRange(other.m_nodes);
        }

        public void Clear()
        {
            if (m_nodes != null)
            {
	            m_nodes.Clear();
	            m_nodes = null;
            }

            m_root = null;
        }

        public void BuildTree()
        {
            InternalBuild(ref m_root, m_rootBound, m_nodes);

            m_dataCount = m_nodes.Count;

            m_nodes.Clear();
            m_nodes = null;
        }

        public class KNNSearch
        {
            public struct NearData: IComparable<NearData>
            {
                public float sqauredDistance;

                public KDData data;

                public int CompareTo(NearData other)
                {
                    return sqauredDistance.CompareTo(other.sqauredDistance);
                }
            }

            int m_maxLookup;
            int m_found;
            List<NearData> m_data;

            public float MaxSquaredCoverRange
            {
                get;
                private set;
            }

            public KNNSearch(int maxPhotonLookup)
            {
                m_data = new List<NearData>(maxPhotonLookup);

                m_maxLookup = maxPhotonLookup;
                m_found = 0;
            }

            public void Reset(int maxPhotonLookup)
            {
                m_data.Clear();

                MaxSquaredCoverRange = 0;
                m_maxLookup = maxPhotonLookup;
                m_found = 0;
            }

            public void AddDataAction(KDData data, Vector3 position, float squaredDist, KDTree<KDData>.Lookuper lookuper)
            {
                if (m_found++ < m_maxLookup)
                {
                    if (MaxSquaredCoverRange < squaredDist)
                    {
                        MaxSquaredCoverRange = squaredDist;
                    }

                    // Neighbor list is still not full, add the newly found data to the array.
                    m_data.Add(new NearData { sqauredDistance = squaredDist, data = data });

                    if (m_data.Count == m_maxLookup)
                    {
                        m_data.Sort();

                        var farPhoton = m_data[m_data.Count - 1];

                        // Tighten up the search radius to the farthest range so far
                        lookuper.MaxSquaredRadius = farPhoton.sqauredDistance;

                        MaxSquaredCoverRange = farPhoton.sqauredDistance;
                    }
                }
                else
                {
                    // Neighbor list is full, we want to find the k nearest ones.

                    // Discard the farthest one.
                    m_data.RemoveAt(m_data.Count - 1);

                    // Insert a new data and makes sure the list is still ordered.
                    var nearPhoton = new NearData { sqauredDistance = squaredDist, data = data };
                    int insertIndex = m_data.BinarySearch(nearPhoton);
                    if (insertIndex < 0)
                    {
                        // bitwise invert the index to find the next insertion index if it is negative
                        insertIndex = ~insertIndex;
                    }
                    m_data.Insert(insertIndex, nearPhoton); // The insertion preserve the order of the list.

                    var farData = m_data[m_data.Count - 1];

                    // Tighten up the search radius to the farthest range so far
                    lookuper.MaxSquaredRadius = farData.sqauredDistance;

                    MaxSquaredCoverRange = farData.sqauredDistance;
                }
            }

            public List<NearData> GetData()
            {
                return m_data;
            }
        }

        public class Lookuper
        {
            public Vector3 Position
            {
                get;
                private set;
            }

            public float MaxSquaredRadius
            {
                get;
                set;
            }

            public Lookuper(Vector3 position, float maxRange)
            {
                Position = position;

                MaxSquaredRadius = maxRange * maxRange;
            }
        }

        public void Lookup(Lookuper lookuper, Action<KDData, Vector3, float, Lookuper> action)
        {
            InternalLookup(m_root, lookuper, action);
        }

        private void InternalLookup(KDNode<KDData> node, Lookuper lookuper, Action<KDData, Vector3, float, Lookuper> action)
        {
            if (node != null)
            {
	            int axis = node.SepAxis;
	            if (axis < 3)
	            {
                    float pa = lookuper.Position[axis], npa = node.Position[axis];
                    float dist = (pa - npa);
                    float squaredDist = dist * dist;
	                KDNode<KDData> inNode = null, colNode = null;

                    if (pa <= npa)
	                {
	                    inNode = node.Left;
                        if (squaredDist < lookuper.MaxSquaredRadius)
	                    {
	                        colNode = node.Right;
	                    }
	                }
	                else
	                {
	                    inNode = node.Right;
                        if (squaredDist < lookuper.MaxSquaredRadius)
	                    {
	                        colNode = node.Left;
	                    }
	                }

                    InternalLookup(colNode, lookuper, action);
                    InternalLookup(inNode, lookuper, action);
	            }

                float squaredDistance = Vector3.SquaredDist(node.Position, lookuper.Position);
                if (squaredDistance <= lookuper.MaxSquaredRadius)
	            {
                    action(node.Data, node.Position, squaredDistance, lookuper);
	            }
            }
        }

        private void InternalBuild(ref KDNode<KDData> newNode, BBox bound, List<KDNode<KDData>> nodes)
        {
            if (nodes.Count > 1)
            {
                List<KDNode<KDData>> xSorted = new List<KDNode<KDData>>(nodes);
                List<KDNode<KDData>> ySorted = new List<KDNode<KDData>>(nodes);
                List<KDNode<KDData>> zSorted = new List<KDNode<KDData>>(nodes);
                nodes = null;

                xSorted.Sort((n0, n1) => n0.Position[0].CompareTo(n1.Position[0]));
                ySorted.Sort((n0, n1) => n0.Position[1].CompareTo(n1.Position[1]));
                zSorted.Sort((n0, n1) => n0.Position[2].CompareTo(n1.Position[2]));

                int xSplitIndex, ySplitIndex, zSplitIndex;
                KDNode<KDData> xSplitNode, ySplitNode, zSplitNode;
                float xVVH = FindSplitPlane(bound, xSorted, 0, out xSplitIndex, out xSplitNode);
                float yVVH = FindSplitPlane(bound, ySorted, 1, out ySplitIndex, out ySplitNode);
                float zVVH = FindSplitPlane(bound, zSorted, 2, out zSplitIndex, out zSplitNode);

                List<KDNode<KDData>> sortedNodes = null;
                int splitAxis, splitIndex;
                float minVVH = Math.Min(xVVH, Math.Min(yVVH, zVVH));
                if (minVVH == xVVH)
                {
                    splitAxis = 0;
                    newNode = xSplitNode;
                    splitIndex = xSplitIndex;
                    sortedNodes = xSorted;
                } 
                else if (minVVH == yVVH)
                {
                    splitAxis = 1;
                    newNode = ySplitNode;
                    splitIndex = ySplitIndex;
                    sortedNodes = ySorted;
                }
                else
                {
                    splitAxis = 2;
                    newNode = zSplitNode;
                    splitIndex = zSplitIndex;
                    sortedNodes = zSorted;
                }

                xSplitNode = ySplitNode = zSplitNode = null;

                newNode.SepAxis = (byte)splitAxis;
                SplitNode(splitAxis, newNode, bound, splitIndex, sortedNodes);
            }
            else if (nodes.Count == 1)
            {
                newNode = nodes[0];
                newNode.SepAxis = 3;
            }
        }

        private void SplitNode(int axis, KDNode<KDData> splitNode, BBox bound, int splitIndex, List<KDNode<KDData>> sortedNodes)
        {
            if (sortedNodes.Count > 1)
            {
                BBox leftBound = new BBox(), rightBound = new BBox();
                bound.Split(axis, splitNode.Position[axis], out leftBound, out rightBound);

                var leftNodes = new List<KDNode<KDData>>(splitIndex);
                var rightNodes = new List<KDNode<KDData>>(sortedNodes.Count - splitIndex - 1);

                // The sortedNodes contains nodes that are sorted for the axis, so it can be sure
                // that the nodes with the indices less than splitIndex will always be under the left node,
                // and same goes for the right node.

                for (int n = 0; n < splitIndex; n++)
                {
                    leftNodes.Add(sortedNodes[n]);
                }

                for (int n = splitIndex + 1; n < sortedNodes.Count; n++)
                {
                    rightNodes.Add(sortedNodes[n]);
                }

                if (sortedNodes.Count > 128)
                {
	                Parallel.Invoke(
                        () => InternalBuild(ref splitNode.Left, leftBound, leftNodes),
                        () => InternalBuild(ref splitNode.Right, rightBound, rightNodes)
	                );
                } 
                else
                {
                    InternalBuild(ref splitNode.Left, leftBound, leftNodes);
                    InternalBuild(ref splitNode.Right, rightBound, rightNodes);
                }
            }
        }

        private float FindSplitPlane(BBox parentBound, List<KDNode<KDData>> axisSorted, int axis, out int splitIndex, out KDNode<KDData> splitNode)
        {
            float minVVH = float.MaxValue;
            splitIndex = 0;
            splitNode = null;

            float parentVV = EstimateVoxelVolume(parentBound);

            int leftNodeCount = 1, rightNodeCount = axisSorted.Count - 1;

            for (int n = 1; n <= axisSorted.Count - 1; n++ )
            {
                var node = axisSorted[n];

                // Use VVH to find the optimal splitting plane
                BBox leftBound, rightBound;
                parentBound.Split(axis, node.Position[axis], out leftBound, out rightBound);
                float leftVV = EstimateVoxelVolume(leftBound), rightVV = EstimateVoxelVolume(rightBound);

                float VVH = leftNodeCount * (leftVV / parentVV) + rightNodeCount * (rightVV / parentVV);
                if (VVH < minVVH)
                {
                    minVVH = VVH;
                    splitIndex = n;
                    splitNode = node;
                }

                leftNodeCount++;
                rightNodeCount--;
            }

            return minVVH;
        }

        private float EstimateVoxelVolume(BBox bound)
        {
            float vv = 1;
            for (int axis = 0; axis < 3; axis++)
            {
                vv *= bound.Upper[axis] - bound.Lower[axis] + 2.0f * m_queryRangeHint;
            }

            return vv;
        }
    }
}
