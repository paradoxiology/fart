﻿using System;

using Fart.Maths;
using Fart.Scene;
using Fart.Shading;

namespace Fart.Render
{
    // Estimate direct illumination for the Shading State.
    public class DirectEstimator
    {
        private int m_maxSampleCount;

        public DirectEstimator(World world, int maxSampleCount, int maxShadowPhotons)
        {
            m_maxSampleCount = maxSampleCount;
        }

        public MultiColor GetRadiance(World world, Vector3 wo, ShadingState sState, ISampler sampler)
        {
            MultiColor finalRadiance = new MultiColor();

            Vector3 position = sState.Position;
            Vector3 normal = sState.Normal;


            foreach (var light in world.Lights)
            {
                LightColor lightRadiance = new LightColor(light, Color3.Black);

                float lightPDF;
                Ray visiRay;
                Vector3 wi;

                LightColor Li = light.SampleLight(position + wo * Ray.MIN_T, normal, sampler.GetSample2D(), out wi, out lightPDF, out visiRay);

                if (Vector3.Dot(wi, normal) < 0.0)
                {
                    continue;
                }

                if (world.CheckVisibility(visiRay, null) && lightPDF > 0.0f)
                {
                    float dot = Vector3.Dot(wi, normal);
                    Color3 fr = sState.Material.f(sState, wi, wo);
                    float bsdfPDF = sState.Material.PDF(sState, wo, wi);
                    float weight = Utils.PowerHeuristic(1, lightPDF, 1, bsdfPDF);

                    lightRadiance += Li * fr * dot * weight / lightPDF; // Add the light contribution (multiple importance sampling)
                }

                finalRadiance.Add(lightRadiance);
            }

            return finalRadiance;
        }
    }
}
