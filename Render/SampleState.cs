﻿using Fart.Maths;

namespace Fart.Render
{
    public struct SampleState
    {
        uint m_instance;
        int m_dim;
        int m_activeDim;

        uint m_trajectory;
        int m_maxTrajectory;

        public int CurrentTrajectory
        {
            get
            {
                return (int) m_trajectory;
            }
        }
        
        public SampleState(uint i, int d, uint j, int m)
        {
            m_instance = i;
            m_dim = d;
            m_activeDim = 0;

            m_trajectory = j;
            m_maxTrajectory = m;
        }

        public SampleState SplitTrajectory(int j)
        {
            return new SampleState(m_instance, m_dim, (uint) j, 0);
        }

        public SampleState SplitTrajectory(int j, int m)
        {
            return new SampleState(m_instance, m_dim, (uint) j, m);
        }

        public SampleState NextState()
        {
            return new SampleState(m_instance + m_trajectory, m_dim + m_activeDim, 0, 0);
        }

        public Sample1 PeakSample1D(int deltaDim)
        {
            int dim = m_dim + deltaDim;

            double s;
            if (m_maxTrajectory == 0)
            {
                s = Utils.Decimal(QMC.GetHalton(dim, m_instance) + QMC.GetHalton(deltaDim, m_trajectory));
            }
            else
            {
                s = Utils.Decimal(QMC.GetHalton(dim, m_instance) + QMC.GetHammersley(deltaDim, m_trajectory, m_maxTrajectory));
            }

            return new Sample1((float) s);
        }

        public Sample1 PopSample1D()
        {
            var sample = PeakSample1D(m_activeDim);

            m_activeDim += 1;

            return sample;
        }

        public Sample2 PeakSample2D(int deltaDim)
        {
            int dim = m_dim + deltaDim;

            double s0, s1;
            if (m_maxTrajectory == 0)
            {
                s0 = Utils.Decimal(QMC.GetHalton(dim, m_instance) + QMC.GetHalton(deltaDim, m_trajectory));
                s1 = Utils.Decimal(QMC.GetHalton(dim + 1, m_instance) + QMC.GetHalton(deltaDim + 1, m_trajectory));
            }
            else
            {
                s0 = Utils.Decimal(QMC.GetHalton(dim, m_instance) + QMC.GetHammersley(deltaDim, m_trajectory, m_maxTrajectory));
                s1 = Utils.Decimal(QMC.GetHalton(dim + 1, m_instance) + QMC.GetHammersley(deltaDim + 1, m_trajectory, m_maxTrajectory));
            }

            return new Sample2((float) s0, (float) s1);
        } 

        public Sample2 PopSample2D()
        {
            var sample = PeakSample2D(m_activeDim);

            m_activeDim += 2;

            return sample;
        }
    }
}
